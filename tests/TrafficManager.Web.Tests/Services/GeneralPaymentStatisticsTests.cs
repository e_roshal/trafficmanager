﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Microsoft.Extensions.DependencyInjection;
//using NUnit.Framework;
//using TrafficManager.TestData.Helpers;
//using TrafficManager.Web.Infrastructure;
//using TrafficManager.Web.Models.Data;
//using TrafficManager.Web.Models.DTO;
//using TrafficManager.Web.Services;

//namespace TrafficManager.Web.Tests.Services
//{
//    [TestFixture]
//    class GeneralPaymentStatisticsTests
//    {
//        private IServiceProvider _services;
//        private IRepository<BrokersUsers> _brkrUsersRepo;
//        private IRepository<AffiliatePayment> _affPaymentsRepo;
//        private IRepository<BrokerPayment> _brkrPaymentsRepo;
//        private IRepository<User> _usersRepo;
//        private IRepository<Broker> _brokerRepo;
//        private IRepository<Affilate> _affRepo;
//        private PaymentStatisticsService _statSrv;

//        [SetUp]
//        public void BeforeEach()
//        {
//            _services = new DatabaseDiBootstrapperInMemory().GetServiceProviderWithSeedDb();

//            _brkrUsersRepo = _services.GetRequiredService<IRepository<BrokersUsers>>();
//            _affPaymentsRepo = _services.GetRequiredService<IRepository<AffiliatePayment>>();
//            _brkrPaymentsRepo = _services.GetRequiredService<IRepository<BrokerPayment>>();
//            _usersRepo = _services.GetRequiredService<IRepository<User>>();
//            _brokerRepo = _services.GetRequiredService<IRepository<Broker>>();
//            _affRepo = _services.GetRequiredService<IRepository<Affilate>>();

//            _statSrv = new PaymentStatisticsService(_services.GetService<OVODbContext>());
//        }


//        [Test]
//        public void Should_filter_brokersUsers_data_by_affiliate()
//        {
//            var filter = new PaymentStatsFilterDTO()
//            {
//                AffiliateIds = "2015, 2008"
//            };
//            var result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 1);

//            filter.AffiliateIds = "2009";
//            result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 3);

//            filter.AffiliateIds = "2000, 2344, 2333";
//            result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 0);
//        }

//        [Test]
//        public void Should_filter_brokersUsers_data_by_broker()
//        {
//            var filter = new PaymentStatsFilterDTO()
//            {
//                BrokerIds = "111"
//            };
//            var result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 3);

//            filter.BrokerIds = "101, 234";
//            result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 1);

//            filter.BrokerIds = "2000, 2344, 2333";
//            result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 0);

//        }

//        [Test]
//        //  [Ignore("we do not filter brokers users by countries")]
//        public void Should_filter_brokersUsers_data_by_country()
//        {
//            var filter = new PaymentStatsFilterDTO()
//            {
//                Countries = "DE"
//            };
//            var result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 1);

//            filter.Countries = "RU";
//            result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 1);

//            filter.Countries = "ES|EN";
//            result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(result.Count(), 0);

//        }

//        [Test]
//        public void Should_filter_brokersUsers_data_by_different_date()
//        {
//            var filter = new PaymentStatsFilterDTO()
//            {
//                DateFrom = "2018-06-12"
//            };
//            var result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(3, result.Count());

//            filter.DateFrom = "2018-07-25";
//            result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(1, result.Count());

//            filter.DateFrom = "2018-07-02";
//            result = _statSrv.GetFilteredBrokerUsersData(filter);
//            Assert.AreEqual(2, result.Count());
//        }

//        [Test]
//        public void Should_filter_brokersUsers_data_by_complex_filters()
//        {
//            var filter = new PaymentStatsFilterDTO()
//            {
//                BrokerIds = "111",
//                DateFrom = "2018-06-01",
//                DateTo = "2018-07-01"
//            };
//            var result = _statSrv.GetFilteredBrokerUsersData(filter);

//            Assert.AreEqual(result.Count(), 1);
//        }
//    }
//}

