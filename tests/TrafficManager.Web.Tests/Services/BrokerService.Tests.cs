﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.TestData.Helpers;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Services;

namespace TrafficManager.Web.Tests.Services
{
    [TestFixture]
    class BrokerServiceTests
    {
        private IServiceProvider _services;
        private IRepository<Broker> _brokerRepo;
        private IRepository<Country> _countryRepo;
        private IBrokerService _brokerService;

        [SetUp]
        public void BeforeEach()
        {
            _services = new DatabaseDiBootstrapperInMemory().GetServiceProviderWithSeedDb();

            _brokerRepo = _services.GetRequiredService<IRepository<Broker>>();
            _countryRepo = _services.GetRequiredService<IRepository<Country>>();

            _brokerService = new BrokerService(_brokerRepo, _countryRepo);
        }

        [Test]
        public void Should_Add_Blocked_Countries_In_TwoCharFormat_While_Addition()
        {
            string blockedCountries = "23546yqtwyv|RU|China";

            var broker = new Broker()
            {
                Id = 999,
                Name = "Broker 999",
                BlockedCountries = blockedCountries
            };
            _brokerService.Add(broker);

            var result = _brokerRepo.Data.FirstOrDefault(x => x.Id == 999);

            string expectedCountries = "|RU|CN|";

            Assert.AreEqual(expectedCountries, result.BlockedCountries);
        }

        [Test]
        public void Should_Add_Blocked_Countries_In_TwoCharFormat_While_Edit()
        {
            //string blockedCountries = "23546yqtwyv|RU|China";

            //var broker = new Broker()
            //{
            //    Id = 111,
            //    Name = "Broker 1",
            //    BlockedCountries = blockedCountries
            //};
            //_brokerService.Update(broker);

            ////var result = _brokerRepo.Data.FirstOrDefault(x => x.Id == 111);

            //string expectedCountries = "|RU|CN|";

            //Assert.AreEqual(expectedCountries, "|RU|CN|");

            var list1 = new[] {new { X = 1 , Y = "A"} , new { X = 2, Y = "B" } };
            var list2 = new[] { new { X = 1, Z = "a" }, new { X = 1, Z = "b" }, new { X = 2, Z = "c" }, new { X = 3, Z = "d" } };

            
            var innerJoin = list2.Join(list1, x => x.X, y => y.X, (one, two) => new {One = one, Two = two}).ToList();

            var leftJoin = from two in list2
                join one in list1 on two.X equals one.X into _join
                from subjoin in _join.DefaultIfEmpty()
                select new {One = subjoin, Two = two};

            var leftJoinFluent = list2.GroupJoin(list1, two => two.X, one => one.X, (two, _join) => new {two, _join})
                .SelectMany(@t => @t._join.DefaultIfEmpty(), (@t, subjoin) => new {One = subjoin, Two = @t.two});


            Assert.IsTrue(leftJoin.Count() == 4);
            Assert.IsTrue(innerJoin.Count == 3);
        }
        }
}
