﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.TestData.Helpers;
using TrafficManager.TestData.Helpers.Data;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;
using TrafficManager.Web.Services;

namespace TrafficManager.Web.Tests.Services
{
    class BrokerPaymentStatisticsServiceTests
    {
        private IServiceProvider _services;
        private IRepository<BrokersUsers> _brkrUsersRepo;
        private IRepository<AffiliatePayment> _affPaymentsRepo;
        private IRepository<BrokerPayment> _brkrPaymentsRepo;
        private IRepository<User> _usersRepo;
        private IRepository<Broker> _brokerRepo;
        private IRepository<Affilate> _affRepo;

        private int totalItems = 10000;
        private PaymentStatisticsService _statSrv;

        [SetUp]
        public void BeforeEach()
        {
            _services = new DatabaseDiBootstrapperInMemory().GetServiceProviderWithSeedDb();

            _brkrUsersRepo = _services.GetRequiredService<IRepository<BrokersUsers>>();
            _affPaymentsRepo = _services.GetRequiredService<IRepository<AffiliatePayment>>();
            _brkrPaymentsRepo = _services.GetRequiredService<IRepository<BrokerPayment>>();
            _usersRepo = _services.GetRequiredService<IRepository<User>>();
            _brokerRepo = _services.GetRequiredService<IRepository<Broker>>();
            _affRepo = _services.GetRequiredService<IRepository<Affilate>>();

            _statSrv = new PaymentStatisticsService(_services.GetService<OVODbContext>());
        }

        [Test]
        public void Should_Affiliates_Has_His_Brokers_Without_Filter()
        {
            var expectingBrkr = 111;
            var expectedAffs = (new List<string> { "2009", "2015" }).AsQueryable()
                                                                    .Distinct()
                                                                    .OrderBy(x => x);

            var brkrPayStats = _statSrv.GetBrkrStats(new PaymentStatsFilterDTO(), out totalItems).Data;

            Assert.IsTrue(brkrPayStats.FirstOrDefault(x => x.BrokerId == expectingBrkr)
                                      .BrokerAffiliates
                                      .SequenceEqual(expectedAffs));
        }

        [Test]
        public void Should_Affiliates_Has_His_Brokers_With_Filtered_Brokers()
        {
            var filter = new PaymentStatsFilterDTO()
            {
                AffiliateIds = "2009",
            };

            var expectingBrkr = 111;

            var expectedAffs = filter.AffiliateIds.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries)
                                     .AsQueryable()
                                     .OrderBy(x => x);

            var brkrPayStats = _statSrv.GetBrkrStats(filter, out totalItems).Data;

            Assert.IsTrue(brkrPayStats.FirstOrDefault(x => x.BrokerId == expectingBrkr)
                                      .BrokerAffiliates
                                      .SequenceEqual(expectedAffs));
        }

        [Test]
        public void Should_Get_Filtered_Statistics_For_Brokers()
        {
            var testBrkrs = new TestBrokers().GetBrokers();
            var filter = new PaymentStatsFilterDTO()
            {
                AffiliateIds = "2015, 2043",
                BrokerIds = "",
                Countries = "",
                DateFrom = "2018.06.15",
                DateTo = "",
                OrderByField = "",
                IsDesc = false,
                PageNumber = 0,
                PageSize = 100
            };

            var brkrPayStats = _statSrv.GetBrkrStats(filter, out totalItems).Data;

            // We do not send whole brokers table as result, only those who has payments
            //foreach (var brkr in testBrkrs)
            //{
            //    Assert.IsTrue(brkrPayStats.Any(x => x.BrokerId == brkr.Id));

            //    var itemUnderTest = brkrPayStats.FirstOrDefault(x => x.BrokerId == brkr.Id);
            //    if (itemUnderTest.Leads == 0)
            //    {
            //        Assert.IsTrue(itemUnderTest.Leads == 0);
            //        Assert.IsTrue(itemUnderTest.Deposites == 0);
            //        Assert.IsTrue(itemUnderTest.Ratio == null);
            //        Assert.IsTrue(itemUnderTest.Payments == 0);
            //        Assert.IsTrue(itemUnderTest.Profit == 0);
            //        Assert.IsTrue(!itemUnderTest.BrokerAffiliates.Any());
            //    }
            //}

            long expectingBrkrId = 111;
            int expectedPayout = 700;
            int expectedProfit = 400;

            Assert.IsTrue(brkrPayStats.FirstOrDefault(x => x.BrokerId == expectingBrkrId)
                                      .Payments == expectedPayout);
            Assert.IsTrue(brkrPayStats.FirstOrDefault(x => x.BrokerId == expectingBrkrId)
                                      .Profit == expectedProfit);
            
            // we do not add statistics item for now
            // Assert.IsTrue(brkrPayStats.Any(x => x.BrokerId == 0));
        }
    }
}
