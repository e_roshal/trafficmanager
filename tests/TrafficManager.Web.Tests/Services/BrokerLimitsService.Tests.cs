﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using TrafficManager.TestData.Helpers;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Services;

namespace TrafficManager.Web.Tests.Services
{
    [TestFixture]
    public class BrokerLimitsService
    {
        private IServiceProvider _services;
        private IRepository<Broker> _brokerRepo;
        private IRepository<Country> _countryRepo;
        private IRepository<BrokerCountryLimit> _brokerCountryLimitRepo;
        private IBrokerLimitService _brokerLimitSvc;

        [SetUp]
        public void BeforeEach()
        {
            _services = new DatabaseDiBootstrapperInMemory().GetServiceProviderWithSeedDb();

            _brokerCountryLimitRepo = _services.GetRequiredService<IRepository<BrokerCountryLimit>>();
            _brokerRepo = _services.GetRequiredService<IRepository<Broker>>();
            _countryRepo = _services.GetRequiredService<IRepository<Country>>();

            _brokerLimitSvc = new Web.Services.BrokerLimitsService(_brokerCountryLimitRepo,
                                                                    _brokerRepo,
                                                                    _countryRepo);
        }

        [Test]
        public void Should_validate_broker_limit_item()
        {
            var limit = new BrokerCountryLimit
            {
                Id = _brokerRepo.Data.Max(x=>x.Id) +1,
                Countries = "UK",
                BrokerId = 101,
                Limit = 7
            };

            var result = _brokerLimitSvc.Add(limit);

            var item = _brokerCountryLimitRepo.Data.FirstOrDefault(x =>
                x.BrokerId == 111 && x.Limit == 7 && x.Countries.Equals("UK"));

            Assert.IsFalse(result.Success);
            Assert.AreEqual(1, result.Messages.Count());
            Assert.IsNull(item);
        }

    }
}
