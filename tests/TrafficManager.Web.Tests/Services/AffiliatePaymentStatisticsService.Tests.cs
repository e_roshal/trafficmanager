﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.TestData.Helpers;
using TrafficManager.TestData.Helpers.Data;
using TrafficManager.Web.Configuration;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;
using TrafficManager.Web.Services;

namespace TrafficManager.Web.Tests.Services
{
    [TestFixture]
    class AffiliatePaymentStatisticsServiceTests
    {
        private IServiceProvider _services;
        private IRepository<BrokersUsers> _brkrUsersRepo;
        private IRepository<AffiliatePayment> _affPaymentsRepo;
        private IRepository<BrokerPayment> _brkrPaymentsRepo;
        private IRepository<User> _usersRepo;
        private IRepository<Broker> _brokerRepo;
        private IRepository<Affilate> _affRepo;
        private PaymentStatisticsService _statSrv;

        [SetUp]
        public void BeforeEach()
        {
            _services = new DatabaseDiBootstrapperInMemory().GetServiceProviderWithSeedDb();

            _brkrUsersRepo = _services.GetRequiredService<IRepository<BrokersUsers>>();
            _affPaymentsRepo = _services.GetRequiredService<IRepository<AffiliatePayment>>();
            _brkrPaymentsRepo = _services.GetRequiredService<IRepository<BrokerPayment>>();
            _usersRepo = _services.GetRequiredService<IRepository<User>>();
            _brokerRepo = _services.GetRequiredService<IRepository<Broker>>();
            _affRepo = _services.GetRequiredService<IRepository<Affilate>>();

            _statSrv = new PaymentStatisticsService(_services.GetService<OVODbContext>());
        }

        [Test]
        [Ignore("unnecessary check")]
        public void Should_Start_Test()
        {
            Assert.NotNull(_brkrUsersRepo);
            Assert.AreEqual(_brkrUsersRepo.Data.Count(), 4);

            Assert.NotNull(_affPaymentsRepo);
            Assert.NotNull(_brkrPaymentsRepo);
            Assert.NotNull(_usersRepo);
            Assert.NotNull(_brokerRepo);
            Assert.NotNull(_affRepo);
        }

        [Test]
        public void Should_get_data()
        {
            var filter = new PaymentStatsFilterDTO()
            {};

            var something = _statSrv.GetAffStats(filter, out var totalItems);
            Assert.AreEqual(something.TotalItems, 2);
            Assert.AreEqual(something.Data.ToList()[0].Payments, 500);
            Assert.AreEqual(something.Data.ToList()[0].Profit, 200);

            filter = new PaymentStatsFilterDTO()
            {   
                   AffiliateIds = "2009",
            };

            something = _statSrv.GetAffStats(filter, out totalItems);
            Assert.AreEqual(something.TotalItems, 1);

        }

        [Test]
        public void Should_Affiliates_Has_His_Brokers_Without_Filter()
        {
            var expectingAff = "2009";
            var expectedBrokers = (new List<long> { 111, 101 }).AsQueryable()
                                                               .Distinct()
                                                               .OrderBy(x => x);

            var affPayStats = _statSrv.GetAffStats(new PaymentStatsFilterDTO(), out var totalItems).Data;

            Assert.IsTrue(affPayStats.FirstOrDefault(x => x.AffiliateId == expectingAff)
                                     .AffiliateBrokers
                                     .SequenceEqual(expectedBrokers));
        }

        [Test]
        public void Should_Affiliates_Has_His_Brokers_With_Filtered_Brokers()
        {
            var filter = new PaymentStatsFilterDTO()
            {
                BrokerIds = "111",
            };

            var expectingAff = "2009";

            var expectedBrokers = filter.BrokerIds.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries)
                                        .Select(long.Parse)
                                        .AsQueryable()
                                        .OrderBy(x => x);

            var affPayStats = _statSrv.GetAffStats(filter, out var totalItems).Data;

            Assert.IsTrue(affPayStats.FirstOrDefault(x => x.AffiliateId == expectingAff)
                                     .AffiliateBrokers
                                     .SequenceEqual(expectedBrokers));
        }

        [Test]
        public void Should_Get_Filtered_Statistics_For_Affiliates()
        {
            var testAffs = new TestAffiliates().GetAffiliates();
            var filter = new PaymentStatsFilterDTO()
            {
                AffiliateIds = "",
                BrokerIds = "111, 101",
                Countries = "",
                DateFrom = "2018.06.15",
                DateTo = "",
                OrderByField = "",
                IsDesc = false,
                PageNumber = 0,
                PageSize = 100
            };

            var affPayStats = _statSrv.GetAffStats(filter, out var totalItems).Data;

            foreach (var aff in testAffs)
            {
                Assert.IsTrue(affPayStats.Any(x => x.AffiliateId == aff.Id));

                var itemUnderTest = affPayStats.FirstOrDefault(x => x.AffiliateId == aff.Id);
                if (itemUnderTest.Leads == 0)
                {
                    Assert.IsTrue(itemUnderTest.Leads == 0);
                    Assert.IsTrue(itemUnderTest.Deposites == 0);
                    Assert.IsTrue(itemUnderTest.Ratio == null);
                    Assert.IsTrue(itemUnderTest.Payments == 0);
                    Assert.IsTrue(itemUnderTest.Profit == 0);
                    Assert.IsTrue(!itemUnderTest.AffiliateBrokers.Any());
                }
            }

            string expectingAffId = "2009";
            int expectedPayout = 500;
            int expectedProfit = 200;

            Assert.IsTrue(affPayStats.FirstOrDefault(x => x.AffiliateId == expectingAffId)
                                     .Payments == expectedPayout);
            Assert.IsTrue(affPayStats.FirstOrDefault(x => x.AffiliateId == expectingAffId)
                                     .Profit == expectedProfit);

            // no 'total' line in this version
            // Assert.IsTrue(affPayStats.Any(x => x.AffiliateId == null));
        }
    }
}
