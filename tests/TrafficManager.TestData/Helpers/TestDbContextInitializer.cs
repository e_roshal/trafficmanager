﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.TestData.Data;
using TrafficManager.TestData.Helpers;
using TrafficManager.TestData.Helpers.Data;
using TrafficManager.Web.Configuration;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Helpers
{
    class TestDbContextInitializer
    {
        private static bool _isInitialized = false;
        private Random _random = new Random();
        private OVODbContext _context;

        public void SeedAppUsersAndRoles(IServiceProvider services)
        {
            AppConfigurator.InitRolesAndUsers(services);
        }

        public void SeedData(IServiceProvider services)
        {
            SeedBrokersAndAffs(services);
            SeedPayments(services);
            SeedUsers(services);
            SeedBrokerUsers(services);
            SeedCountries(services);
            SeedBrokerCountryLimits(services);
        }

        private void SeedCountries(IServiceProvider services)
        {
            var countriesRepo = services.GetRequiredService<IRepository<Country>>();

            countriesRepo.Add(new TestCountries().GetCountries());
        }

        public void SeedPayments(IServiceProvider services)
        {
            var affPaymentsRepo = services.GetRequiredService<IRepository<AffiliatePayment>>();
            var brokerPaymentsRepo = services.GetRequiredService<IRepository<BrokerPayment>>();

            affPaymentsRepo.Add(new TestAffPayments().GetAffPayments());
            brokerPaymentsRepo.Add(new TestBrokerPayments().GetBrokerPayments());
        }

        public void SeedBrokersAndAffs(IServiceProvider services)
        {
            var affRepo = services.GetRequiredService<IRepository<Affilate>>();
            var brkrRepo = services.GetRequiredService<IRepository<Broker>>();

            affRepo.Add(new TestAffiliates().GetAffiliates());
            brkrRepo.Add(new TestBrokers().GetBrokers());
        }

        public void SeedUsers(IServiceProvider services)
        {
            var usersRepo = services.GetRequiredService<IRepository<User>>();
            usersRepo.Add(new TestUsers().GetUsers());
        }

        public void SeedBrokerUsers(IServiceProvider services)
        {
            var buRepo = services.GetRequiredService<IRepository<BrokersUsers>>();
            buRepo.Add(new TestBrokersUsers().GetBrokersUsers());
            
        }

        public void SeedBrokerCountryLimits(IServiceProvider services)
        {
            var bclRepo = services.GetRequiredService<IRepository<BrokerCountryLimit>>();
            bclRepo.Add(new TestBrokerCountryLimits().GetBrokerCountryLimits());
        }
    }
}
