﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using TrafficManager.Web.Configuration;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Models;

namespace TrafficManager.TestData.Helpers
{
    public class DatabaseDiBootstrapperInMemory : IServiceProviderBootstrapper
    {
        public OVODbContext GetDataContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<OVODbContext>();
            optionsBuilder.UseInMemoryDatabase("TestInMemory");
            var ctx = new OVODbContext(optionsBuilder.Options);
            return ctx;
        }

        public IServiceProvider GetServiceProvider()
        {
            var services = new ServiceCollection();

            services.AddEntityFrameworkInMemoryDatabase()
                .AddDbContext<OVODbContext>(options => options.UseInMemoryDatabase("Test"));
            services.AddEntityFrameworkInMemoryDatabase()
                .AddDbContext<TMDbContext>(options => options.UseInMemoryDatabase("TestIdentity"));

            services.AddIdentity<TMUser, IdentityRole>()
                .AddEntityFrameworkStores<TMDbContext>();

            var httpContext = new DefaultHttpContext();
            httpContext.Features.Set<IHttpAuthenticationFeature>(new HttpAuthenticationFeature());
            services.AddTransient<IHttpContextAccessor>(h => new HttpContextAccessor { HttpContext = httpContext });

            AppConfigurator.ConfigureDataServices(services);


            services.AddSingleton<IConfigurationRoot>(c => new Mock<IConfigurationRoot>().Object);
            services.AddSingleton<IConfiguration>(c => new Mock<IConfiguration>().Object);

            var serviceProvider = services.BuildServiceProvider();
            return serviceProvider;
        }

        public IServiceProvider GetServiceProviderWithSeedDb()
        {
            var provider = GetServiceProvider();
            var dbSeed = new TestDbContextInitializer();
            dbSeed.SeedAppUsersAndRoles(provider);
            dbSeed.SeedData(provider);

            return provider;
        }
    }
}
