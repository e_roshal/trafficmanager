﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Infrastructure;

namespace TrafficManager.TestData.Helpers
{
    public interface IServiceProviderBootstrapper
    {
        OVODbContext GetDataContext();

        IServiceProvider GetServiceProvider();
        IServiceProvider GetServiceProviderWithSeedDb();
    }
}
