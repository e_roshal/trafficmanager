﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Helpers.Data
{
    public class TestUsers
    {
        private ICollection<User> Users = new List<User>
        {
            new User(){ Id = 1, AffId = "2009", Country = "US" , },
            new User(){ Id = 2, AffId = "2015", Country = "RU" , },
            new User(){ Id = 3, AffId = "2009", Country = "CN" , },
            new User(){ Id = 4, AffId = "2009", Country = "DE" , },
        };

        public ICollection<User> GetUsers()
        {
            return Users;
        }
    }
}
