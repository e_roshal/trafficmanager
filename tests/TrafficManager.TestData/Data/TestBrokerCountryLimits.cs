﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Data
{
    class TestBrokerCountryLimits
    {
        private ICollection<BrokerCountryLimit> brokerCountryLimits = new List<BrokerCountryLimit>()
        {
            new BrokerCountryLimit {
                Id = 1,
                Countries = "**",
                BrokerId = 111,
                Limit = 7
            },
            new BrokerCountryLimit {
                Id = 2,
                Countries = "|DE|GB|UK",
                BrokerId = 101,
                Limit = 10
            },
            new BrokerCountryLimit {
                Id = 3,
                Countries = "|US|RU|",
                BrokerId = 101,
                Limit = 12
            },

        };

        public ICollection<BrokerCountryLimit> GetBrokerCountryLimits()
        {
            return brokerCountryLimits;
        }
    }
}
