﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Data
{
    class TestCountries
    {
        private ICollection<Country> countries = new List<Country>()
        {
            new Country { CountryName = "China" , CountryIso = "CN" },
            new Country { CountryName = "Russian Federation" , CountryIso = "RU" },
            new Country { CountryName = "France" , CountryIso = "FR" }
        };

        public ICollection<Country> GetCountries()
        {
            return countries;
        }
    }
}
