﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Helpers.Data
{
    public class TestBrokers
    {
        private ICollection<Broker> Brokers = new List<Broker>()
        {
            new Broker { Id = 111, Name = "Broker 1", BlockedCountries = "RU" },
            new Broker { Id = 101, Name = "Broker 2" },
            new Broker { Id = 115, Name = "Broker 3" },
            new Broker { Id = 159, Name = "Broker 4" }
        };

        public ICollection<Broker> GetBrokers()
        {
            return Brokers;
        }
    }
}
