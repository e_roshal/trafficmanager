﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Data
{
    class TestBrokerPayments
    {
        private ICollection<BrokerPayment> BrokerPaymentsList = new List<BrokerPayment>()
        {
            new BrokerPayment {
                Id = 1,
                Country = "**",
                PaymentType = 1, // CPA
                BrokerId = 111,
                Amount = 700,
                ValidFrom = DateTime.Parse("2018-01-01"),
            },


        };

        public ICollection<BrokerPayment> GetBrokerPayments()
        {
            return BrokerPaymentsList;
        }
    }
}
