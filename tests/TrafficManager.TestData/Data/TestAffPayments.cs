﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Data
{
    class TestAffPayments
    {
        private ICollection<AffiliatePayment> AffPaymentsList = new List<AffiliatePayment>()
        {
            new AffiliatePayment {
                Id = 3,
                Country = "**",
                PaymentType = 2,  //CPA
                AffiliateId = 2015,
                Amount = 300,
                ValidFrom = DateTime.Parse("2018-01-01"),
            },

            new AffiliatePayment {
                Id = 1,
                Country = "**",
                PaymentType = 1,  //CPA
                AffiliateId = 2009,
                Amount = 100,
                ValidFrom = DateTime.Parse("2018-01-01"),
            },

            new AffiliatePayment {
                Id = 2,
                Country = "DE",
                PaymentType = 1,  //CPA
                AffiliateId = 2009,
                Amount = 500,
                ValidFrom = DateTime.Parse("2018-03-01"),
            },

        };

        public ICollection<AffiliatePayment> GetAffPayments()
        {
            return AffPaymentsList;
        }

    }
}
