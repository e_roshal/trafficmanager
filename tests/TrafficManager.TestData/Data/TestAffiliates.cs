﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Helpers.Data
{
    public class TestAffiliates
    {
        private ICollection<Affilate> Affiliates = new List<Affilate>()
        {
            new Affilate { Id = "2009", Name = "Test1"},
            new Affilate { Id = "2015", Name = "Test2"},
        };

        public ICollection<Affilate> GetAffiliates()
        {
            return Affiliates;
        }
    }
}
