﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.TestData.Data
{
    public class TestBrokersUsers
    {
        private ICollection<BrokersUsers> BrokersUsersList = new List<BrokersUsers>()
        {
            new BrokersUsers {
                Id = 1,
                UserId = 1,
                BrokerId = 111,
                AffId = "2009",
                LeadDate = new DateTime(2018,05,02)},

            new BrokersUsers {
                Id = 2,
                UserId = 2,
                BrokerId = 111,
                AffId = "2015",
                LeadDate = new DateTime(2018,06,15),
                DepositDate = new DateTime(2018,06,30)},

            new BrokersUsers {
                Id = 3,
                UserId = 3,
                BrokerId = 101,
                AffId = "2009",
                LeadDate = new DateTime(2018,07,23)},

            new BrokersUsers {
                Id = 4,
                BrokerId = 111,
                AffId = "2009",
                UserId = 4,
                LeadDate = new DateTime(2018,07,11),
                DepositDate = new DateTime(2018,08,13)}
        };

        public ICollection<BrokersUsers> GetBrokersUsers()
        {
            return BrokersUsersList;
        }
    }
}
