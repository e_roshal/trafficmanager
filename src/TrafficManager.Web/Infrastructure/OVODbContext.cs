﻿using Microsoft.EntityFrameworkCore;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.Web.Infrastructure
{
    public partial class OVODbContext : DbContext
    {
        public virtual DbSet<Affilate> Affilates { get; set; }
        public virtual DbSet<Broker> Brokers { get; set; }
        public virtual DbSet<BrokersUsers> BrokersUsers { get; set; }
        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<Click> Clicks { get; set; }
        public virtual DbSet<Offer> Offers { get; set; }
        public virtual DbSet<Routing> Routing { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<AffiliatePayment> AffilatePayments { get; set; }
        public virtual DbSet<BrokerPayment> BrokerPayments { get; set; }


        public virtual DbSet<Mail> Mails { get; set; }
        public virtual DbSet<Country> Countries{ get; set; }

        public virtual DbSet<BrokerCountryLimit> BrokerCountryLimits { get; set; }
        public virtual DbSet<AffiliateCountryLimit> AffiliateCountryLimits { get; set; }

        // Unable to generate entity type for table 'dbo.countries'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.mails'. Please see the warning messages.


        public OVODbContext(DbContextOptions<OVODbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer(@"Server=.\;Database=OVO_ser;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Affilate>(entity =>
            {
                entity.Property(e => e.Id)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<Broker>(entity =>
            {
                entity.Property(e => e.AccountId).IsUnicode(false);
                entity.Property(e => e.EndDate).HasDefaultValueSql("('2099-01-01')");
                entity.Property(e => e.LastSync).HasDefaultValueSql("('2016-01-01 00:00:00.000')");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.Password).IsUnicode(false);
                entity.Property(e => e.UserName).IsUnicode(false);
            });

            modelBuilder.Entity<BrokersUsers>(entity =>
            {
                entity.Property(e => e.AffId).IsUnicode(false);
                entity.Property(e => e.Currency).IsUnicode(false);
                entity.Property(e => e.Guid).IsUnicode(false);
                entity.Property(e => e.Id2).IsUnicode(false);
                entity.Property(e => e.OfferId).IsUnicode(false);
                entity.Property(e => e.CallStatus).IsUnicode(false);
                //entity.HasOne(d => d.Broker)
                //    .WithMany(p => p.BrokersUsers)
                //    .HasForeignKey(d => d.BrokerId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK_brokers_users_users");

                //entity.HasOne(d => d.User)
                //    .WithMany(p => p.BrokersUsers)
                //    .HasForeignKey(d => d.UserId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK_brokers_users_users1");
            });

            modelBuilder.Entity<Campaign>(entity =>
            {
                entity.Property(e => e.CmpId).IsUnicode(false);
                entity.Property(e => e.CmpName).IsUnicode(false);
                entity.Property(e => e.Language).IsUnicode(false);

                //entity.HasOne(d => d.Broker)
                //    .WithMany(p => p.Campaigns)
                //    .HasForeignKey(d => d.BrokerId)
                //    .HasConstraintName("FK_campaigns_brokers");
            });

            modelBuilder.Entity<Click>(entity =>
            {
                entity.Property(e => e.AdditinalParameter).IsUnicode(false);
                entity.Property(e => e.AffId).IsUnicode(false);
                entity.Property(e => e.City).IsUnicode(false);
                entity.Property(e => e.Country).IsUnicode(false);
                entity.Property(e => e.Ip).IsUnicode(false);
                entity.Property(e => e.OfferId).IsUnicode(false);
                entity.Property(e => e.Referrel).IsUnicode(false);
                entity.Property(e => e.UserAgent).IsUnicode(false);
            });

            modelBuilder.Entity<Offer>(entity =>
            {
                entity.Property(e => e.Id)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<Routing>(entity =>
            {
                entity.Property(e => e.AffId).IsUnicode(false);
                entity.Property(e => e.Countries).IsUnicode(false);
                entity.Property(e => e.OfferId).IsUnicode(false);

                //entity.HasOne(d => d.Broker)
                //    .WithMany(p => p.Routing)
                //    .HasForeignKey(d => d.BrokerId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK_aff_brokers_brokers");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.AdditinalParameter).IsUnicode(false);
                entity.Property(e => e.AffId).IsUnicode(false);
                entity.Property(e => e.Country).IsUnicode(false);
                entity.Property(e => e.Email).IsUnicode(false);
                entity.Property(e => e.Ip).IsUnicode(false);
                entity.Property(e => e.OfferId).IsUnicode(false);
                entity.Property(e => e.Phone).IsUnicode(false);
                entity.Property(e => e.Referrer).IsUnicode(false);
                entity.Property(e => e.UserAgent).IsUnicode(false);
            });

            modelBuilder.Entity<Mail>(entity =>
            {
                entity.Property(e => e.Country).IsUnicode(false);
                entity.Property(e => e.Ip).IsUnicode(false);
                entity.Property(e => e.Email).IsUnicode(false);
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.CountryName).IsUnicode(false);
                entity.Property(e => e.CountryIso).IsUnicode(false);

                entity.HasKey(e => e.CountryIso);
            });

            modelBuilder.Entity<BrokerPayment>(entity =>
            {
                entity.Property(e=>e.Country).IsUnicode(false);
            });

            modelBuilder.Entity<AffiliatePayment>(entity =>
            {
                entity.Property(e => e.Country).IsUnicode(false);
            });
        }
    }
}
