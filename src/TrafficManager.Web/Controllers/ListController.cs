﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/list")]
    [Authorize]
    public class ListController : Controller
    {
        private IRepository<Country> _countriesRepo;
        private IRepository<Broker> _brokersRepo;
        private IRepository<Affilate> _affiliatesrsRepo;
        private IRepository<Offer> _offersRepo;

        public ListController(
            IRepository<Country> countriesRepo,
            IRepository<Broker> brokersRepo,
            IRepository<Affilate> affiliatesrsRepo,
            IRepository<Offer> offersRepo)
        {
            _countriesRepo = countriesRepo;
            _brokersRepo = brokersRepo;
            _affiliatesrsRepo = affiliatesrsRepo;
            _offersRepo = offersRepo;
        }

        [HttpGet("countries")]
        public IEnumerable<Country> GetCountries()
        {
            return _countriesRepo.Data.OrderBy(i => i.CountryName);
        }

        [HttpGet("brokers")]
        public IEnumerable<ListDto> GetBrokers()
        {
            return _brokersRepo.Data.Select(b => new ListDto { Id = b.Id.ToString(), Name = b.Name, IsActive = b.Active.Value }).OrderByDescending(i => i.IsActive).ThenBy(i => i.Name);
        }

        [HttpGet("affiliates")]
        public IEnumerable<ListDto> GetAffiliates()
        {
            return _affiliatesrsRepo.Data.Select(a => new ListDto { Id = a.Id, Name = a.Name }).OrderBy(i => i.Name);
        }

        [HttpGet("offers")]
        public IEnumerable<ListDto> GetOffers()
        {
            return _offersRepo.Data.Select(a => new ListDto { Id = a.Id, Name = a.Name }).OrderBy(i => i.Name);
        }
    }
}