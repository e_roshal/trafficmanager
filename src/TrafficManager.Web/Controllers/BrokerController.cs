﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;
using TrafficManager.Web.Services;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Broker")]
    [Authorize]
    public class BrokerController : Controller
    {
        private IBrokerService _brokerService;

        public BrokerController(IBrokerService brokerService)
        {
            this._brokerService = brokerService;
        }

        // GET: api/Broker
        [HttpGet]
        public WebResult<IEnumerable<BrokerDTO>> Get(BrokersFilterDto filters)
        {
            var resultData = _brokerService.GetByFilters(filters, out int totalItems);

            var result = new WebResult<IEnumerable<BrokerDTO>>()
            {
                Data = resultData,
                Success = true,
                PageNumber = filters.PageNumber?? 0,
                PageSize = filters.PageSize ?? 0,
                TotalItems = totalItems
            };
            return result;
        }

        // GET: api/Broker/5
        [HttpGet("{id}", Name = "Get")]
        public BrokerDTO Get(long id)
        {
            var broker = _brokerService.Get(b=>b.Id == id).FirstOrDefault();
            if (broker == null) return null;

            return new BrokerDTO()
            {
                Countries = broker.Countries,
                Limit = broker.Limit,
                Name = broker.Name,
                Id = broker.Id,
                Active = broker.Active,
                BlockedCountries = broker.BlockedCountries
            };
        }
        
        // POST: api/Broker
        [HttpPost]
        public OperationResult<Broker> Post([FromBody]Broker broker)
        {
            var result = _brokerService.Add(broker);
            return result;
        }
        
        // PUT: api/Broker/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]BrokerDTO brokerData)
        {
            var broker = _brokerService.Get(b => b.Id == brokerData.Id).FirstOrDefault();
            if (broker == null)
            {
                return NotFound($"No broker with id [{brokerData.Id}] found");
            }

            broker.Active = brokerData.Active;
            broker.Limit = brokerData.Limit;
            broker.Countries = brokerData.Countries;
            broker.BlockedCountries = brokerData.BlockedCountries;
            broker.Priority = brokerData.Priority;
            var updateResult = _brokerService.Update(broker);
            var result = new OperationResult<BrokerDTO>()
            {
                Success = updateResult.Success,
                Messages = updateResult.Messages,
                Data = new BrokerDTO()
                {
                    Countries = updateResult.Data.Countries,
                    BlockedCountries = updateResult.Data.BlockedCountries,
                    Limit =  updateResult.Data.Limit,
                    Id = updateResult.Data.Id,
                    Name = updateResult.Data.Name,
                    Active = updateResult.Data.Active,
                    Priority = updateResult.Data.Priority
                }
            };     
            return Ok(result);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
