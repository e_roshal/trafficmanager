﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Routing")]
    [Authorize]
    public class RoutingController : Controller
    {
        private IRoutingService _routingSvc;

        public RoutingController(IRoutingService routingSvc)
        {
            this._routingSvc = routingSvc;
        }

        [HttpGet]
        public IActionResult Get(RoutingFilterDto filters)
        {
            var data = _routingSvc.GetByFilters(filters, out int totalItems);

            var result = new WebResult<IEnumerable<RoutingResponseDto>>()
            {
              Data = data,
              Success = true,
              PageNumber = filters.PageNumber ?? 0,
              PageSize = filters.PageSize ?? 0,
              TotalItems = totalItems
            };

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Routing routing)
        {
	        var errors = _routingSvc.Validate(routing);
            if (!errors.Any())
            {
                var result = _routingSvc.Add(routing);

                if (result.Success)
                {
                    return Ok(result);
                }
                else
                {
                    return new ObjectResult(result.Messages)
                    { StatusCode = 500 };
                }
            }

            return BadRequest(errors);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Routing routing)
        {
	        if (!_routingSvc.Get(x => x.Id == id).Any())
            {
                return NotFound($"No Routing with id [{id}]");
            }

			var errors = _routingSvc.Validate(routing);
            if (!errors.Any())
            {
                var result = _routingSvc.Update(routing);

                if (result.Success)
                {
                    return Ok(result);
                }
                else
                {
                    return new ObjectResult(result.Messages)
                    { StatusCode = 500 };
                }
            }

            return BadRequest(errors);
        }


        [HttpDelete]
        public IActionResult Delete(string ids)
        {
            var routingIdsToDelete = string.IsNullOrWhiteSpace(ids)
                    ? new List<int>()
                    : ids
                        .Replace(" ","")
                        .Split(new []{','},StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse);


            var before = _routingSvc.Get().Count();
            foreach (var id in routingIdsToDelete)
            {
                var result = Delete(id);
            }
            var after = _routingSvc.Get().Count();

            return Ok(before - after);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var routing = _routingSvc.Get(x => x.Id == id).FirstOrDefault();

            if (routing == null)
                return NotFound($"No item with id [{id}]");

            return Ok(_routingSvc.Delete(routing));
        }
    }
}