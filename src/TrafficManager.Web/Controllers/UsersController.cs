﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private IBrokersUsersService _brokersUsersService;
       

        public UsersController(IBrokersUsersService borkersUserService)
        {
            this._brokersUsersService = borkersUserService;
        }

        [HttpGet]
        public IActionResult Get(UsersFilterDto filters)
        {
            
            var responseData = _brokersUsersService.GetByFilters(filters, out int totalItems);

            var result = new WebResult()
            {
                Data = responseData.ToList(),
                PageSize = filters.PageSize ?? 0,
                PageNumber = filters.PageNumber ?? 0,
                TotalItems = totalItems,
                Success = true
            };

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Post([FromBody] UsersToBrokerDto usersToBroker)
        {
            var userIds = usersToBroker.UserIds.Trim().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            var ids = new List<long>();
            foreach (var id in userIds)
            {
                long parsedId;
                if (long.TryParse(id, out parsedId))
                {
                    ids.Add(parsedId);
                }
            }
            if (!ids.Any() || usersToBroker.BrokerId < 1)
                return BadRequest("No user Id`s or Broker Id");

            var result = this._brokersUsersService.UpdateBrokerForUsers(ids, usersToBroker.BrokerId);

            return Ok(result);
        }
    }
}