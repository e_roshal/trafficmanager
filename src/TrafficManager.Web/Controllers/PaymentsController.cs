﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Payments")]
    public class PaymentsController : Controller
    {
        private readonly IPaymentsService<BrokerPayment> _brkrPaymentSvc;
        private readonly IPaymentsService<AffiliatePayment> _affPaymentSvc;

        public PaymentsController(IPaymentsService<BrokerPayment> brkrPaymentSvc, IPaymentsService<AffiliatePayment> affPaymentSvc)
        {
            this._brkrPaymentSvc = brkrPaymentSvc;
            this._affPaymentSvc = affPaymentSvc;
        }
        
        [Route("affiliate")]
        [HttpGet]
        public IActionResult Get(PaymentFilterDTO filter)
        {
            var result = this._affPaymentSvc.GetByFilters(filter, out int totalItems);
            if(result.Success)
            {
                return Ok(result);
            }
            return BadRequest(filter);
        }

        [Route("affiliate/{Id}")]
        [HttpGet]
        public IActionResult GetAffPayment(long Id)
        {
            var result = this._affPaymentSvc.Get(x => x.Id == Id).FirstOrDefault();
            if(result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("broker")]
        [HttpGet]
        public IActionResult GetBrkrPayments(PaymentFilterDTO filter)
        {
            var result = this._brkrPaymentSvc.GetByFilters(filter, out int totalItems);
            if (result.Success)
            {
                return Ok(result);
            }
            return BadRequest(filter);
        }

        [Route("broker/{Id}")]
        [HttpGet]
        public IActionResult GetBrkrPayment(long Id)
        {
            var result = this._brkrPaymentSvc.Get(x => x.Id == Id).FirstOrDefault();
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("affiliate")]
        [HttpPost]
        public IActionResult AddAffPayment([FromBody] AffiliatePayment payment)
        {
            var validationResult = this._affPaymentSvc.Validate(payment);

            if (validationResult.Success)
            {
                var result = this._affPaymentSvc.Add(payment);
                return Ok(result);
            }

            return BadRequest(validationResult);
        }

        [Route("broker")]
        [HttpPost]
        public IActionResult AddBrkrPayment([FromBody] BrokerPayment payment)
        {
            var validationResult = this._brkrPaymentSvc.Validate(payment);

            if (validationResult.Success)
            {
                var result = this._brkrPaymentSvc.Add(payment);
                return Ok(result);
            }

            return BadRequest(validationResult);
        }

        [Route("affiliate/{Id}")]
        [HttpPut]
        public IActionResult UpdateAffPayment(long Id, [FromBody] AffiliatePayment payment)
        {
            if(Id != payment.Id)
            {
                return BadRequest(new OperationResult
                {
                    Success = false,
                    Messages = new[] { "Id is not equal payment.Id" },
                    Data = payment
                });
            }

            if (!this._affPaymentSvc.Get(x => x.Id == Id).Any())
            {
                return NotFound($"No payment with id = [{Id}] found");
            }

            var validationResult = this._affPaymentSvc.Validate(payment);

            if (validationResult.Success)
            {
                var result = this._affPaymentSvc.Update(payment);
                return Ok(result);
            }

            return BadRequest(validationResult);
        }

        [Route("broker/{Id}")]
        [HttpPut]
        public IActionResult UpdateBrkrPayment(long Id, [FromBody] BrokerPayment payment)
        {
            if (Id != payment.Id)
            {
                return BadRequest(new OperationResult
                {
                    Success = false,
                    Messages = new[] { "Id is not equal payment.Id" },
                    Data = payment
                });
            }

            if (!this._brkrPaymentSvc.Get(x => x.Id == Id).Any())
            {
                return NotFound($"No payment with id = [{Id}] found");
            }

            var validationResult = this._brkrPaymentSvc.Validate(payment);

            if (validationResult.Success)
            {
                var result = this._brkrPaymentSvc.Update(payment);
                return Ok(result);
            }

            return BadRequest(validationResult);
        }

        [Route("affiliate/{Id}")]
        [HttpDelete]
        public IActionResult RemoveAffPayment(long Id)
        {
            var existingItem = _affPaymentSvc.Get(p => p.Id == Id).FirstOrDefault();

            if (existingItem == null)
            {
                return NotFound($"No payment with id = [{Id}] found");
            }

            var result = _affPaymentSvc.Delete(existingItem);
            if (result.Success)
            {
                return NoContent();
            }

            return BadRequest();
        }

     

        [Route("affiliate")]
        [HttpDelete]
        public IActionResult RemoveAffPayments(string Ids)
        {
            var idList = Ids.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var idNumbers = idList.Select(int.Parse).ToArray();

            if (idNumbers != null)
            {
                foreach (var Id in idNumbers)
                {
                    var existingItem = _affPaymentSvc.Get(p => p.Id == Id).FirstOrDefault();

                    if (existingItem == null)
                    {
                        continue;
                    }

                    var result = _affPaymentSvc.Delete(existingItem);
                    if (!result.Success)
                    {
                        return StatusCode(500);
                    }
                }
                return NoContent();
            }
            return BadRequest();
        }

        [Route("broker/{Id}")]
        [HttpDelete]
        public IActionResult RemoveBrkrPayment(long Id)
        {
            var existingItem = _brkrPaymentSvc.Get(p => p.Id == Id).FirstOrDefault();

            if (existingItem == null)
            {
                return NotFound($"No payment with id = [{Id}] found");
            }

            var result = _brkrPaymentSvc.Delete(existingItem);
            if (result.Success)
            {
                return NoContent();
            }

            return BadRequest();
        }

        [Route("broker")]
        [HttpDelete]
        public IActionResult RemoveBrkrPayments(string Ids)
        {
            var idList = Ids.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var idNumbers = idList.Select(int.Parse).ToArray();

            if (idNumbers != null)
            {
                foreach (var Id in idNumbers)
                {
                    var existingItem = _brkrPaymentSvc.Get(p => p.Id == Id).FirstOrDefault();

                    if (existingItem == null)
                    {
                        continue;
                    }

                    var result = _brkrPaymentSvc.Delete(existingItem);
                    if (!result.Success)
                    {
                        return StatusCode(500);
                    }
                }
                return NoContent();
            }
            return BadRequest();
        }
    }
}