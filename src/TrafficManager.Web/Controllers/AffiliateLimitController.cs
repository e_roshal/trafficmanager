﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/affiliate-limit")]
    [Authorize]
    public class AffiliateLimitController : Controller
    {
        private readonly IAffiliateLimitService _affiliateLimitService;

        public AffiliateLimitController (
            IAffiliateLimitService affiliateLimitService)
        {
            _affiliateLimitService = affiliateLimitService;
        }

        [HttpGet]
        public WebResult<IEnumerable<AffiliateCountryLimit>> Get(AffiliateCountryLimitFilterDto filters)
        {
            var resultData = _affiliateLimitService.GetByFilters(filters, out int totalItems);

            var result = new WebResult<IEnumerable<AffiliateCountryLimit>>()
            {
                Data = resultData,
                Success = true,
                PageNumber = filters.PageNumber ?? 0,
                PageSize = filters.PageSize ?? 0,
                TotalItems = totalItems
            };
            return result;
        }

        [HttpPost]
        public IActionResult Add([FromBody] AffiliateCountryLimit item)
        {
            var result = _affiliateLimitService.Add(item);

            if (result.Success)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] AffiliateCountryLimit item)
        {
            var result = _affiliateLimitService.Update(item);

            if (result.Success)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _affiliateLimitService.Delete(id);

            if (result.Success)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpDelete()]
        public IActionResult Delete(string ids)
        {
            var routingIdsToDelete = string.IsNullOrWhiteSpace(ids)
                ? new List<int>()
                : ids
                    .Replace(" ", "")
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse);


            var before = _affiliateLimitService.Get().Count();
            foreach (var id in routingIdsToDelete)
            {
                var result = Delete(id);
            }
            var after = _affiliateLimitService.Get().Count();

            return Ok(before - after);
        }

        [HttpPost("add-deposit")]
        public IActionResult AddDeposit([FromBody] AddAffiliateDepositsDto request)
        {
            
            var result = this._affiliateLimitService.AddDeposit(request.Ids);

            return Ok();
        }
    }
}