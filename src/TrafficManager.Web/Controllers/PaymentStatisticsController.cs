﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/payment_statistics")]
    public class PaymentStatisticsController : Controller
    {
        private readonly IPaymentStatisticsService _paymentStatSvc;

        public PaymentStatisticsController(IPaymentStatisticsService paymentStatSvc)
        {
            this._paymentStatSvc = paymentStatSvc;
        }

        [Route("affiliate")]
        [HttpGet]
        public IActionResult GetAffStats(PaymentStatsFilterDTO filter)
        {
            var result = this._paymentStatSvc.GetAffStats(filter, out int totalItems);
            if (result.Success)
            {
                return Ok(result);
            }
            return BadRequest(filter);
        }

        [Route("broker")]
        [HttpGet]
        public IActionResult GetBrkrStats(PaymentStatsFilterDTO filter)
        {
            var result = this._paymentStatSvc.GetBrkrStats(filter, out int totalItems);
            if (result.Success)
            {
                return Ok(result);
            }
            return BadRequest(filter);
        }

        [Route("country")]
        [HttpGet]
        public IActionResult GetCountryStats(PaymentStatsFilterDTO filter)
        {
            var result = this._paymentStatSvc.GetCountryStats(filter, out int totalItems);
            if (result.Success)
            {
                return Ok(result);
            }
            return BadRequest(filter);
        }
    }
}