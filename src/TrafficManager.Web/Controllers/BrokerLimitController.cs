﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/broker-limit")]
    [Authorize]
    public class BrokerLimitController : Controller
    {
        private readonly IBrokerLimitService _brokerLimitService;

        public BrokerLimitController(
            IBrokerLimitService brokerLimitService)
        {
            _brokerLimitService = brokerLimitService;
        }

        // GET: api/Broker
        [HttpGet]
        public WebResult<IEnumerable<BrokerCountryLimit>> Get(BrokerCountryLimitFilterDto filters)
        {
            var resultData = _brokerLimitService.GetByFilters(filters, out int totalItems);

            var result = new WebResult<IEnumerable<BrokerCountryLimit>>()
            {
                Data = resultData,
                Success = true,
                PageNumber = filters.PageNumber ?? 0,
                PageSize = filters.PageSize ?? 0,
                TotalItems = totalItems
            };
            return result;
        }

        [HttpPost]
        public IActionResult Add([FromBody] BrokerCountryLimit item)
        {
            var result = _brokerLimitService.Add(item);

            if (result.Success)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] BrokerCountryLimit item)
        {
            var result = _brokerLimitService.Update(item);

            if (result.Success)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var result = _brokerLimitService.Delete(id);

            if (result.Success)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpDelete()]
        public IActionResult Delete(string ids)
        {
            var routingIdsToDelete = string.IsNullOrWhiteSpace(ids)
                ? new List<long>()
                : ids
                    .Replace(" ", "")
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(long.Parse);


            var before = _brokerLimitService.Get().Count();
            foreach (var id in routingIdsToDelete)
            {
                var result = Delete(id);
            }
            var after = _brokerLimitService.Get().Count();

            return Ok(before - after);
        }
    }
}