﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/dynamics")]
    public class DashboardController : Controller
    {
        private IDynamicsService _dynamicsSvc;

        public DashboardController(IDynamicsService dynamicsSvc)
        {
            this._dynamicsSvc = dynamicsSvc;
        }

        [HttpGet]
        public IActionResult Get(DynamicsFilterDto filter)
        {
            var result = _dynamicsSvc.GetDynamics(filter);

            return Ok(result);
        }
    }
}