﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class UsersToBrokerDto
    {
        public string UserIds { get; set; }
        public long BrokerId { get; set; }
    }
}
