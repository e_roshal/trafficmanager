﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class PaymentFilterDTO : StandardFiltersDto
    {
        public long SubjectId { get; set; }
        public string Countries { get; set; }
        public short PaymentType { get; set; }
    }
}
