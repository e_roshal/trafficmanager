﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class BrokerDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }
        public int? Limit { get; set; }
        public byte? Priority { get; set; }
        public string Countries { get; set; }
        public string BlockedCountries { get; set; }
    }
}
