﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class AddAffiliateDepositsDto
    {
        public int[] Ids { get; set; }
    }
}
