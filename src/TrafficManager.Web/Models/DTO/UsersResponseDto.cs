﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class UsersResponseDto
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long BrokerId { get; set; }
        public string Broker { get; set; }
        public string AffId { get; set; }
        public string Affiliate { get; set; }
        public string Country { get; set; }
        public DateTime? LeadDate { get; set; }
        public DateTime? DepositDate { get; set; }
        public int? FTD { get; set; }
        public int? TotalDeposit { get; set; }

        public string CallStatus { get; set; }
        public string OfferId { get; set; }

        public string HasOfferId { get; set; }

        public UsersResponseDto()
        {
        }

        public UsersResponseDto(UsersResponseDto userResponse)
        {
            this.Id = userResponse.Id;
            this.UserId = userResponse.UserId;
            this.Email = userResponse.Email;
            this.Phone = userResponse.Phone;
            this.FirstName = userResponse.FirstName;
            this.LastName = userResponse.LastName;
            this.BrokerId = userResponse.BrokerId;
            this.Broker = userResponse.Broker;
            this.AffId = userResponse.AffId;
            this.Affiliate = userResponse.Affiliate;
            this.Country = userResponse.Country;
            this.LeadDate = userResponse.LeadDate;
            this.DepositDate = userResponse.DepositDate;
            this.FTD = userResponse.FTD;
            this.TotalDeposit = userResponse.TotalDeposit;
            this.CallStatus = userResponse.CallStatus;
            this.OfferId = userResponse.OfferId;
            this.HasOfferId = userResponse.HasOfferId;
        }
    }
}
