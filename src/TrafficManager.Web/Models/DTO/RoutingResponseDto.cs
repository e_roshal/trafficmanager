﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class RoutingResponseDto
    {
        public int Id { get; set; }
        public string AffId { get; set; }
        public string Affiliate { get; set; }
        public long BrokerId { get; set; }
        public string Broker { get; set; }
        public byte Priority { get; set; }
        public string OfferId { get; set; }
        public string Offer { get; set; }
        public string Countries { get; set; }
        public bool? Active { get; set; }
        public TimeSpan? From { get; set; }
        public TimeSpan? To { get; set; }
        public int? Limit { get; set; }

        public RoutingResponseDto()
        {
        }

        public RoutingResponseDto(RoutingResponseDto source)
        {
            this.Id = source.Id;
            this.AffId = source.AffId;
            this.Affiliate = source.Affiliate;
            this.BrokerId = source.BrokerId;
            this.Broker = source.Broker;
            this.Priority = source.Priority;
            this.OfferId = source.OfferId;
            this.Offer = source.Offer;
            this.Countries = source.Countries;
            this.Active = source.Active;
            this.Limit = source.Limit;
            this.From = source.From;
            this.To = source.To;

        }

    }
}
