﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class DynamicsItem
    {
        public DateTime? Date { get; set; }
        public double? Amount { get; set; }
    }
}
