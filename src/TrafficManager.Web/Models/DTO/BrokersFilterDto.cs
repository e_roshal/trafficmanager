﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class BrokersFilterDto : StandardFiltersDto
    {
        public string BrokerName { get; set; }
        public bool? IsActive { get; set; }
        public int? Limit { get; set; }
        public bool? IsGreater { get; set; }
        public string Countries { get; set; }
    }
}
