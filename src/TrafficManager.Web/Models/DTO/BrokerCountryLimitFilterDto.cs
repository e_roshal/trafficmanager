﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class BrokerCountryLimitFilterDto : StandardFiltersDto
    {
        public string BrokerName { get; set; }
        public string BrokerIds { get; set; }
        public int? Limit { get; set; }
        public bool? IsGreater { get; set; }
        public string Countries { get; set; }
    }
}
