﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class BrokerPaymentStatsDTO
    {
        public long BrokerId { get; set; }
        public int? Leads { get; set; }
        public double? Ratio { get; set; }
        public int? Deposites { get; set; }
        public double? ReportedRatio { get; set; }
        public int? ReportedDeposites { get; set; }
        public IEnumerable<string> BrokerAffiliates { get; set; }
        public int? Payments { get; set; }
        public int? Payouts { get; set; }
        public int? Profit { get; set; }

        public int? ReportedPayments { get; set; }
        public int? ReportedPayouts { get; set; }
        public int? ReportedProfit { get; set; }
    }
}
