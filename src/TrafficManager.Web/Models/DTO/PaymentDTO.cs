﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class PaymentDTO
    {
        public long Id { get; set; }
        public long SubjectId { get; set; }
        public string Country { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public int Amount { get; set; }
        public short PaymentType { get; set; }
    }
}
