﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class AffiliateCountryLimitFilterDto : StandardFiltersDto
    {
        public string AffiliateName { get; set; }
        public string AffiliateIds { get; set; }
        public double? Factor { get; set; }
        public double? Min { get; set; }
        public double? Max { get; set; }
        public bool? IsMinGreater { get; set; }
        public bool? IsMaxGreater { get; set; }
        public bool? IsFactorGreater { get; set; }
        public string Countries { get; set; }
    }
}
