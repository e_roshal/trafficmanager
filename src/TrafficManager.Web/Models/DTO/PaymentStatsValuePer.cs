﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class PaymentStatsValuePerAff
    {
        public string AffId { get; set; }
        public int Payout { get; set; }
        public int Count { get; set; }
    }

    public class PaymentStatsValuePerBrkr
    {
        public long BrokerId { get; set; }
        public int Payout { get; set; }
        public int Count { get; set; }
    }
}
