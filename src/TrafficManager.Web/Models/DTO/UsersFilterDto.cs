﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class UsersFilterDto :  StandardFiltersDto
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string FromDepositDate { get; set; }
        public string ToDepositDate { get; set; }
        public string BrokerIds { get; set; }
        public string AffiliateIds { get; set; }
        public string TextFilter { get; set; }

        public string Countries { get; set; }

    }
}
