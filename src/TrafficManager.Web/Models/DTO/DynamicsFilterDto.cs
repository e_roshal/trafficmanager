﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class DynamicsFilterDto
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}
