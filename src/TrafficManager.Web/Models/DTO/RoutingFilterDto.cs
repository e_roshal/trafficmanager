﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class RoutingFilterDto : StandardFiltersDto
    {
        public string BrokerIds { get; set; }
        public string OfferIds { get; set; }
        public string AffiliateIds { get; set; }
    }
}
