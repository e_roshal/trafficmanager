﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class BrokerUserWithPayments
    {
        public long UserId { get; set; }
        public DateTime LeadDate { get; set; }
        public DateTime? DepositDate { get; set; }
        public long AffiliateId { get; set; }
        public long BrokerId { get; set; }
        public string Country { get; set; }
        public int AffiliatePayment { get; set; }
        public int BrokerPayment { get; set; }

        public bool ApiReport { get; set; }
        public bool IsReported { get; set; }
        public bool IsDepAdded { get; set; }
    }
}
