﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.DTO
{
    public class DynamicsDto
    {
        public IEnumerable<DynamicsItem> Deposits { get; set; }
        public IEnumerable<DynamicsItem> Leads { get; set; }
    }
}
