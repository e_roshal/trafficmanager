﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.Data
{
    [Table("aff_payment")]
    public class AffiliatePayment
    {
        [Column("id")]
        public long Id { get; set; }
        [Column("aff_id")]
        public long AffiliateId { get; set; }
        [Column("country")]
        [StringLength(2)]
        public string Country { get; set; } = "";
        [Column("valid_from", TypeName = "datetime")]
        public DateTime ValidFrom { get; set; }
        [Column("valid_to", TypeName = "datetime")]
        public DateTime? ValidTo { get; set; }
        [Column("amount")]
        public int Amount { get; set; }
        [Column("payment_type")]
        public short PaymentType { get; set; }
    }
}
