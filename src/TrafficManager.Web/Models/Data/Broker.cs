﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("brokers")]
    public partial class Broker
    {
        public Broker()
        {
            //BrokersUsers = new HashSet<BrokersUsers>();
            //Campaigns = new HashSet<Campaign>();
            //Routing = new HashSet<Routing>();
        }

        [Column("id")]
        public long Id { get; set; }
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [Column("user_name")]
        [StringLength(50)]
        public string UserName { get; set; }
        [Required]
        [Column("password")]
        [StringLength(50)]
        public string Password { get; set; }
        [Column("start_date", TypeName = "date")]
        public DateTime StartDate { get; set; }
        [Column("end_date", TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [Column("active")]
        public bool? Active { get; set; }
        [Column("countries")]
        [StringLength(150)]
        public string Countries { get; set; }

        [Column("blocked_countries")]
        [StringLength(150)]
        public string BlockedCountries { get; set; }

        [Column("priority")]
        public byte? Priority { get; set; }
        [Column("limit")]
        public int? Limit { get; set; }
        [Column("last_sync", TypeName = "datetime")]
        public DateTime LastSync { get; set; }
        [Column("account_id")]
        [StringLength(20)]
        public string AccountId { get; set; }

        //[InverseProperty("Broker")]
        //public ICollection<BrokersUsers> BrokersUsers { get; set; }
        //[InverseProperty("Broker")]
        //public ICollection<Campaign> Campaigns { get; set; }
        //[InverseProperty("Broker")]
        //public ICollection<Routing> Routing { get; set; }
    }
}
