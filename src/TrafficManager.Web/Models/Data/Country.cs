﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("countries")]
    public class Country
    {
        [Column("country_name")]
        [StringLength(50)]
        public string CountryName { get; set; }

        [Column("country_iso")]
        [StringLength(2)]
        public string CountryIso { get; set; }

    }
}
