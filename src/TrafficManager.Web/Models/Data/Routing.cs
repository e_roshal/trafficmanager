﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("routing")]
    public partial class Routing
    {
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("aff_id")]
        [StringLength(50)]
        public string AffId { get; set; }
        [Column("broker_id")]
        public long BrokerId { get; set; }
        [Column("priority")]
        public byte Priority { get; set; }
        [Column("offer_id")]
        [StringLength(50)]
        public string OfferId { get; set; }
        [Column("countries")]
        [StringLength(150)]
        public string Countries { get; set; }
        [Column("from_time")]
        public TimeSpan? From { get; set; }
        [Column("to_time")]
        public TimeSpan? To { get; set; }
        [Column("active")]
        public bool? Active { get; set; }

        [Column("limit")]
        public int? Limit{ get; set; }

        //[ForeignKey("BrokerId")]
        //[InverseProperty("Routing")]
        //public Broker Broker { get; set; }
    }
}
