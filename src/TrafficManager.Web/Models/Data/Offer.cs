﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("offers")]
    public partial class Offer
    {
        [Column("id")]
        [StringLength(50)]
        public string Id { get; set; }
        [Required]
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }
    }
}
