﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("mails")]
    public class Mail
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("mail")]
        [StringLength(150)]
        public string Email { get; set; }

        [Column("ip")]
        [StringLength(50)]
        public string Ip { get; set; }

        [Column("country")]
        [StringLength(50)]
        public string Country { get; set; }

        [Column("date", TypeName = "datetime")]
        public DateTime Date { get; set; }
    }
}
