﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.Data
{
    [Table("broker_country_limit")]
    public class BrokerCountryLimit
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("broker_id")]
        public long BrokerId { get; set; }

        [Column("countries")]
        public string Countries { get; set; }

        [Column("limit")]
        public int Limit { get; set; }
    }
}
