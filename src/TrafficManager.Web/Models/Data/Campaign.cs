﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("campaigns")]
    public partial class Campaign
    {
        [Column("id")]
        public long Id { get; set; }
        [Column("cmp_id")]
        [StringLength(50)]
        public string CmpId { get; set; }
        [Column("cmp_name")]
        [StringLength(50)]
        public string CmpName { get; set; }
        [Column("language")]
        [StringLength(50)]
        public string Language { get; set; }
        [Column("broker_id")]
        public long? BrokerId { get; set; }

        //[ForeignKey("BrokerId")]
        //[InverseProperty("Campaign")]
        //public Broker Broker { get; set; }
    }
}
