﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("clicks")]
    public partial class Click
    {
        [Column("id")]
        public long Id { get; set; }
        [Required]
        [Column("offer_id")]
        [StringLength(50)]
        public string OfferId { get; set; }
        [Column("date", TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Column("ip")]
        [StringLength(50)]
        public string Ip { get; set; }
        [Column("country")]
        [StringLength(50)]
        public string Country { get; set; }
        [Column("city")]
        [StringLength(50)]
        public string City { get; set; }
        [Column("referrel")]
        [StringLength(150)]
        public string Referrel { get; set; }
        [Column("user_agent")]
        [StringLength(150)]
        public string UserAgent { get; set; }
        [Column("aff_id")]
        [StringLength(50)]
        public string AffId { get; set; }
        [Column("additinal_parameter")]
        [StringLength(50)]
        public string AdditinalParameter { get; set; }
    }
}
