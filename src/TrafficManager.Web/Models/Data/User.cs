﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("users")]
    public partial class User
    {
        public User()
        {
            //BrokersUsers = new HashSet<BrokersUsers>();
        }

        [Column("id")]
        public long Id { get; set; }
        [Column("email")]
        [StringLength(50)]
        public string Email { get; set; }
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }
        [Column("second_name")]
        [StringLength(50)]
        public string SecondName { get; set; }
        [Column("phone")]
        [StringLength(50)]
        public string Phone { get; set; }
        [Column("country")]
        [StringLength(50)]
        public string Country { get; set; }
        [Required]
        [Column("offer_id")]
        [StringLength(50)]
        public string OfferId { get; set; }
        [Column("ip")]
        [StringLength(50)]
        public string Ip { get; set; }
        [Column("referrer")]
        [StringLength(200)]
        public string Referrer { get; set; }
        [Column("user_agent")]
        [StringLength(200)]
        public string UserAgent { get; set; }
        [Column("additinal_parameter")]
        [StringLength(50)]
        public string AdditinalParameter { get; set; }
        [Column("aff_id")]
        [StringLength(50)]
        public string AffId { get; set; }

        [Column("send_to_broker")]
        public long? SendtoBroker { get; set; }

        //[InverseProperty("User")]
        //public ICollection<BrokersUsers> BrokersUsers { get; set; }
    }
}
