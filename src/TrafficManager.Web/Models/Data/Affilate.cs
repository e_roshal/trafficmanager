﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("affilates")]
    public partial class Affilate
    {
        [Column("id")]
        [StringLength(50)]
        public string Id { get; set; }
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }
        [Column("factor")]
        public int? Factor { get; set; }
        [Column("is_add_dep")]
        public bool? IsAddDep { get; set; }
    }
}
