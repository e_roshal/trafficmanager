﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Models.Data
{
    [Table("aff_cr")]
    public class AffiliateCountryLimit
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("aff_id")]
        public string AffiliateId { get; set; }

        [Column("min_cr")]
        public double? Min { get; set; }

        [Column("max_cr")]
        public double? Max { get; set; }

        [Column("country")]
        public string Countries { get; set; }

        [Column("cr_type")]
        public string Type { get; set; }

        [Column("cr_factor")]
        public double? Factor { get; set; }

        [Column("is_add_dep")]
        public bool? IsAddDep { get; set; }
    }
}
