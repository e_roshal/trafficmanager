﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrafficManager.Web.Models.Data
{
    [Table("brokers_users")]
    public partial class BrokersUsers
    {
        [Column("id")]
        public long Id { get; set; }
        [Column("user_id")]
        public long UserId { get; set; }
        [Column("lead_date", TypeName = "datetime")]
        public DateTime? LeadDate { get; set; }
        [Column("broker_id")]
        public long BrokerId { get; set; }
        [Column("guid")]
        [StringLength(50)]
        public string Guid { get; set; }
        [Column("id2")]
        [StringLength(70)]
        public string Id2 { get; set; }
        [Column("amount")]
        public double? Amount { get; set; }
        [Column("currency")]
        [StringLength(10)]
        public string Currency { get; set; }
        [Column("deposit_date", TypeName = "datetime")]
        public DateTime? DepositDate { get; set; }
        [Column("call_status")]
        [StringLength(50)]
        public string CallStatus { get; set; }
        [Column("offer_id")]
        [StringLength(50)]
        public string OfferId { get; set; }
        [Column("aff_id")]
        [StringLength(50)]
        public string AffId { get; set; }
        [Column("conversion_sent")]
        public bool? ConversionSent { get; set; }
        [Column("deposit_sent")]
        public bool? DepositSent { get; set; }

        [Column("total_amount")]
        public double? TotalAmount { get; set; }

        [Column("api_report")]
        public bool? ApiReport { get; set; }
        [Column("is_reported")]
        public bool? IsReported { get; set; }
        [Column("auto_login")]
        public string AutoLogin { get; set; }
        [Column("password")]
        public string Password{ get; set; }
        [Column("prev_call_status")]
        public string PrevCallStatus { get; set; }
        [Column("is_dep_added")]
        public bool? IsDepAdded { get; set; }

        //[ForeignKey("BrokerId")]
        //[InverseProperty("BrokersUsers")]
        //public Broker Broker { get; set; }
        //[ForeignKey("UserId")]
        //[InverseProperty("BrokersUsers")]
        //public User User { get; set; }
    }
}
