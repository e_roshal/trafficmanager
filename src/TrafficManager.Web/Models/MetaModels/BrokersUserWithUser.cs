﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;

namespace TrafficManager.Web.Models.MetaModels
{
    public class BrokersUserWithUser
    {
        public BrokersUsers BrokersUser { get; set; }
        public User User { get; set; }
    }
}
