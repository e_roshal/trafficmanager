﻿using System.Collections.Generic;

namespace TrafficManager.Web.Models
{
    public class WebResult : OperationResult
    {
        public long TotalItems { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }

    public class WebResult<T> : OperationResult<T>
    {
        public long TotalItems { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; } 
    }
}
