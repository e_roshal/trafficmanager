﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Services
{
    public class RoutingService : IRoutingService
    {
        private Dictionary<string, Expression<Func<RoutingResponseDto, object>>> orderBys = new Dictionary<string, Expression<Func<RoutingResponseDto, object>>>()
        {
            {"affId", x => x.Affiliate},
            {"offerId", x=>x.Offer},
            {"brokerId", x=>x.Broker},
            {"priority", x=>x.Priority},
            {"active", x=>x.Active},
            {"limit", x=>x.Limit}
        };

        private IRepository<Routing> _routingRepo;
        private IRepository<Broker> _brokersRepo;
        private IRepository<Affilate> _affiliatesRepo;
        private IRepository<Offer> _offersRepo;

        public RoutingService(
            IRepository<Routing> repo,
            IRepository<Broker> brokersRepo,
            IRepository<Affilate> affiliatesRepo,
            IRepository<Offer> offersRepo)
        {
            this._routingRepo = repo;
            this._affiliatesRepo = affiliatesRepo;
            this._offersRepo = offersRepo;
            this._brokersRepo = brokersRepo;
        }

        public IQueryable<Routing> Get()
        {
            return _routingRepo.Data;
        }

        public IQueryable<Routing> Get(Expression<Func<Routing, bool>> func)
        {
            return _routingRepo.Data.Where(func);
        }

        public OperationResult<Routing> Add(Routing item)
        {
            var result = new OperationResult<Routing>()
            {
                Data = _routingRepo.Add(item),
                Success = true
            };

            return result;
        }

        public OperationResult<Routing> Update(Routing item)
        {
            var result = new OperationResult<Routing>()
            {
                Data = _routingRepo.Update(item),
                Success = true
            };

            return result;
        }

        public OperationResult<Routing> Delete(Routing item)
        {
            var result = new OperationResult<Routing>()
            {
                Data = _routingRepo.Delete(item),
                Success = true
            };

            return result;
        }

        public IQueryable<RoutingResponseDto> GetByFilters(RoutingFilterDto filters, out int totalItems)
        {
            var data = _routingRepo.Data;
            data = string.IsNullOrWhiteSpace(filters.BrokerIds)
                ? data
                : data.Where(r => filters.BrokerIds
                    .Replace(" ", "")
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Contains(r.BrokerId.ToString()));

            data = string.IsNullOrWhiteSpace(filters.OfferIds)
                ? data
                : data.Where(r => filters.OfferIds
                    .Replace(" ", "")
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Contains(r.OfferId)
                );

            data = string.IsNullOrWhiteSpace(filters.AffiliateIds)
                ? data
                : data.Where(r => filters.AffiliateIds
                    .Replace(" ", "")
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Contains(r.AffId)
                );

            var resultData = from d in data
                             join b in this._brokersRepo.Data
                                 on d.BrokerId equals b.Id into temp
                             from x in temp.DefaultIfEmpty()
                             select new RoutingResponseDto()
                             {
                                 Id = d.Id,
                                 BrokerId = x.Id,
                                 Broker = x.Name,
                                 Countries = d.Countries,
                                 Active = d.Active,
                                 AffId = d.AffId,
                                 OfferId = d.OfferId,
                                 To = d.To,
                                 From = d.From,
                                 Priority = d.Priority,
                                 Limit = d.Limit
                             };


            resultData = from rd in resultData
                         join a in this._affiliatesRepo.Data
                                on rd.AffId equals a.Id into temp
                         from x in temp.DefaultIfEmpty()
                         select new RoutingResponseDto(rd)
                         {
                             Affiliate = x.Name
                         };

            //resultData = resultData.Join(this._affiliatesRepo.Data, rd => rd.AffId, a => a.Id.DefaultIfEmpty(), (rd, a) =>
            //    new RoutingResponseDto(rd)
            //    {
            //        Affiliate = a.Name
            //    });



            resultData = from rd in resultData
                         join o in this._offersRepo.Data
                             on rd.OfferId equals o.Id into temp
                         from x in temp.DefaultIfEmpty()
                         select new RoutingResponseDto(rd)
                         {
                             OfferId = rd.OfferId,
                             Offer = string.IsNullOrWhiteSpace(x.Name) ? rd.OfferId : x.Name
                         };
            //resultData = resultData.Join(this._offersRepo.Data, rd => rd.OfferId, @o => o.Id, (rd, o) =>
            //    new RoutingResponseDto(rd)
            //    {
            //        Offer = o.Name
            //    }).DefaultIfEmpty();

            if (!string.IsNullOrWhiteSpace(filters.OrderByField) && orderBys.ContainsKey(filters.OrderByField))
            {
                var orderExp = orderBys[filters.OrderByField];

                resultData = filters.IsDesc
                    ? resultData.OrderByDescending(orderExp)
                    : resultData.OrderBy(orderExp);
            }


            filters.PageNumber = filters.PageNumber ?? 0;
            totalItems = data.Count();

            resultData = filters.PageSize == null
                ? resultData
                : resultData.Skip((int)(filters.PageNumber)
                            * (int)filters.PageSize)
                    .Take((int)filters.PageSize);

            return resultData;
        }

        public List<string> Validate(Routing routing)
        {
            if (string.IsNullOrWhiteSpace(routing.Countries))
            {
                routing.Countries = "|**|";
            }

            if (routing.OfferId == "")
            {
                routing.OfferId = null;
            }

            var list = new List<string>();
            var isOk = long.TryParse(routing.AffId, out long affId);
            if (affId < 0)
            {
                list.Add($"Wrong affilate Id [{routing.AffId}]");
            }

            isOk = long.TryParse(routing.OfferId, out long offerId);
            if (offerId < 0)
            {
                list.Add($"Wrong offer Id [{routing.OfferId}]");
            }

            if (routing.BrokerId < 1)
            {
                list.Add($"Wrong broker Id [{routing.BrokerId}]");
            }

            if (affId < 1 && offerId < 1)
            {
                list.Add("Affiliate Id and offer Id can not be both empty same time");
            }

            if (routing.Priority < 1)
            {
                list.Add($"Priority should be more than zero");
            }


            ////  if we have all this fields
            //if (routing.To != null &&
            //    routing.From != null)
            //{
            //    var routingDTO = new
            //    {
            //        Countries = routing.Countries,
            //        To = routing.To,
            //        From = routing.From,
            //        OfferId = routing.OfferId,
            //        AffilateId = routing.AffId,
            //        BrokerId = routing.BrokerId
            //    };

            //    if (routingDTO.From != routingDTO.To)
            //    {
            //        var existingRoutes = _routingRepo.Data.Where(x =>
            //            x.To != null &&  // not old records 
            //            x.From != null && // because before there was no 
            //            x.Countries != null && // 'to', 'from', 'countries' fields 
            //            x.OfferId == routingDTO.OfferId &&
            //            x.AffId == routingDTO.AffilateId &&
            //            x.BrokerId == routingDTO.BrokerId &&
            //            x.Id != routing.Id); // not the same record

            //        var existingRoutesToValidate = new List<Routing>();

            //        if (routingDTO.Countries.Contains("**"))
            //        {
            //            existingRoutesToValidate = existingRoutes.ToList();
            //        }
            //        else
            //        {
            //            // take only those database records where we have at leas one same country
            //            foreach (var record in existingRoutes)
            //            {
            //                if (record.Countries.Contains("**"))
            //                {
            //                    existingRoutesToValidate.Add(record);
            //                    continue;
            //                }

            //                // if we already added to list skip it
            //                // if we have intersection by country add to lt
            //                if (record.Countries != routingDTO.Countries) continue;

            //                existingRoutesToValidate.Add(record);
            //            }
            //        }


            //        for (var i = 0; i < existingRoutesToValidate.Count; i++)
            //        {
            //            // is 'From' of new record greater than To of existing record
            //            var fromNewGtToEx = routingDTO.From > existingRoutesToValidate[i].To;
            //            var toNewGtToEx = routingDTO.To > existingRoutesToValidate[i].To;
            //            var fromNewLtToNew = routingDTO.From < routingDTO.To;


            //            if (fromNewGtToEx &&
            //                toNewGtToEx &&
            //                fromNewLtToNew)
            //            {
            //                // everything is ok for right side of the span
            //            }
            //            else
            //            {

            //                var toNewLtFromEx = existingRoutesToValidate[i].From > routingDTO.To;
            //                var toNewGtFromNew = routingDTO.To > routingDTO.From;

            //                if ((toNewLtFromEx && toNewGtFromNew) ||
            //                    (fromNewGtToEx && toNewLtFromEx))
            //                {
            //                    // everything is ok for the left side of span
            //                }
            //                else
            //                {
            //                    // we have an intersection with existing route
            //                    list.Add($"Routing has an intersection with routing id = [{existingRoutesToValidate[i].Id}]");
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        list.Add($"The start and end time of the routing can not be the same");
            //    }
            //}
            //else
            //{
            //    // if we have empty from and to fields
            //    if (routing.From == null && routing.To == null)
            //    {
            //        var records = _routingRepo.Data.Where(x =>
            //             x.From == null && x.To == null && x.AffId == routing.AffId && x.OfferId == routing.OfferId &&
            //             x.BrokerId == routing.BrokerId);

            //        foreach (var record in records)
            //        {
            //            if (record.Countries == routing.Countries)
            //            {

            //                list.Add($"Such record already exists. Id = [{ record.Id}]");
            //                break;
            //            }
            //        }

            //    }
            //    else
            //    {
            //        list.Add($"One of the fields from or to is empty");
            //    }

            //}
            return list;
        }
    }
}
