﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Services
{
    public class PaymentStatisticsService : IPaymentStatisticsService
    {
        private Dictionary<string, Expression<Func<AffPaymentStatsDTO, object>>> orderAffBys = new Dictionary<string, Expression<Func<AffPaymentStatsDTO, object>>>()
        {
            {"affiliate", x => x.AffiliateId},
            {"leads", x => x.Leads},
            {"ratio", x => x.Ratio},
            {"deposites", x => x.Deposites},
            {"reportedRatio", x => x.ReportedRatio},
            {"reportedDeposites", x => x.ReportedDeposites},
            {"brokers", x => x.AffiliateBrokers},
            {"payments", x => x.Payments},
            {"profit", x => x.Profit}
        };
        private Dictionary<string, Expression<Func<BrokerPaymentStatsDTO, object>>> orderBrkrBys = new Dictionary<string, Expression<Func<BrokerPaymentStatsDTO, object>>>()
        {
            {"broker", x => x.BrokerId},
            {"leads", x => x.Leads},
            {"ratio", x => x.Ratio},
            {"deposites", x => x.Deposites},
            {"reportedRatio", x => x.ReportedRatio},
            {"reportedDeposites", x => x.ReportedDeposites},
            {"affiliates", x => x.BrokerAffiliates},
            {"payments", x => x.Payments},
            {"profit", x => x.Profit}
        };
        private Dictionary<string, Expression<Func<CountyPaymentStatsDTO, object>>> orderCountryBys = new Dictionary<string, Expression<Func<CountyPaymentStatsDTO, object>>>()
        {
            {"country", x => x.Country},
            {"broker", x => x.Brokers},
            {"leads", x => x.Leads},
            {"ratio", x => x.Ratio},
            {"reportedRatio", x => x.ReportedRatio},
            {"reportedDeposites", x => x.ReportedDeposites},
            {"deposites", x => x.Deposites},
            {"affiliates", x => x.Affiliates},
            {"payments", x => x.Payments},
            {"profit", x => x.Profit}
        };


        private readonly OVODbContext _context;


        public PaymentStatisticsService(OVODbContext context)
        {
            _context = context;
        }

        public WebResult<IEnumerable<AffPaymentStatsDTO>> GetAffStats(PaymentStatsFilterDTO filter, out int totalItems)
        {
            DateTime dateFrom;
            DateTime dateTo;
            if (DateTime.TryParse(filter.DateFrom, out dateFrom) == false)
            {
                dateFrom = DateTime.MinValue;
            }
            if (DateTime.TryParse(filter.DateTo, out dateTo) == false)
            {
                dateTo = DateTime.MaxValue.AddDays(-10);
            }
            dateTo = dateTo.AddDays(1).AddSeconds(-1);

            var temp2 = GetFilteredBrokerUsersPayments(filter);

            var temp = temp2.GroupBy(x => x.AffiliateId).Select(x => new AffPaymentStatsDTO()
            {
                AffiliateId = x.Key.ToString(),
                AffiliateBrokers = x.Select(y => y.BrokerId).Distinct(),
                Deposites = x.Where(y => y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo && y.IsDepAdded==false).Select(y => y.UserId).Distinct().Count(),
                ReportedDeposites = x.Where(y => y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                Leads = x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                ReportedPayments =  x.Where(y=> y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.AffiliatePayment),
                ReportedPayouts = x.Where(y => y.IsDepAdded == false && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.BrokerPayment),
                Payments = x.Where(y=> y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.AffiliatePayment),
                Payouts = x.Where(y => y.IsDepAdded == false && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.BrokerPayment),
                Ratio = (double)x.Where(y => y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo && y.IsDepAdded == false).Select(y => y.UserId).Distinct().Count() 
                        / x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                ReportedRatio = (double)x.Where(y => (y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo)).Select(y => y.UserId).Distinct().Count()
                        / x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count()
            }).AsQueryable();


            var data = temp;

            if (!string.IsNullOrWhiteSpace(filter.OrderByField) && orderAffBys.ContainsKey(filter.OrderByField))
            {
                var orderExpression = orderAffBys[filter.OrderByField];
                var orderedData = filter.IsDesc
                    ? data.OrderByDescending(orderExpression)
                    : data.OrderBy(orderExpression);

                data = orderedData.AsQueryable();
            }

            totalItems = data.Count();

            data = filter.PageSize.HasValue && filter.PageNumber.HasValue
                       ? data.Skip((filter.PageSize * filter.PageNumber).Value).Take(filter.PageSize.Value)
                       : data;
            var totalItem = new AffPaymentStatsDTO()
            {
                Leads = data.Sum(x => x.Leads),
                Deposites = data.Sum(x => x.Deposites),
                ReportedDeposites = data.Sum(x => x.ReportedDeposites),
                Payouts = data.Sum(x => x.Payouts),
                Payments = data.Sum(x => x.Payments),
                ReportedPayouts = data.Sum(x => x.ReportedPayouts),
                ReportedPayments = data.Sum(x => x.ReportedPayments),

            };
            totalItem.Ratio = totalItem.Leads > 0 ? (double)totalItem.Deposites / totalItem.Leads : 0;
            totalItem.ReportedRatio = totalItem.Leads > 0 ? (double)totalItem.ReportedDeposites / totalItem.Leads : 0;
            var resultData = data.ToList();
            resultData.Add(totalItem);

            var result = new WebResult<IEnumerable<AffPaymentStatsDTO>>()
            {
                Data = resultData,
                Success = true,
                PageSize = filter.PageSize ?? 0,
                PageNumber = filter.PageNumber ?? 0,
                TotalItems = totalItems,
            };

            return result;
        }

        public WebResult<IEnumerable<BrokerPaymentStatsDTO>> GetBrkrStats(PaymentStatsFilterDTO filter, out int totalItems)
        {
            DateTime dateFrom;
            DateTime dateTo;
            if (DateTime.TryParse(filter.DateFrom, out dateFrom) == false)
            {
                dateFrom = DateTime.MinValue;
            }
            if (DateTime.TryParse(filter.DateTo, out dateTo) == false)
            {
                dateTo = DateTime.MaxValue.AddDays(-10);
            }
            dateTo = dateTo.AddDays(1).AddSeconds(-1);

            var temp2 = GetFilteredBrokerUsersPayments(filter);

            var temp = temp2.GroupBy(x => x.BrokerId).Select(x => new BrokerPaymentStatsDTO()
            {
                BrokerId = x.Key,
                BrokerAffiliates = x.Select(y => y.AffiliateId.ToString()).Distinct(),
                Deposites = x.Where(y => y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo && y.IsDepAdded == false).Select(y => y.UserId).Distinct().Count(),
                ReportedDeposites = x.Where(y => y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                Leads = x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                ReportedPayments = x.Where(y => y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.AffiliatePayment),
                ReportedPayouts = x.Where(y => y.IsDepAdded == false && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.BrokerPayment),
                Payments = x.Where (y=> y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.AffiliatePayment),
                Payouts = x.Where(y => y.IsDepAdded == false && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.BrokerPayment),
                Ratio = (double)x.Where(y => y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo && y.IsDepAdded == false).Select(y => y.UserId).Distinct().Count()
                        / x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                ReportedRatio = (double)x.Where(y => (y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo)).Select(y => y.UserId).Distinct().Count()
                                / x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count()
            }).AsQueryable();

            var data = temp;

            if (!string.IsNullOrWhiteSpace(filter.OrderByField) && orderBrkrBys.ContainsKey(filter.OrderByField))
            {
                var orderExpression = orderBrkrBys[filter.OrderByField];
                var orderedData = filter.IsDesc
                    ? data.OrderByDescending(orderExpression)
                    : data.OrderBy(orderExpression);
                data = orderedData.AsQueryable();
            }

            totalItems = data.Count();

            data = filter.PageSize.HasValue && filter.PageNumber.HasValue
                       ? data.Skip((filter.PageSize * filter.PageNumber).Value).Take(filter.PageSize.Value)
                       : data;

            var totalItem = new BrokerPaymentStatsDTO()
            {
                Leads = data.Sum(x => x.Leads),
                Deposites = data.Sum(x => x.Deposites),
                ReportedDeposites = data.Sum(x => x.ReportedDeposites),
                Payouts = data.Sum(x => x.Payouts),
                Payments = data.Sum(x => x.Payments),
                ReportedPayouts = data.Sum(x => x.ReportedPayouts),
                ReportedPayments = data.Sum(x => x.ReportedPayments),
            };
            totalItem.Ratio = totalItem.Leads > 0 ? (double)totalItem.Deposites / totalItem.Leads : 0;
            totalItem.ReportedRatio = totalItem.Leads > 0 ? (double)totalItem.ReportedDeposites / totalItem.Leads : 0;

            var resultData = data.ToList();
            resultData.Add(totalItem);

            var result = new WebResult<IEnumerable<BrokerPaymentStatsDTO>>()
            {
                Data = resultData,
                Success = true,
                PageSize = filter.PageSize ?? 0,
                PageNumber = filter.PageNumber ?? 0,
                TotalItems = totalItems,
            };

            return result;
        }

        public WebResult<IEnumerable<CountyPaymentStatsDTO>> GetCountryStats(PaymentStatsFilterDTO filter,
            out int totalItems)
        {
            DateTime dateFrom;
            DateTime dateTo;
            if (DateTime.TryParse(filter.DateFrom, out dateFrom) == false)
            {
                dateFrom = DateTime.MinValue;
            }
            if (DateTime.TryParse(filter.DateTo, out dateTo) == false)
            {
                dateTo = DateTime.MaxValue.AddDays(-10);
            }
            dateTo = dateTo.AddDays(1).AddSeconds(-1);

            var temp2 = GetFilteredBrokerUsersPayments(filter);

            var temp = temp2.GroupBy(x => x.Country).Select(x => new CountyPaymentStatsDTO()
            {
                Country = x.Key,
                Affiliates = x.Select(y => y.AffiliateId.ToString()).Distinct(),
                Brokers = x.Select(y => y.BrokerId.ToString()).Distinct(),
                Deposites = x.Where(y => y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo && y.IsDepAdded==false).Select(y => y.UserId).Distinct().Count(),
                ReportedDeposites = x.Where(y => y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                Leads = x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                ReportedPayments = x.Where(y => y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.AffiliatePayment),
                ReportedPayouts = x.Where(y => y.IsDepAdded == false && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.BrokerPayment),
                Payments = x.Where(y=> y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.AffiliatePayment),
                Payouts = x.Where(y => y.IsDepAdded==false && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo).Sum(y => y.BrokerPayment),
                Ratio = (double)x.Where(y => y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo && y.IsDepAdded == false).Select(y => y.UserId).Distinct().Count()
                        / x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count(),
                ReportedRatio = (double)x.Where(y => (y.IsReported && y.DepositDate != null && y.DepositDate >= dateFrom && y.DepositDate <= dateTo)).Select(y => y.UserId).Distinct().Count()
                                / x.Where(y => y.LeadDate >= dateFrom && y.LeadDate <= dateTo).Select(y => y.UserId).Distinct().Count()
            }).AsQueryable();

            var data = temp;

            if (!string.IsNullOrWhiteSpace(filter.OrderByField) && orderCountryBys.ContainsKey(filter.OrderByField))
            {
                var orderExpression = orderCountryBys[filter.OrderByField];
                var orderedData = filter.IsDesc
                    ? data.OrderByDescending(orderExpression)
                    : data.OrderBy(orderExpression);
                data = orderedData.AsQueryable();
            }

            totalItems = data.Count();

            data = filter.PageSize.HasValue && filter.PageNumber.HasValue
                ? data.Skip((filter.PageSize * filter.PageNumber).Value).Take(filter.PageSize.Value)
                : data;
            var totalItem = new CountyPaymentStatsDTO()
            {
                Leads = data.Sum(x => x.Leads),
                Deposites = data.Sum(x => x.Deposites),
                ReportedDeposites = data.Sum(x => x.ReportedDeposites),
                Payouts = data.Sum(x => x.Payouts),
                Payments = data.Sum(x => x.Payments),
                ReportedPayouts = data.Sum(x => x.ReportedPayouts),
                ReportedPayments = data.Sum(x => x.ReportedPayments),
            };
            totalItem.Ratio = totalItem.Leads > 0 ? (double)totalItem.Deposites / totalItem.Leads : 0;
            totalItem.ReportedRatio = totalItem.Leads > 0 ? (double)totalItem.ReportedDeposites / totalItem.Leads : 0;

            var resultData = data.ToList();
            resultData.Add(totalItem);

            var result = new WebResult<IEnumerable<CountyPaymentStatsDTO>>()
            {
                Data = resultData,
                Success = true,
                PageSize = filter.PageSize ?? 0,
                PageNumber = filter.PageNumber ?? 0,
                TotalItems = totalItems,
            };

            return result;
        }
        

        public IQueryable<BrokerUserWithPayments> GetFilteredBrokerUsersPayments(PaymentStatsFilterDTO filter)
        {
            var affiliateIds = !string.IsNullOrWhiteSpace(filter.AffiliateIds)
                ? filter.AffiliateIds.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(long.Parse)
                    .ToArray()
                : new long[] { };

            var brokerIds = !string.IsNullOrWhiteSpace(filter.BrokerIds)
                ? filter.BrokerIds.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(long.Parse)
                    .ToArray()
                : new long[] { };

            var countries = !string.IsNullOrWhiteSpace(filter.Countries)
                ? filter.Countries.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                : new string[] { };

            DateTime dateFrom;
            DateTime dateTo;
            if (DateTime.TryParse(filter.DateFrom, out dateFrom) == false)
            {
                dateFrom = SqlDateTime.MinValue.Value;
            }
            if (DateTime.TryParse(filter.DateTo, out dateTo) == false)
            {
                dateTo = DateTime.MaxValue.AddDays(-10);
            }
            dateTo = dateTo.AddDays(1).AddSeconds(-1);

            var temp2 = new List<BrokerUserWithPayments>();

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"EXEC GetBrokerUsersWithPayments '{dateFrom.ToString("u").Replace("Z", "")}', '{dateTo.ToString("u").Replace("Z", "")}'";
                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var leadDate = DateTime.Parse(reader["lead_date"].ToString());
                        var isDepositDate = DateTime.TryParse(reader["deposit_date"].ToString(), out var depositDate);


                        var affPayment = reader["affPayment"].ToString();
                        var brokerPayment = reader["brokerPayment"].ToString();
                        var country = reader["country"].ToString();
                        var brokerId = reader["broker_id"].ToString();
                        var affId = reader["aff_id"].ToString();
                        var userId = reader["user_id"].ToString();
                        var apiReport = reader["api_report"].ToString();
                        var isReported = reader["is_reported"].ToString();
                        var isDepAdded= reader["is_dep_added"].ToString();

                        temp2.Add(new BrokerUserWithPayments()
                        {
                            AffiliatePayment = string.IsNullOrEmpty(affPayment) ? 0 : int.Parse(affPayment),
                            BrokerPayment = string.IsNullOrEmpty(brokerPayment) ? 0 : int.Parse(brokerPayment),
                            Country = country,
                            BrokerId = string.IsNullOrEmpty(brokerId) ? 0 : long.Parse(brokerId),
                            AffiliateId = string.IsNullOrEmpty(affId) ? 0 : long.Parse(affId),
                            UserId = string.IsNullOrEmpty(userId) ? 0 : long.Parse(userId),
                            LeadDate = leadDate,
                            DepositDate = isDepositDate ? (DateTime?)depositDate : null,

                            ApiReport = !string.IsNullOrEmpty(apiReport) && bool.Parse(apiReport),
                            IsReported = !string.IsNullOrEmpty(isReported) && bool.Parse(isReported),
                            IsDepAdded = !string.IsNullOrEmpty(isDepAdded) && bool.Parse(isDepAdded),
                        });
                    }
                }
            }

            var temp3 = temp2.AsQueryable();

            temp3 = affiliateIds.Any()
                ? temp3.Where(bu => affiliateIds.Contains(bu.AffiliateId))
                : temp3;

            temp3 = brokerIds.Any()
                ? temp3.Where(bu => brokerIds.Contains(bu.BrokerId))
                : temp3;

            temp3 = countries.Any()
                ? temp3.Where(bu => countries.Contains(bu.Country))
                : temp3;

            return temp3;
        }

    }
}
