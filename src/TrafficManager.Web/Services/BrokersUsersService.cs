﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Services
{
    public class BrokersUsersService : IBrokersUsersService
    {
        private Dictionary<string, Expression<Func<UsersResponseDto, object>>> orderBys = new Dictionary<string, Expression<Func<UsersResponseDto, object>>>()
        {
            {"firstName", x => x.FirstName},
            {"lastName", x=>x.LastName },
            {"email", x=>x.Email },
            {"phone", x=>x.Phone },
            {"broker", x=>x.Broker },
            {"affiliate", x=>x.Affiliate},
            {"country", x=>x.Country },
            {"leadDate", x=>x.LeadDate },
            {"depositDate", x=>x.DepositDate },
            {"ftd", x=>x.FTD },
            {"totalDeposit", x=>x.TotalDeposit },
            {"callStatus", x=>x.CallStatus },
            {"offerId", x=>x.OfferId },
            {"hasOfferId", x=>x.HasOfferId},

        };

        private IRepository<BrokersUsers> _brokersUsersRepo;
        private IRepository<User> _usersRepo;
        private IRepository<Broker> _brokersRepo;
        private IRepository<Affilate> _affiliatesRepo;

        public BrokersUsersService(IRepository<BrokersUsers> brokersUsersRepo,
            IRepository<User> usersRepo,
            IRepository<Broker> brokersRepo,
            IRepository<Affilate> affiliatesRepo)
        {
            this._brokersUsersRepo = brokersUsersRepo;
            this._usersRepo = usersRepo;
            this._brokersRepo = brokersRepo;
            this._affiliatesRepo = affiliatesRepo;
        }

        public IQueryable<BrokersUsers> Get()
        {
            return _brokersUsersRepo.Data;
        }

        public IQueryable<BrokersUsers> Get(Expression<Func<BrokersUsers, bool>> func)
        {
            return _brokersUsersRepo.Data.Where(func);
        }

        public OperationResult<BrokersUsers> Add(BrokersUsers item)
        {
            throw new NotImplementedException();
        }

        public OperationResult<BrokersUsers> Update(BrokersUsers item)
        {
            throw new NotImplementedException();
        }

        public OperationResult<BrokersUsers> Delete(BrokersUsers item)
        {
            throw new NotImplementedException();
        }

        public OperationResult UpdateBrokerForUsers(IEnumerable<long> userIds, long brokerId)
        {
            var records = _usersRepo.Data.Where(x => userIds.Contains(x.Id));
            foreach (var user in records)
            {
                user.SendtoBroker = brokerId;
            }

            var updatedRecords = _usersRepo.Update(records);

            var result = new OperationResult()
            {
                Success = true,
                Messages = new[] { $"Update {updatedRecords.Count()} records." }
            };

            return result;
        }

        public IQueryable<UsersResponseDto> GetByFilters(UsersFilterDto filters, out int totalItems)
        {
            var isDateFrom = DateTime.TryParse(filters.FromDate, out DateTime dateFrom);
            var isDateTo = DateTime.TryParse(filters.ToDate, out DateTime dateTo);

            var isDepositDateFrom = DateTime.TryParse(filters.FromDepositDate, out DateTime depositDateFrom);
            var isDepositDateTo = DateTime.TryParse(filters.ToDepositDate, out DateTime depositDateTo);

            depositDateTo = depositDateTo.AddDays(1).AddSeconds(-1);
            dateTo = dateTo.AddDays(1).AddSeconds(-1);

            var affiliatesIds = string.IsNullOrWhiteSpace(filters.AffiliateIds)
                    ? new string[] { }
                    : filters.AffiliateIds
                                .Replace(" ", "")
                                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var countries = string.IsNullOrWhiteSpace(filters.Countries)
                    ? new string[] { }
                    : filters.Countries
                                .Replace(" ", "")
                                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);


            var brokerIds = string.IsNullOrWhiteSpace(filters.BrokerIds)
                ? new List<long> { }
                : filters.BrokerIds.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => long.Parse(x))
                    .ToList();

            var data = _brokersUsersRepo.Data;

            data = isDateFrom
                ? data.Where(bu => bu.LeadDate >= dateFrom)
                : data;

            data = isDateTo
                ? data.Where(bu => bu.LeadDate <= dateTo)
                : data;


            data = isDepositDateFrom
                ? data.Where(bu => bu.DepositDate >= depositDateFrom)
                : data;

            data = isDepositDateTo
                ? data.Where(bu => bu.DepositDate <= depositDateTo)
                : data;

            data = brokerIds.Any()
                ? data.Where(bu => brokerIds.Contains(bu.BrokerId))
                : data;

            data = affiliatesIds.Any()
                ? data.Where(bu => affiliatesIds.Contains(bu.AffId))
                : data;




            var responseData = from d in data
                               join u in _usersRepo.Data
                               on d.UserId equals u.Id into temp
                               from x in temp.DefaultIfEmpty()
                               select new UsersResponseDto()
                               {
                                   Id = d.Id,
                                   UserId = x.Id,
                                   Email = x.Email,
                                   Phone = x.Phone,
                                   FirstName = x.Name,
                                   LastName = x.SecondName,
                                   BrokerId = d.BrokerId,
                                   AffId = d.AffId,
                                   Country = x.Country,
                                   LeadDate = d.LeadDate,
                                   DepositDate = d.DepositDate,
                                   FTD = (int?)d.Amount,
                                   TotalDeposit = (int?)d.TotalAmount,
                                   CallStatus = d.CallStatus,
                                   OfferId=d.OfferId,
                                   HasOfferId=d.Guid,
                               } ;

            responseData = countries.Any()
               ? responseData.Where(bu => countries.Contains(bu.Country))
               : responseData;

            if (string.IsNullOrWhiteSpace(filters.TextFilter) == false)
            {
                var txtFilter = filters.TextFilter.Trim().ToLower().Replace(".", "").Replace("+", "");

                responseData = responseData.Where(u => u.Email.ToLower().Replace(".", "").Contains(txtFilter)
                || u.FirstName.ToLower().Contains(txtFilter) ||
                u.LastName.ToLower().Contains(txtFilter) || u.Phone.ToLower().Contains(txtFilter) || u.HasOfferId.ToLower().Contains(txtFilter));
            }

            totalItems = responseData.Count();

            //var responseData = data.Join(_usersRepo.Data, bu => bu.UserId, u => u.Id, (bu, u) =>
            //new UsersResponseDto()
            //{
            //    Id = bu.Id,
            //    UserId = u.Id,
            //    Email = u.Email,
            //    FirstName = u.Name,
            //    LastName = u.SecondName,
            //    BrokerId = bu.BrokerId,
            //    AffId = bu.AffId,
            //    Country = u.Country,
            //    LeadDate = bu.LeadDate,
            //    DepositDate = bu.DepositDate,
            //});

            responseData = from rd in responseData
                           join b in _brokersRepo.Data
                           on rd.BrokerId equals b.Id into temp
                           from x in temp.DefaultIfEmpty()
                           select new UsersResponseDto(rd)
                           {
                               Broker = x.Name
                           };

            //responseData = responseData.Join(_brokersRepo.Data, ur => ur.BrokerId, b => b.Id, (ur, b) =>
            //    new UsersResponseDto(ur)
            //    {
            //        Broker = b.Name
            //    });

            responseData = from rd in responseData
                           join a in _affiliatesRepo.Data
                           on rd.AffId equals a.Id into temp
                           from x in temp.DefaultIfEmpty()
                           select new UsersResponseDto(rd)
                           {
                               Affiliate = x.Name
                           };

            //responseData = responseData.Join(_affiliatesRepo.Data, ur => ur.AffId, a => a.Id, (ur, a) =>
            //    new UsersResponseDto(ur)
            //    {
            //        Affiliate = a.Name
            //    });

            if (!string.IsNullOrWhiteSpace(filters.OrderByField) && orderBys.ContainsKey(filters.OrderByField))
            {
                var orderExp = orderBys[filters.OrderByField];

                responseData = filters.IsDesc
                    ? responseData.OrderByDescending(orderExp)
                    : responseData.OrderBy(orderExp);
            }

            filters.PageNumber = filters.PageNumber ?? 0;

            responseData = filters.PageSize == null
                ? responseData
                : responseData.Skip((int)(filters.PageNumber)
                            * (int)filters.PageSize)
                    .Take((int)filters.PageSize);

            return responseData;
        }
    }
}
