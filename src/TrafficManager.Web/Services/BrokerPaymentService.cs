﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Services
{
    public class BrokerPaymentService : IPaymentsService<BrokerPayment>
    {
        private Dictionary<string, Expression<Func<BrokerPayment, object>>> orderBys = new Dictionary<string, Expression<Func<BrokerPayment, object>>>()
        {
            {"countries", x => x.Country},
            {"validFrom", x => x.ValidFrom},
            {"validTo", x => x.ValidTo},
            {"paymentType", x => x.PaymentType},
            {"amount", x => x.Amount}
        };

        private readonly IRepository<BrokerPayment> _brkrPaymentsRepo;

        public BrokerPaymentService(IRepository<BrokerPayment> brkrPaymentsRepo)
        {
            this._brkrPaymentsRepo = brkrPaymentsRepo;
        }

        public IQueryable<BrokerPayment> Get()
        {
            return this._brkrPaymentsRepo.Data;
        }

        public IQueryable<BrokerPayment> Get(Expression<Func<BrokerPayment, bool>> func)
        {
            return this._brkrPaymentsRepo.Data.Where(func);
        }

        public WebResult<IQueryable<PaymentDTO>> GetByFilters(PaymentFilterDTO filter, out int totalItems)
        {
            var data = this._brkrPaymentsRepo.Data;

            if (filter.SubjectId >= 0)
            {
                data = data.Where(p => p.BrokerId == filter.SubjectId);
            }

            if (!string.IsNullOrWhiteSpace(filter.Countries))
            {
                var countryFilter = filter.Countries.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                var filteredData = new List<BrokerPayment>();
              if (!countryFilter.Any(cf => cf == "**"))
              {
                foreach (string country in countryFilter)
                {
                  filteredData.AddRange(data.Where(p => p.Country == country));
                }

                data = filteredData.AsQueryable();
              }
            }

            if (filter.PaymentType != 0)
            {
                data = data.Where(p => p.PaymentType == filter.PaymentType);
            }

            filter.PageNumber = filter.PageNumber ?? 0;

            totalItems = data.Count();

            if (!string.IsNullOrWhiteSpace(filter.OrderByField) && orderBys.ContainsKey(filter.OrderByField))
            {
                var orderExp = orderBys[filter.OrderByField];

                if (filter.IsDesc)
                {
                    data = data.OrderByDescending(orderExp);
                }
                else
                {
                    data = data.OrderBy(orderExp);
                }
            }

            if (filter.PageSize != null)
            {
                data = data.Skip((int)filter.PageSize * (int)filter.PageNumber)
                           .Take((int)filter.PageSize);
            }

            return new WebResult<IQueryable<PaymentDTO>>
            {
                Data = data.Select(d => new PaymentDTO()
                {
                    Id = d.Id,
                    SubjectId = d.BrokerId,
                    Country = d.Country,
                    Amount = d.Amount,
                    PaymentType = d.PaymentType,
                    ValidFrom = d.ValidFrom,
                    ValidTo = d.ValidTo
                }),
                Success = true,
                PageNumber = filter.PageNumber ?? 0,
                PageSize = filter.PageSize ?? 0,
                TotalItems = totalItems,
            };
        }

        public OperationResult<BrokerPayment> Add(BrokerPayment item)
        {
            return new OperationResult<BrokerPayment>()
            {
                Data = this._brkrPaymentsRepo.Add(item),
                Success = true,
                Messages = new[] { "Record created" }
            };
        }

        public OperationResult<BrokerPayment> Update(BrokerPayment item)
        {
            return new OperationResult<BrokerPayment>()
            {
                Data = this._brkrPaymentsRepo.Update(item),
                Success = true,
                Messages = new[] { "Record updated" }
            };
        }

        public OperationResult<BrokerPayment> Delete(BrokerPayment item)
        {
            return new OperationResult<BrokerPayment>()
            {
                Data = this._brkrPaymentsRepo.Delete(item), 
                Success = true,
                Messages = new[] { "Record was successfully removed" }
            };
        }

        public OperationResult<BrokerPayment> Validate(BrokerPayment item)
        {
            var messages = new List<string>();

            if (item.Amount < 1)
            {
                messages.Add("Amount should be grater than zero");
            }

            if (item.ValidTo != null &&
               item.ValidFrom > item.ValidTo)
            {
                messages.Add("First date can`t be greater than end date");
            }

            var existingItem =
                this._brkrPaymentsRepo.Data.AsNoTracking().FirstOrDefault(x => x.BrokerId == item.BrokerId &&
                                                                x.Country.Equals(item.Country, StringComparison.CurrentCultureIgnoreCase) &&
                                                                x.PaymentType == item.PaymentType &&
                                                                !x.ValidTo.HasValue &&
                                                                !item.ValidTo.HasValue);
            if (existingItem != null && existingItem.Id != item.Id)
            {
                messages.Add($"There is an open record id = [{existingItem.Id}] " +
                             $"ValidFrom = [{existingItem.ValidFrom}] " +
                             $"ValidTo = [{existingItem.ValidTo}] " +
                             $"Amount = [{existingItem.Amount}] " +
                             $"PaymentType = [{existingItem.PaymentType}]");
            }
            
            if (item.Id == 0)
            {
                existingItem = _brkrPaymentsRepo.Data.AsNoTracking().FirstOrDefault(x => x.BrokerId == item.BrokerId &&
                                                                         x.Country.Equals(item.Country, StringComparison.CurrentCultureIgnoreCase) &&

                                                                         // different payments types also not allowed at the same time
                                                                         // x.PaymentType == item.PaymentType &&

                                                                         ((x.ValidTo.HasValue && 
                                                                         !((x.ValidFrom >= item.ValidFrom && x.ValidFrom > item.ValidTo) ||
                                                                         (x.ValidTo < item.ValidFrom && x.ValidTo <= item.ValidTo))) ||

                                                                         (!x.ValidTo.HasValue &&
                                                                         !(x.ValidFrom >= item.ValidFrom && x.ValidFrom > item.ValidTo))));
            }
            else
            {
                existingItem = _brkrPaymentsRepo.Data.AsNoTracking().FirstOrDefault(x => x.Id != item.Id &&
                                                                         x.BrokerId == item.BrokerId &&
                                                                         x.Country.Equals(item.Country, StringComparison.CurrentCultureIgnoreCase) &&
                                                                         
                                                                         // different payments types also not allowed at the same time
                                                                         // x.PaymentType == item.PaymentType &&

                                                                         ((x.ValidTo.HasValue &&
                                                                         !((x.ValidFrom >= item.ValidFrom && x.ValidFrom > item.ValidTo) ||
                                                                         (x.ValidTo < item.ValidFrom && x.ValidTo <= item.ValidTo))) ||

                                                                         (!x.ValidTo.HasValue &&
                                                                         !((x.ValidFrom >= item.ValidFrom && x.ValidFrom > item.ValidTo)))));
            }

            if (existingItem != null)
            {
                messages.Add($"There is a payment overlapping with " +
                             $"record id = [{existingItem.Id}] " +
                             $"country = [{existingItem.Country}] " +
                             $"validFrom = [{existingItem.ValidFrom}] " +
                             $"validTo = [{existingItem.ValidTo}] " +
                             $"type = [{existingItem.PaymentType}]");
            }

            existingItem = null;

            var result = new OperationResult<BrokerPayment>()
            {
                Data = item,
                Messages = messages,
                Success = messages.Count == 0
            };
            return result;
        }
    }
}
