﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Services
{
    public class DynamicsService : IDynamicsService
    {
        private IRepository<BrokersUsers> _brokersUsersRepo;

        public DynamicsService(IRepository<BrokersUsers> brokersUsersRepo)
        {
            this._brokersUsersRepo = brokersUsersRepo;
        }

        public DynamicsDto GetDynamics(DynamicsFilterDto filters)
        {
            
            var isDateFrom = DateTime.TryParse(filters.FromDate, out DateTime dateFrom);
            var isDateTo = DateTime.TryParse(filters.ToDate, out DateTime dateTo);

            dateFrom = isDateFrom ? dateFrom : System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            dateTo = isDateTo ? dateTo.AddDays(1).AddSeconds(-1) : DateTime.MaxValue;

            var deposits = _brokersUsersRepo
                    .Data
                    .Where(x=>x.DepositDate.HasValue)
                    .Where(x => x.DepositDate >= dateFrom && x.DepositDate <= dateTo)
                    .GroupBy(bu => bu.DepositDate.Value.Date)
                    .Select(group => new DynamicsItem()
                        {
                            Date = group.Key,
                            Amount = group.Count()
                        });

            var leads = _brokersUsersRepo
                .Data
                .Where(x=>x.LeadDate.HasValue)
                .Where(x => x.LeadDate >= dateFrom && x.LeadDate <= dateTo)
                .GroupBy(bu => bu.LeadDate.Value.Date)
                .Select(group => new DynamicsItem()
                {
                    Date = group.Key,
                    Amount = group.Count()
                });

            var result = new DynamicsDto()
            {
                Deposits =  deposits.ToList(),
                Leads =  leads.ToList()
            };

            return result;
        }
    }
}
