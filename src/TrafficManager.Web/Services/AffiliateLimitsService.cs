﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Services
{
    public class AffilitateLimitsService : IAffiliateLimitService
    {
        private Dictionary<string, Expression<Func<AffiliateCountryLimit, object>>> orderBys = new Dictionary<string, Expression<Func<AffiliateCountryLimit, object>>>()
        {
            {"affiliate", x => x.AffiliateId},
            {"countries", x=>x.Countries},
            {"type", x=>x.Type},
            {"min", x=>x.Min},
            {"max", x=>x.Max},
            {"factor", x=>x.Factor},
        };

        private IRepository<AffiliateCountryLimit> _affiliateLimitsRepo;
        private IRepository<Affilate> _affiliateRepo;
        

        public AffilitateLimitsService(
            IRepository<AffiliateCountryLimit> affiliateLimitsRepo,
            IRepository<Affilate> affiliateRepo
            )
        {
            _affiliateLimitsRepo = affiliateLimitsRepo;
            _affiliateRepo = affiliateRepo;
        
        }


        public IQueryable<AffiliateCountryLimit> Get()
        {
            return _affiliateLimitsRepo.Data.AsNoTracking();
        }

        public IQueryable<AffiliateCountryLimit> GetByFilters(AffiliateCountryLimitFilterDto filters, out int totalItems)
        {
            var data = _affiliateLimitsRepo.Data;
            var affiliates = string.IsNullOrWhiteSpace(filters.AffiliateName)
                ? _affiliateRepo.Data
                : _affiliateRepo.Data.AsNoTracking().Where(x => x.Name.ToLower().Contains(filters.AffiliateName.ToLower()));


            // by affiliates Ids
            data = string.IsNullOrWhiteSpace(filters.AffiliateIds) 
                ? data
                : data.Where(r => filters.AffiliateIds
                    .Replace(" ", "")
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Contains(r.AffiliateId.ToString()));

            // by broker name
            data = string.IsNullOrWhiteSpace(filters.AffiliateName)
                ? data
                : data.Join(affiliates, x => x.AffiliateId, y => y.Id, (x,y) => x);

            // by factor 
            data = filters.Factor == null
                ? data
                : filters.IsFactorGreater == null
                    ? data.Where(x => x.Factor == filters.Factor.Value)
                    : filters.IsFactorGreater.Value
                        ? data.Where(x => x.Factor >= filters.Factor)
                        : data.Where(x => x.Factor <= filters.Factor);

            // by min value
            data = filters.Min == null
                ? data
                : filters.IsMinGreater == null
                    ? data.Where(x => x.Min == filters.Min.Value)
                    : filters.IsMinGreater.Value
                        ? data.Where(x => x.Min >= filters.Min.Value)
                        : data.Where(x => x.Min <= filters.Min.Value);

            // by max value
            data = filters.Max == null
                ? data
                : filters.IsMaxGreater == null
                    ? data.Where(x => x.Max == filters.Max.Value)
                    : filters.IsMaxGreater.Value
                        ? data.Where(x => x.Max >= filters.Max.Value)
                        : data.Where(x => x.Max <= filters.Max.Value);

            // by country
            var countriesInFilter = filters.Countries?.Split(new []{","} , StringSplitOptions.RemoveEmptyEntries);

            data = string.IsNullOrWhiteSpace(filters.Countries)
                ? data
                : data.Where(x => countriesInFilter.Any(y => x.Countries.Contains(y)));
            
            // order by
            if (orderBys.ContainsKey(filters.OrderByField ?? ""))
            {
                data = filters.IsDesc
                    ? data.OrderByDescending(orderBys[filters.OrderByField])
                    : data.OrderBy(orderBys[filters.OrderByField]);
            }

            filters.PageNumber = filters.PageNumber ?? 0;
            totalItems = data.Count();

            // pagintion
            data= filters.PageSize == null
                ? data
                : data.Skip((int)(filters.PageNumber)
                                  * (int)filters.PageSize)
                    .Take((int)filters.PageSize);

            return data;
        }

        public OperationResult<AffiliateCountryLimit> Add(AffiliateCountryLimit item)
        {
            var result = new OperationResult<AffiliateCountryLimit>
            {
                Data = item,
                Messages = Validate(item).ToList()
            };

            if (result.Messages.Any())
            {
                return result;
            }

            _affiliateLimitsRepo.Add(item);
            result.Success = true;

            return result;
        }

        public OperationResult<AffiliateCountryLimit> Update(AffiliateCountryLimit item)
        {
            var result = new OperationResult<AffiliateCountryLimit>
            {
                Data = item,
                Messages = Validate(item).ToList()
            };

            if (result.Messages.Any())
            {
                return result;
            }

            _affiliateLimitsRepo.Update(item);
            result.Success = true;

            return result;
        }

        public OperationResult<AffiliateCountryLimit> Delete(int id)
        {
            var item = _affiliateLimitsRepo.Data.FirstOrDefault(x => x.Id.Equals(id));
            var result = new OperationResult<AffiliateCountryLimit>
            {
                Data = item
            };

            if (item == null)
            {
                result.Messages = new[] {$"No BrokerCountryLimit with id [{id}]"};
                return result;
            }

            _affiliateLimitsRepo.Delete(item);

            result.Success = true;
            return result;
        }

        public OperationResult AddDeposit(IEnumerable<int> ids)
        {
            var affiliateLimits = _affiliateLimitsRepo.Data
                .Where(x => ids.Contains(x.Id)).ToList();

            affiliateLimits.ForEach(x => x.IsAddDep = true);

            var count = _affiliateLimitsRepo.Update(affiliateLimits).Count();

            return new OperationResult() { Success = true, Data = count}; 
        }

        private IEnumerable<string> Validate(AffiliateCountryLimit item)
        {
            var validations = new List<string>();

            // affiliate is set,
            if (string.IsNullOrWhiteSpace(item.AffiliateId))
            {
                validations.Add("Affiliate Id is not set");
            }

            if ( string.IsNullOrWhiteSpace(item.Countries))
            {
                validations.Add("Countries are not set");
            }

            // should be no limits with the same country for the broker in other records
            var itemCountries = item.Countries
                .Split(new []{ "|" }, StringSplitOptions.RemoveEmptyEntries)
                .OrderBy(x => x);

            var countries = _affiliateLimitsRepo.Data
                .AsNoTracking()
                .Where(x => x.AffiliateId == item.AffiliateId && x.Id != item.Id)
                .Select(x=>x.Countries.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries))
                .ToList();

            var affiliateCountries = new List<string>();
            countries.ForEach(x => affiliateCountries.AddRange(x));

            var intersection = affiliateCountries.Intersect(itemCountries).ToList();
            var duplicatedCountry = intersection.Any();

            if (duplicatedCountry)
            {
                validations.Add($"The country [{string.Join(",", intersection)}] limit already exists");
            }

            // by limit type
            if (!AffiliateLimitType.AllowedTypes.Contains(item.Type))
            {
                validations.Add($"The type [{item.Type}] is not allowed Affiliate country limit type");
            }

            if (item.Type.Equals(AffiliateLimitType.Relative, StringComparison.CurrentCultureIgnoreCase))
            {
                if (!item.Factor.HasValue || item.Factor <= 0)
                {
                    validations.Add($"Factor [{item.Factor}] can't be null or less or equal than zero");
                }
            }

            if (item.Type.Equals(AffiliateLimitType.Absolute, StringComparison.CurrentCultureIgnoreCase))
            {
                if (!item.Max.HasValue || item.Max<= 0 || !item.Min.HasValue || item.Min <= 0)
                {
                    validations.Add("Minimum and Maximum values should be defined greater than zero");
                }

                if (item.Min> item.Max)
                {
                    validations.Add("Minimum should not be grater than Maximum");
                }
            }

            return validations;
        }

    }
}
