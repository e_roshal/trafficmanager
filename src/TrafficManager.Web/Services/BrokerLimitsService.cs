﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Services
{
    public class BrokerLimitsService :IBrokerLimitService
    {
        private Dictionary<string, Expression<Func<BrokerCountryLimit, object>>> orderBys = new Dictionary<string, Expression<Func<BrokerCountryLimit, object>>>()
        {
            {"broker", x => x.BrokerId},
            {"countries", x=>x.Countries},
            {"limit", x=>x.Limit},
        };

        private IRepository<BrokerCountryLimit> _brokerLimitsRepo;
        private IRepository<Broker> _brokerRepo;
        private IRepository<Country> _countryRepo;

        public BrokerLimitsService(
            IRepository<BrokerCountryLimit> brokerLimitsRepo,
            IRepository<Broker> brokerRepo,
            IRepository<Country> countryRepo
            )
        {
            _brokerLimitsRepo = brokerLimitsRepo;
            _brokerRepo = brokerRepo;
            _countryRepo = countryRepo;
        }


        public IQueryable<BrokerCountryLimit> Get()
        {
            return _brokerLimitsRepo.Data.AsNoTracking();
        }

        public IQueryable<BrokerCountryLimit> GetByFilters(BrokerCountryLimitFilterDto filters, out int totalItems)
        {
            var data = _brokerLimitsRepo.Data;
            var brokers = string.IsNullOrWhiteSpace(filters.BrokerName)
                ? _brokerRepo.Data
                : _brokerRepo.Data.Where(x => x.Name.ToLower().Contains(filters.BrokerName.ToLower()));


            // by broker Ids
            data = string.IsNullOrWhiteSpace(filters.BrokerIds) 
                ? data
                : data.Where(r => filters.BrokerIds
                    .Replace(" ", "")
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Contains(r.BrokerId.ToString()));

            // by broker name
            data = string.IsNullOrWhiteSpace(filters.BrokerName)
                ? data
                : data.Join(brokers, x=>x.BrokerId, y=>y.Id, (x,y) => x);

            // by limit 
            data = filters.Limit == null
                ? data
                : filters.IsGreater == null
                    ? data.Where(x => x.Limit == filters.Limit)
                    : filters.IsGreater.Value
                        ? data.Where(x => x.Limit >= filters.Limit)
                        : data.Where(x => x.Limit <= filters.Limit);

            // by country
            var countriesInFilter = filters.Countries?.Split(new []{","} , StringSplitOptions.RemoveEmptyEntries);

            data = string.IsNullOrWhiteSpace(filters.Countries)
                ? data
                : data.Where(x => countriesInFilter.Any(y => x.Countries.Contains(y)));
            
            // order by
            if (orderBys.ContainsKey(filters.OrderByField ?? ""))
            {
                data = filters.IsDesc
                    ? data.OrderByDescending(orderBys[filters.OrderByField])
                    : data.OrderBy(orderBys[filters.OrderByField]);
            }

            filters.PageNumber = filters.PageNumber ?? 0;
            totalItems = data.Count();

            // pagintion
            data= filters.PageSize == null
                ? data
                : data.Skip((int)(filters.PageNumber)
                                  * (int)filters.PageSize)
                    .Take((int)filters.PageSize);

            return data;
        }

        public OperationResult<BrokerCountryLimit> Add(BrokerCountryLimit item)
        {
            var result = new OperationResult<BrokerCountryLimit>
            {
                Data = item,
                Messages = Validate(item).ToList()
            };

            if (result.Messages.Any())
            {
                return result;
            }

            _brokerLimitsRepo.Add(item);
            result.Success = true;

            return result;
        }

        public OperationResult<BrokerCountryLimit> Update(BrokerCountryLimit item)
        {
            var result = new OperationResult<BrokerCountryLimit>
            {
                Data = item,
                Messages = Validate(item).ToList()
            };

            if (result.Messages.Any())
            {
                return result;
            }

            _brokerLimitsRepo.Update(item);
            result.Success = true;

            return result;
        }

        public OperationResult<BrokerCountryLimit> Delete(long id)
        {
            var item = _brokerLimitsRepo.Data.FirstOrDefault(x => x.Id.Equals(id));
            var result = new OperationResult<BrokerCountryLimit>
            {
                Data = item
            };

            if (item == null)
            {
                result.Messages = new[] {$"No BrokerCountryLimit with id [{id}]"};
                return result;
            }

            _brokerLimitsRepo.Delete(item);

            result.Success = true;
            return result;
        }

        private IEnumerable<string> Validate(BrokerCountryLimit item)
        {
            var validations = new List<string>();

            // broker is set,
            if (item.BrokerId < 1)
            {
                validations.Add("Broker Id is not set");
            }

            // limit is more than 0
            if (item.Limit < 1)
            {
                validations.Add("Limit should be more than 0");
            }

            if ( string.IsNullOrWhiteSpace(item.Countries))
            {
                validations.Add("Countries are not set");
            }

            // should be no limits with the same country for the broker in other records
            var itemCountries = item.Countries
                .Split(new []{ "|" }, StringSplitOptions.RemoveEmptyEntries)
                .OrderBy(x => x);

            var countries = _brokerLimitsRepo.Data.AsNoTracking().Where(x => x.BrokerId == item.BrokerId && x.Id != item.Id).Select(x=>x.Countries.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries)).ToList();

            var brokerCountries = new List<string>();
            countries.ForEach(x => brokerCountries.AddRange(x));

            var intersection = brokerCountries.Intersect(itemCountries).ToList();
            var duplicatedCountry = intersection.Any();

            if (duplicatedCountry)
            {
                validations.Add($"the country [{string.Join(",", intersection)}] limit already exists");
            }

            return validations;
        }

    }
}
