﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using IdentityServer4.Extensions;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Services
{
    public class BrokerService : IBrokerService
    {
        private Dictionary<string, Expression<Func<Broker, object>>> orderBys = new Dictionary<string, Expression<Func<Broker, object>>>()
        {
            {"name", x => x.Name},
            {"active", x=>x.Active},
            {"countries", x=>x.Countries},
            {"priority", x=>x.Priority},
            {"limit", x=>x.Limit},
            {"blockedCountries", x=>x.BlockedCountries}
        };

        private IRepository<Broker> _brokerRepo;
        private IRepository<Country> _countryRepo;

        public BrokerService(IRepository<Broker> brokerRepo,
                             IRepository<Country> countryRepo)
        {
            this._brokerRepo = brokerRepo;
            this._countryRepo = countryRepo;
        }

        public IQueryable<Broker> Get()
        {
            return _brokerRepo.Data;
        }

        public IQueryable<Broker> Get(Expression<Func<Broker, bool>> func)
        {
            return _brokerRepo.Data.Where(func);
        }

        public OperationResult<Broker> Add(Broker broker)
        {
            // Checking if obtained blockedCountries are valid and assignment valid value
            var blockedCountries = CheckAndConvertCountries(broker.BlockedCountries);

            broker.BlockedCountries = blockedCountries;

            var result = new OperationResult<Broker>()
            {
                Success = true,
                Data = _brokerRepo.Add(broker)
            };

            return result;
        }

        public OperationResult<Broker> Update(Broker broker)
        {
            var messages = new List<string>();
            // Checking if obtained blockedCountries are valid and assignment valid value


            broker.Countries = broker.Countries?? "" ;
            broker.BlockedCountries= broker.BlockedCountries ?? "";

            var countries = CheckAndConvertCountries(broker.Countries);
            if (!broker.Countries.Equals(countries))
            {
                messages.Add($"countries passed in wrong format: [{broker.Countries}]");
            }

            var blockedCountries = CheckAndConvertCountries(broker.BlockedCountries);
            if (!broker.BlockedCountries.Equals(blockedCountries))
            {
                messages.Add($"blocked countries passed in wrong format: [{broker.BlockedCountries}]");
            }


            broker.BlockedCountries = blockedCountries;

            var result = new OperationResult<Broker>()
            {
                Data = _brokerRepo.Update(broker),
                Messages = messages,
            };

            return result;
        }

        public OperationResult<Broker> Delete(Broker broker)
        {
            var result = new OperationResult<Broker>()
            {
                Data = _brokerRepo.Delete(broker),
                Success = true
            };

            return result;
        }

        public IQueryable<BrokerDTO> GetByFilters(BrokersFilterDto filters, out int totalItems)
        {
            IQueryable<Broker> data = _brokerRepo.Data;

            data = string.IsNullOrWhiteSpace(filters.BrokerName)
                ? data
                : data.Where(b => b.Name.Contains(filters.BrokerName));

            data = filters.IsActive == null
                ? data
                : data.Where(b => b.Active == filters.IsActive);


            // TODO: Refactor THIS ↓
            if (!string.IsNullOrWhiteSpace(filters.Countries))
            {
                var filterCountries = filters
                    .Countries
                    .Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                var localData = data.ToList() as IEnumerable<Broker>;
                localData = localData
                    .Where(x => !string.IsNullOrWhiteSpace(x.Countries))
                    .Where(x => filterCountries.Any(c => x.Countries.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Any(s => s.Equals(c, StringComparison.CurrentCultureIgnoreCase))));

                data = localData.AsQueryable();
            }

            if (filters.Limit != null)
            {
                if (filters.IsGreater == null)
                {
                    data = data.Where(b => b.Limit == filters.Limit);
                }
                if (filters.IsGreater == true)
                {
                    data = data.Where(b => b.Limit > filters.Limit);
                }
                if (filters.IsGreater == false)
                {
                    data = data.Where(b => b.Limit < filters.Limit);
                }
            }

            filters.PageNumber = filters.PageNumber ?? 0;

            totalItems = data.Count();

            if (!string.IsNullOrWhiteSpace(filters.OrderByField) && orderBys.ContainsKey(filters.OrderByField))
            {
                var orderExp = orderBys[filters.OrderByField];

                data = filters.IsDesc
                    ? data.OrderByDescending(orderExp)
                    : data.OrderBy(orderExp);
            }

            data = filters.PageSize == null
                ? data
                : data.Skip((int)(filters.PageNumber)
                            * (int)filters.PageSize)
                    .Take((int)filters.PageSize);

            return data.Select(b => new BrokerDTO()
            {
                Id = b.Id,
                Active = b.Active,
                Limit = b.Limit,
                Priority = b.Priority,
                Name = b.Name,
                Countries = b.Countries,
                BlockedCountries = b.BlockedCountries,
            });
        }

        public string CheckAndConvertCountries(string countries)
        {
            string result = null;

            var temp = countries.Split(new char[] { '|', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in temp)
            {
                var country = _countryRepo.Data.FirstOrDefault(x => x.CountryName.ToUpper().Equals(item.ToUpper()) ||
                                                                    x.CountryIso.ToUpper().Equals(item.ToUpper()));

                if (country != null)
                {
                    result += "|" + country.CountryIso;
                }
            }
            if (result != null)
            {
                result = result + "|";
            }

            return result;
        }
    }
}
