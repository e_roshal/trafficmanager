﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TrafficManager.Web.Infrastructure;
using TrafficManager.Web.Interfaces;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Services;

namespace TrafficManager.Web.Configuration
{
    public class AppConfigurator
    {
        public static void ConfigureDataServices(IServiceCollection services)
        {
            services.AddScoped<IRepository<User>, DbRepository<User>>();
            services.AddScoped<IRepository<Broker>, DbRepository<Broker>>();
            services.AddScoped<IRepository<Routing>, DbRepository<Routing>>();
            services.AddScoped<IRepository<Country>, DbRepository<Country>>();
            services.AddScoped<IRepository<Affilate>, DbRepository<Affilate>>();
            services.AddScoped<IRepository<Offer>, DbRepository<Offer>>();
            services.AddScoped<IRepository<BrokersUsers>, DbRepository<BrokersUsers>>();
            services.AddScoped<IRepository<AffiliatePayment>, DbRepository<AffiliatePayment>>();
            services.AddScoped<IRepository<BrokerPayment>, DbRepository<BrokerPayment>>();
            services.AddScoped<IRepository<BrokerCountryLimit>, DbRepository<BrokerCountryLimit>>();
            services.AddScoped<IRepository<AffiliateCountryLimit>, DbRepository<AffiliateCountryLimit>>();
            

            services.AddScoped<IBrokerService, BrokerService>();
            services.AddScoped<IRoutingService, RoutingService>();
            services.AddScoped<IBrokersUsersService, BrokersUsersService>();
            services.AddScoped<IDynamicsService, DynamicsService>();
            services.AddScoped<IPaymentsService<AffiliatePayment>, AffiliatePaymentService>();
            services.AddScoped<IPaymentsService<BrokerPayment>, BrokerPaymentService>();
            services.AddScoped<IPaymentStatisticsService, PaymentStatisticsService>();
            services.AddScoped<IBrokerLimitService, BrokerLimitsService>();
            services.AddScoped<IAffiliateLimitService, AffilitateLimitsService>();
        }

        public static void InitRolesAndUsers(IServiceProvider services)
        {
            var roles = new List<IdentityRole>()
            {
                new IdentityRole("Admin"),
                new IdentityRole("User")
            };

            var users = new Dictionary<TMUser, string>()
            {
                {new TMUser(){ UserName = "admin"}, "Password@123"},
            };

            var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = services.GetRequiredService<UserManager<TMUser>>();
            var logger = services.GetRequiredService<ILogger<Program>>();
            var config = services.GetRequiredService<IConfigurationRoot>();

            foreach (var role in roles)
            {
                if (!roleManager.RoleExistsAsync(role.Name).Result)
                {
                    roleManager.CreateAsync(role).Wait(Constants.AsyncTaskWaitTime);
                }
            }

            foreach (var user in users)
            {
                if (!userManager.Users.Any(u => u.UserName.Equals(user.Key.UserName)))
                {
                    var task = userManager.CreateAsync(user.Key, user.Value);
                    task.Wait(Constants.AsyncTaskWaitTime);
                    var result = task.Result;
                    if (!result.Succeeded)
                    {
                        logger.LogError(string.Join("\n", result.Errors.Select(x => x.Description)));
                    }
                }
            }

            bool.TryParse(config["ResetAdminPassword"], out bool resetPassword);

            if (resetPassword)
            {
                var admin = userManager.FindByNameAsync("admin").Result;
                var token = userManager.GeneratePasswordResetTokenAsync(admin).Result;
                var result = userManager.ResetPasswordAsync(admin, token, config["NewPassword"] ?? "Pasword@123").Result;
            }
        }

        //public static void AffPaymentsDataForStatistics(IServiceProvider services)
        //{
        //    var affPaymentsRepo = services.GetRequiredService<IPaymentsService<AffiliatePayment>>();
        //    var affIds = new List<int>() { 2009, 2015, 2043, 2901 };
        //    var countries = new List<string>() { "DE", "US", "RU", "CN" };
        //    var paymentTypes = new List<short>() { 1, 2 };

        //    var validDates = new DateTime[3][];

        //    validDates[0] = new DateTime[] { new DateTime(2018, 06, 01), new DateTime(2018, 07, 01) };
        //    validDates[1] = new DateTime[] { new DateTime(2018, 07, 01), new DateTime(2018, 08, 01) };
        //    validDates[2] = new DateTime[] { new DateTime(2018, 08, 01), new DateTime(2018, 09, 01) };

        //    foreach (var id in affIds)
        //    {
        //        foreach (var country in countries)
        //        {
        //            foreach (var pay in paymentTypes)
        //            {
        //                for (int i = 0; i < 3; i++)
        //                {
        //                    var payment = new AffiliatePayment
        //                    {
        //                        AffiliateId = id,
        //                        Country = country,
        //                        PaymentType = pay,
        //                        ValidFrom = validDates[i][0],
        //                        ValidTo = validDates[i][1]
        //                    };

        //                    switch (i)
        //                    {
        //                        case 0:
        //                            payment.Amount = 150;
        //                            break;

        //                        case 1:
        //                            payment.Amount = 100;
        //                            break;

        //                        case 2:
        //                            payment.Amount = 50;
        //                            break;

        //                        default:
        //                            break;
        //                    }

        //                    affPaymentsRepo.Add(payment);
        //                }
        //            }
        //        }
        //    }
        //}

        //public static void BrkrPaymentsDataForStatistics(IServiceProvider services)
        //{
        //    var brkrPaymentsRepo = services.GetRequiredService<IPaymentsService<BrokerPayment>>();
        //    var affIds = new List<int>() { 111, 101, 115, 159 };
        //    var countries = new List<string>() { "DE", "US", "RU", "CN" };
        //    var paymentTypes = new List<short>() { 1, 2 };
        //    var validDates = new DateTime[3][];

        //    validDates[0] = new DateTime[] { new DateTime(2018, 06, 01), new DateTime(2018, 07, 01) };
        //    validDates[1] = new DateTime[] { new DateTime(2018, 07, 01), new DateTime(2018, 08, 01) };
        //    validDates[2] = new DateTime[] { new DateTime(2018, 08, 01), new DateTime(2018, 09, 01) };

        //    foreach (var id in affIds)
        //    {
        //        foreach (var country in countries)
        //        {
        //            foreach (var pay in paymentTypes)
        //            {
        //                for (int i = 0; i < 3; i++)
        //                {
        //                    var payment = new BrokerPayment
        //                    {
        //                        BrokerId = id,
        //                        Country = country,
        //                        PaymentType = pay,
        //                        ValidFrom = validDates[i][0],
        //                        ValidTo = validDates[i][1]
        //                    };

        //                    switch (i)
        //                    {
        //                        case 0:
        //                            payment.Amount = 200;
        //                            break;

        //                        case 1:
        //                            payment.Amount = 150;
        //                            break;

        //                        case 2:
        //                            payment.Amount = 100;
        //                            break;

        //                        default:
        //                            break;
        //                    }

        //                    brkrPaymentsRepo.Add(payment);
        //                }
        //            }
        //        }
        //    }
        //}
    }
}
