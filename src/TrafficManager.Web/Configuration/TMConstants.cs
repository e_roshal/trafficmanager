﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrafficManager.Web.Configuration
{
    public class TMConstants
    {
        public const int AsyncTaskWaitTime = 10_000;
    }
}
