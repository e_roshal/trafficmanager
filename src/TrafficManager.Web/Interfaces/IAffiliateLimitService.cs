﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Interfaces
{
    public interface IAffiliateLimitService
    {
        IQueryable<AffiliateCountryLimit> Get();
        IQueryable<AffiliateCountryLimit> GetByFilters(AffiliateCountryLimitFilterDto filters, out int totalItems);

        OperationResult<AffiliateCountryLimit> Add(AffiliateCountryLimit item);
        OperationResult<AffiliateCountryLimit> Update(AffiliateCountryLimit item);
        OperationResult<AffiliateCountryLimit> Delete(int id);
        OperationResult AddDeposit(IEnumerable<int> ids);
    }
}
