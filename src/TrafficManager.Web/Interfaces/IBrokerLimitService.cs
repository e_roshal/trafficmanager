﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Interfaces
{
    public interface IBrokerLimitService
    {
        IQueryable<BrokerCountryLimit> Get();
        IQueryable<BrokerCountryLimit> GetByFilters(BrokerCountryLimitFilterDto filters, out int totalItems);

        OperationResult<BrokerCountryLimit> Add(BrokerCountryLimit item);
        OperationResult<BrokerCountryLimit> Update(BrokerCountryLimit item);
        OperationResult<BrokerCountryLimit> Delete(long id);
    }
}
