﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Interfaces
{
    public interface IPaymentsService<T> : IDataService<T>
    {
        OperationResult<T> Validate(T item);

        WebResult<IQueryable<PaymentDTO>> GetByFilters(PaymentFilterDTO filter, out int totalItems);
    }
}
