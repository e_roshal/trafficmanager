﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Interfaces
{
    public interface IBrokerService : IDataService<Broker>
    {
        IQueryable<BrokerDTO> GetByFilters(BrokersFilterDto filters, out int totalItems);
    }
}
