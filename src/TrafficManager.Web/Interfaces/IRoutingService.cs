﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Interfaces
{
    public interface IRoutingService : IDataService<Routing>
    {
        IQueryable<RoutingResponseDto> GetByFilters(RoutingFilterDto filters, out int totalItems);
        List<string> Validate(Routing routing);
    }
}
