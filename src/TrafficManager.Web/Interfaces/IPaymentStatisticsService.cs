﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Interfaces
{
    public interface IPaymentStatisticsService
    {
        WebResult<IEnumerable<AffPaymentStatsDTO>> GetAffStats(PaymentStatsFilterDTO filter, out int totalItems);
        WebResult<IEnumerable<BrokerPaymentStatsDTO>> GetBrkrStats(PaymentStatsFilterDTO filter, out int totalItems);
        WebResult<IEnumerable<CountyPaymentStatsDTO>> GetCountryStats(PaymentStatsFilterDTO filter, out int totalItems);
    }
}
