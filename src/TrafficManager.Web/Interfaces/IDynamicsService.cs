﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Interfaces
{
    public interface IDynamicsService
    {
        DynamicsDto GetDynamics(DynamicsFilterDto filters);
    }
}
