﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrafficManager.Web.Models;
using TrafficManager.Web.Models.Data;
using TrafficManager.Web.Models.DTO;

namespace TrafficManager.Web.Interfaces
{
    public interface IBrokersUsersService : IDataService<BrokersUsers>
    {
        OperationResult UpdateBrokerForUsers(IEnumerable<long> userIds, long brokerId);
        IQueryable<UsersResponseDto> GetByFilters(UsersFilterDto filters, out int totalItems);
    }
}
