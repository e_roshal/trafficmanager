﻿using System.Collections.Generic;

public class AffiliateLimitType
{
    public const string Absolute = "Absolute";
    public const string Relative = "Relative";

    public static string[] AllowedTypes = new[] { Absolute, Relative };
}