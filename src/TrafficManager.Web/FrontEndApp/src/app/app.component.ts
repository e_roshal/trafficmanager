import { Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { UserService } from './services/user.service';
import { debug } from 'util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationService } from './services/notification.service';

import { Injector } from '@angular/core';
import { SecurityService } from './services/security.service';
import { ListService } from './services/list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck, OnInit {
  spinnerIsVisible: boolean = false;
  menuIsVisible: boolean = true;
  title = 'OVO TM';

  notificationPanelClasses = {
    notifications: "bg-primary",
    errors: "bg-danger",
    successes: "bg-success"
  };

  constructor(
    public userSvc: UserService,
    private notificationsSvc: NotificationService,
    public snackBar: MatSnackBar,
    public securitySvc: SecurityService,
    public listSvc: ListService
  ) {
  }

  ngOnInit(): void {
    if (this.securitySvc.isAuthenticated()) {
      this.listSvc.getAllLists();
    }
  }

  ngDoCheck(): void {
    this.menuIsVisible = !this.userSvc.hideMenu;
    this.spinnerIsVisible = this.userSvc.spinnerIsVisible;
    for (let messageType in this.notificationsSvc.messages) {
      if (this.notificationsSvc.messages[messageType].length > 0) {
        for (let item in this.notificationsSvc.messages[messageType]) {
          this.showNotification(this.notificationsSvc.messages[messageType].pop(), messageType);
        }
      }
    }
  }

  showNotification(notification: any, messgeType: string) {
    let panelClass = this.notificationPanelClasses[messgeType];

    setTimeout(() => {
      this.snackBar.open(notification.message, notification.buttonText, {
        panelClass: [panelClass, "text-white"],
        verticalPosition: "top"
      });
    }, 0);
  }
}
