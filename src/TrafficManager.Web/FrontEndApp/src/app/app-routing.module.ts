import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SecurityService } from './services/security.service';
import { LoginCmpnt } from './components/login.cmpnt';
import { DashboardCmpnt } from './components/dashboard.cmpnt';
import { NotFoundComponent } from './components/notFoundComponent';
import { BrokersCmpnt } from './components/brokers.cmpnt';
import { RoutingCmpnt } from './components/routing.cmpnt';
import { ProfileCmpnt } from './components/profile.cmpnt';
import { UsersCmpnt } from './components/users.cmpnt';
import { AffiliatePaymentCmpnt } from './components/affiliatePayment.cmpnt';
import { BrokerPaymentCmpnt } from './components/brokerPayment.cmpnt';
import { AffiliateStatisticsCmpnt } from './components/affiliateStatistics.cmpnt';
import { BrokerStatisticsCmpnt } from './components/brokerStatistics.cmpnt';
import { CountryStatisticsCmpnt } from './components/countryStatistics.cmpnt';
import { BrokersLimitsCmpnt } from './components/brokersLimits.cmpnt';
import { AffiliatesLimitsCmpnt } from './components/affiliateLimits.cmpnt';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardCmpnt, canActivate: [SecurityService] },
  { path: 'login', component: LoginCmpnt },
  { path: 'brokers', component: BrokersCmpnt, canActivate: [SecurityService] },
  { path: 'brokers-limits', component: BrokersLimitsCmpnt, canActivate: [SecurityService] },
  { path: 'affiliates-limits', component: AffiliatesLimitsCmpnt, canActivate: [SecurityService] },
  { path: 'routing', component: RoutingCmpnt, canActivate: [SecurityService] },
  { path: 'profile', component: ProfileCmpnt, canActivate: [SecurityService] },
  { path: 'users', component: UsersCmpnt, canActivate: [SecurityService] },
  { path: 'payments/affiliate', component: AffiliatePaymentCmpnt, canActivate: [SecurityService] },
  { path: 'payments/broker', component: BrokerPaymentCmpnt, canActivate: [SecurityService] },
  { path: 'statistics/affiliate', component: AffiliateStatisticsCmpnt, canActivate: [SecurityService] },
  { path: 'statistics/broker', component: BrokerStatisticsCmpnt, canActivate: [SecurityService] },
  { path: 'statistics/country', component: CountryStatisticsCmpnt, canActivate: [SecurityService] },
  { path: '**', component: NotFoundComponent, canActivate: [SecurityService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
