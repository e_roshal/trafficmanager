import { StandardFilters } from "./standardFilters";

export class BrokerLimitsFiltersDTO extends StandardFilters {
    brokerName?: string;
    countries?: string;
    limit?: number;
    isGreater?: boolean = null;
    brokerIds?: string[];
}
