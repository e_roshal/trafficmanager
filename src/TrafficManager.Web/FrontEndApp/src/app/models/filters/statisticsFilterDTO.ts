import { StandardFilters } from "./standardFilters";

export class StatisticsFilterDTO extends StandardFilters {
    brokerIds: string;
    affiliateIds: string;
    dateFrom: any;
    dateTo: any;
    countries: string;
}
