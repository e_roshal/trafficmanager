import { StandardFilters } from "./standardFilters";

export class BrokerFiltersDTO extends StandardFilters {
    brokerName: string;
    isActive: boolean = null;
    countries: string;
    dailyLimit: number;
    isGreater: boolean = null ;
}
