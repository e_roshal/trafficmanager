import { StandardFilters } from "./standardFilters";

export class PaymentFilterDTO extends StandardFilters {
    subjectId: number;
    countries: string;
    paymentType: number;
}
