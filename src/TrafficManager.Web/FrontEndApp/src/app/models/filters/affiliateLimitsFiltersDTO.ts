import { StandardFilters } from "./standardFilters";

export class AffiliateLimitsFiltersDTO extends StandardFilters {
    affiliateName?: string;
    affiliateIds?: string[];
    countries?: string;
    factor?: number;
    isFactorGreater?: boolean = null;
    min?: number;
    isMinGreater?: boolean = null;
    max?: number;
    isMaxGreater?: boolean = null;
}
