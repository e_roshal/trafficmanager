import { StandardFilters } from "./standardFilters";

export class RoutingFiltersDTO extends StandardFilters {
    affiliateIds: string;
    brokerIds: string;
    offerIds: any;
}
