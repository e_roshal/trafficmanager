import { StandardFilters } from "./standardFilters";

export class UsersFilterDTO extends StandardFilters {
    brokerIds: string;
    affiliateIds: string;
    fromDate: any;
    toDate: any;
    fromDepositDate: string;
    toDepositDate: string;
    countries: string;
    textFilter: string;
}
