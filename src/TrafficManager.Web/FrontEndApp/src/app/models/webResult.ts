export class WebResult<T> {
    data: T;
    totalItems: number;
    pageNumber: number;
    pageSize: number; 
    success: boolean;
    messages: string[];
}
