export class Broker {
    id: number;
    name: string;
    active: boolean;
    priority: number;
    limit: number;
    countries: string;
    blockedCountries: string;
    
}
