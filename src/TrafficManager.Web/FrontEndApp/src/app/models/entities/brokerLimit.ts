export interface BrokerCountryLimit {
    id: number;
    brokerId: number;
    countries: string;
    limit: number;
    
    broker?: string;
}
