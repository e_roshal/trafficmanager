export class Routing {
    id: number;
    affId: string;
    affiliate: string;
    brokerId: number;
    broker: string;
    priority: number;
    offerId: string;
    offer: string;
    countries: string;
    from: any;
    to: any;
    active: boolean;
    limit: number;
}
