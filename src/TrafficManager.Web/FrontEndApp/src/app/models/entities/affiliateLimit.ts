import { AffiliateLimitType } from "../../enums/affiliateLimitType";

export interface AffiliateCountryLimit {
    id: number;
    affiliateId: number;
    countries: string;
    type: AffiliateLimitType;
    factor?: number;
    minimum?: number;
    maximum?: number;
    
}
