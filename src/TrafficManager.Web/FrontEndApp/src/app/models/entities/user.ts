export class User {
  id: number;
  userId: number;
  email: string;
  phone: string;
  firstName: string;
  lastName: string;
  brokerId: number;
  broker: string;
  affId: number;
  affiliate: string;
  leadDate: string;
  depositDate: string;
  country: string;
  ftd: number;
  // totalDeposit: number;
  isReported: boolean;
  callStatus: string;
  offerId: string;
  hasOfferId: string;
}

export class ExtendedUser extends User {
  index: number;
  // brokerName: string;
  // affName: string;
  countryName: string;
}
