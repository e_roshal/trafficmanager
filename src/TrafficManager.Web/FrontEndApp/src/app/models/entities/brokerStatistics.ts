export class BrokerStatistics {
  id: number;
  brokerId: number;
  broker: string;
  leads: number;
  ratio: string;
  deposites: number;
  reportedRatio: string;
  reportedDeposites: number;
  brokerAffiliates: any;
  affiliate: string;
  payments: number;
  payouts: any;
  profit: number;
  reportedPayments: number;
  reportedPayouts: number;
}


export class ExtendedBrokerStatistics extends BrokerStatistics {
  index: number;
  // brokerName: string;
  // affName: string;
  countryName: string;
}
