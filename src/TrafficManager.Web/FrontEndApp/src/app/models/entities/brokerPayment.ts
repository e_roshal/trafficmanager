export class BrokerPayment {
    id: number;
    country: string;
    brokerId: number;
    validFrom: string;
    validTo: string;
    paymentType: string;
    amount: number;
}
