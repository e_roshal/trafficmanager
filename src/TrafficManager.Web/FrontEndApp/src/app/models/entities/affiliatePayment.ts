export class AffiliatePayment {
    id: number;
    AffiliateId: number;
    country: string;
    validFrom: string;
   validTo: string;
    paymentType: string;
    amount: number;
}
