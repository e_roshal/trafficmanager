export class CountryStatistics {
    id: number;
    country: string;
    affiliates: any;
    leads: number;
    ratio: string;
    deposites: number;
    reportedRatio: string;
    reportedDeposites: number;
    brokers: any;
    payments: number;
    payouts: any;
    profit: number;
    reportedPayments: number;
    reportedPayouts: number;
  }
  
  export class ExtendedCountryStatistics extends CountryStatistics {
    index: number;
    // brokerName: string;
    // affName: string;
    countryName: string;
  }
