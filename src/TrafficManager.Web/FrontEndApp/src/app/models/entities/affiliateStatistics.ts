export class AffiliateStatistics {
  id: number;
  affiliateId: number;
  affiliate: string;
  leads: number;
  ratio: string;
  reportedRatio: string;
  deposites: number;
  reportedDeposites: number;
  affiliateBrokers: any;
  broker: string;
  payments: number;
  profit: number;
  payouts: any;
  reportedPayments: number;
  reportedPayouts: number;
}

export class ExtendedAffiliateStatistics extends AffiliateStatistics {
  index: number;
  // brokerName: string;
  // affName: string;
  countryName: string;
}
