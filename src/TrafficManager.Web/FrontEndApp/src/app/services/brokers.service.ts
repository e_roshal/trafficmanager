import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { share } from 'rxjs/operators';
import { Endpoints } from "../enums/endpoints";
import { BrokerFiltersDTO } from "../models/filters/brokerFiltersDTO";
import { Observable } from "rxjs/Observable";
import { Broker } from "../models/entities/broker";
import { WebResult } from "../models/webResult";
import { BrokerLimitsFiltersDTO } from "../models/filters/brokerLimitsFiltersDTO";
import { BrokerCountryLimit } from "../models/entities/brokerLimit";

@Injectable()
export class BrokersService {
    constructor(
        private http: HttpClient) {
    }

    updateBroker(broker) {
        let url = Endpoints.api.brokers.update.replace("%id%", broker.id);

        const observable = this.http.put(
            url,
            broker
        ).pipe(share());

        return observable;
    }

    getBrokers(filters?: BrokerFiltersDTO): Observable<WebResult<Broker[]>> {
        let url = Endpoints.api.brokers.list;
        filters.isActive = typeof filters.isActive === 'undefined' ? null : filters.isActive;
        filters.isGreater = typeof filters.isGreater === 'undefined' ? null : filters.isGreater;
        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;

        let  params = filters 
            ? new HttpParams()
                .set('BrokerName', filters.brokerName || "")
                .set('IsActive', <string><any>filters.isActive)
                .set('Countries', filters.countries || "")
                .set('IsGreater', String(filters.isGreater))
                .set('Limit', <string><any>filters.dailyLimit || "")
                .set('OrderByField', filters.orderByField || '')
                .set('IsDesc', <string><any>filters.isDesc)
                .set('PageSize', <string><any>filters.pageSize || "")
                .set('PageNumber', <string><any>filters.pageNumber || "")
            : null;

        const observable = this.http.get<WebResult<Broker[]>>(
            url,
            { params }
        ).pipe(share());
        return observable;
    }

    updateBrokerLimit(brokerLimit: BrokerCountryLimit) {
        const url = brokerLimit.id 
            ? Endpoints.api.brokerLimits.update.replace("%id%", brokerLimit.id.toString())
            : Endpoints.api.brokerLimits.update.replace("%id%", '');

        const updateFunc = brokerLimit.id 
            ? this.http.put.bind(this.http)
            : this.http.post.bind(this.http);

        const observable = updateFunc(
            url,
            brokerLimit
        ).pipe(share());

        return observable;
    }

    getBrokersLimits(filters?: BrokerLimitsFiltersDTO): Observable<WebResult<BrokerCountryLimit[]>> {
        const url = Endpoints.api.brokerLimits.list;
        filters.isGreater = typeof filters.isGreater === 'undefined' ? null : filters.isGreater;
        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;
        
        const brokerIds = filters.brokerIds &&  filters.brokerIds.length > 0
            ? filters.brokerIds.reduce((prev, curr) => `${prev}, ${curr}`)
            : "";

        let  params = filters 
            ? new HttpParams()
                .set('BrokerName', filters.brokerName || "")
                .set('BrokerIds', brokerIds)
                .set('Countries', filters.countries || "")
                .set('IsGreater', String(filters.isGreater))
                .set('Limit', <string><any>filters.limit || "")
                .set('OrderByField', filters.orderByField || "")
                .set('IsDesc', <string><any>filters.isDesc)
                .set('PageSize', <string><any>filters.pageSize || "")
                .set('PageNumber', <string><any>filters.pageNumber || "")
            : null;

        const observable = this.http.get<WebResult<BrokerCountryLimit[]>>(
            url,
            { params }
        ).pipe(share());
        return observable;
    }

    deleteBrokerLimits(idsToDelete: string): Observable<any> {
        const url = Endpoints.api.brokerLimits.list + `?ids=${idsToDelete}`;

        const observable = this.http.delete(url).pipe(share());
        return observable;
    }
}
