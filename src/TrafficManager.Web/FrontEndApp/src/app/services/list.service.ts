import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { share } from 'rxjs/operators';
import { Endpoints } from "../enums/endpoints";
import { ListDTO } from "../models/listDTO";
import { CountriesDTO } from "../models/countriesDTO";
import { NotificationService } from '../services/notification.service';
import { EntityForBooleanValue } from "../models/EntityForBooleanValue";

@Injectable()
export class ListService {

    constructor(
        private http: HttpClient,
        private notifications: NotificationService) {
    }

    static affiliates: ListDTO[] = [];
    static offers: ListDTO[] = [];
    static countries: CountriesDTO[] = [];
    static brokers: ListDTO[] = [];
    static timeInterval: ListDTO[] = [
        { id: 0, name: "00:00"},
        { id: 1, name: "00:30"},
        { id: 2, name: "01:00"},
        { id: 3, name: "01:30"},
        { id: 4, name: "02:00"},
        { id: 5, name: "02:30"},
        { id: 6, name: "03:00"},
        { id: 7, name: "03:30"},
        { id: 8, name: "04:00"},
        { id: 9, name: "04:30"},
        { id: 10, name: "05:00"},
        { id: 11, name: "05:30"},
        { id: 12, name: "06:00"},
        { id: 13, name: "06:30"},
        { id: 14, name: "07:00"},
        { id: 15, name: "07:30"},
        { id: 16, name: "08:00"},
        { id: 17, name: "08:30"},
        { id: 18, name: "09:00"},
        { id: 19, name: "09:30"},
        { id: 20, name: "10:00"},
        { id: 21, name: "10:30"},
        { id: 22, name: "11:00"},
        { id: 23, name: "11:30"},
        { id: 24, name: "12:00"},
        { id: 25, name: "12:30"},
        { id: 26, name: "13:00"},
        { id: 27, name: "13:30"},
        { id: 28, name: "14:00"},
        { id: 29, name: "14:30"},
        { id: 30, name: "15:00"},
        { id: 31, name: "15:30"},
        { id: 32, name: "16:00"},
        { id: 33, name: "16:30"},
        { id: 34, name: "17:00"},
        { id: 35, name: "17:30"},
        { id: 36, name: "18:00"},
        { id: 37, name: "18:30"},
        { id: 38, name: "19:00"},
        { id: 39, name: "19:30"},
        { id: 40, name: "20:00"},
        { id: 41, name: "20:30"},
        { id: 42, name: "21:00"},
        { id: 43, name: "21:30"},
        { id: 44, name: "22:00"},
        { id: 45, name: "22:30"},
        { id: 46, name: "23:00"},
        { id: 47, name: "23:30"}
    ];

    static type: ListDTO[] = [
        { id: 0, name: "All" },
        { id: 1, name: "CPA" },
        { id: 2, name: "CPL" }
    ];

    static isLoadedPromise: Promise<boolean>;

    logicalOperations: EntityForBooleanValue[] = [
        { name: "", value: null },
        { name: ">", value: true },
        { name: "<", value: false },
        { name: "=", value: "" as any }
    ];

    booleanValues: EntityForBooleanValue[] = [
        { name: "", value: null },
        { name: "Yes", value: true },
        { name: "No", value: false }
    ];

    static getIsLoadedPromise(): Promise<boolean> {
        return this.isLoadedPromise;
    }

    deleteCountFistElementList(count, array: ListDTO[]): ListDTO[] {
        array = array.slice(count);
        return array;
    }

    getAllLists() {
        let affPromise = this.getList(Endpoints.api.list.affiliates, 'affiliates').toPromise();
        let brokerPromise = this.getList(Endpoints.api.list.brokers, 'brokers').toPromise();
        let counriesPromise = this.getList(Endpoints.api.list.countries, 'countries').toPromise();
        let offersPromise = this.getList(Endpoints.api.list.offers, 'offers').toPromise();

        ListService.isLoadedPromise = Promise.all([
            affPromise,
            brokerPromise,
            counriesPromise,
            offersPromise
        ]) as any;

        return ListService.isLoadedPromise;
    }

    getList(url: string, property: string) {
        let observable = this.http.get(
            url
        ).pipe(share());
        observable.subscribe(
            data => {
                ListService[property] = data as any;
            },
            err => {
                this.notifications.showError(err.message);
            }
        );
        return observable;
    }

    clearLists() {
        ListService.affiliates = [];
        ListService.offers = [];
        ListService.countries = [];
        ListService.brokers = [];
        ListService.isLoadedPromise = null;
    }

    getName(id: any, itemsArray: ListDTO[]): any {

        if (id) {
            let name = itemsArray.find(o => o.id == id);
            if (name) {
                return name.name;
            } else {
                return "N/A";
            }
        } else {
            return "N/A";
        }
    }

    getFullNameCountriesStringFromIsoCountries(isoString: string): string {
        if (isoString !== "N/A" && isoString !== "" && isoString) {
            let resultString: string = '';
            let array = isoString.split('|').filter(x => x); // we dont need empty elements
            if (array.length === 0) {
                return "N/A";
            }
            array.forEach(element => {
                if (element == '**') {
                    resultString = 'All  ';
                    return;
                }

                let c = ListService.countries.find(o => o.countryIso === element);
                if (c) {
                    resultString += c.countryName + ", ";
                } else {
                    resultString += element + ", ";
                }
            });
            return resultString.slice(0, resultString.length - 2);
        } else {
            return "N/A";
        }
    }
}
