import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { share } from 'rxjs/operators';
import { Endpoints } from "../enums/endpoints";
import { UsersFilterDTO } from "../models/filters/userFiltersDTO";
import { WebResult } from "../models/webResult";
import { User } from "../models/entities/user";
import { Observable } from "rxjs/Observable";

@Injectable()
export class UsersService {
    constructor(
        private http: HttpClient) {

    }

    getUsers(filters: UsersFilterDTO): Observable<WebResult<User[]>> {
        let url = Endpoints.api.users.users;

        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;

        let params = new HttpParams()
            .set('AffiliateIds', filters.affiliateIds || '')
            .set('BrokerIds', filters.brokerIds || '')
            .set('FromDate', filters.fromDate || '')
            .set('ToDate', filters.toDate || '')
            .set('FromDepositDate', filters.fromDepositDate || '')
            .set('ToDepositDate', filters.toDepositDate || '')
            .set('Countries', filters.countries || "")
            .set('TextFilter', <string><any> filters.textFilter || '')
            .set('OrderByField', filters.orderByField || '')
            .set('IsDesc', <string><any>filters.isDesc)
            .set('PageNumber', <string><any> filters.pageNumber || '')
            .set('PageSize', <string><any> filters.pageSize || '');

        let observable = this.http.get<WebResult<User[]>>(
            url,
            { params }
        ).pipe(share());
        return observable;
    }

    sendUsersToBroker(userIds: string, brokerId: number): Observable<Object> {
        let url = Endpoints.api.users.users;

        let observable = this.http.post(
            url,
            {
                userIds,
                brokerId
            }
        ).pipe(share());
        return observable;
    }
}
