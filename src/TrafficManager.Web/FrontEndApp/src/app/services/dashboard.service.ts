import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { share } from 'rxjs/operators';
import { Endpoints } from "../enums/endpoints";

@Injectable()
export class DashboardService {
    constructor(
        private http: HttpClient) {

    }

    getData(filters) {
        let url = Endpoints.api.dashboard.apiDynamics;

        let  params = filters 
            ? new HttpParams()
                .set('fromDate', filters.fromDate || "")
                .set('toDate', filters.toDate)
            : null;
        let observable = this.http.get(
            url,
            {params}
        ).pipe(share());
        return observable;
    }
}
