import { Injectable } from "@angular/core";

@Injectable()
export class FormatterService {

    //    getCsvStringFromArrayCountries(array: any): string {
    getPipedStringFromArray(array: any): string {
        if (Array.isArray(array) && array.length > 0) {
            let resultString: string = '|';
            array.forEach(element => {
                if (element) {
                    resultString += element + "|";
                }
            });
            return resultString;
        }

        return null;
    }

    // getCsvStringFromArray(array: any): string {
    //     if (Array.isArray(array)) {
    //         let resultString: string = '';
    //         array.forEach(element => {
    //             if (element) {
    //                 resultString += element + ", ";
    //             }
    //         });
    //         return resultString.slice(0, resultString.length - 2);
    //     }
    //     return array;
    // }

    getCsvStringFromArrayForDeletion(array: any): string {
        if (Array.isArray(array)) {
            let resultString: string = '';
            array.forEach(element => {
                if (element) {
                    resultString += element.id + ", ";
                }
            });
            return resultString.slice(0, resultString.length - 2);
        }
        return array;
    }

    getNullIfValueLessThanOne(value) {
        if (value < 1) {
            return null;
        }
        return value;
    }

    convertDate(date: string): string {
        date = date && (date as any).format('YYYY-MM-DD')
            ? (date as any).format('YYYY-MM-DD')
            : date || null;
        return date;
    }
}
