import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { share } from 'rxjs/operators';
import { Endpoints } from "../enums/endpoints";
import { ListService } from "./list.service";
import { Routing } from '../models/entities/routing';
import { RoutingFiltersDTO } from "../models/filters/routingFiltresDTO";
import { WebResult } from "../models/webResult";
import { Observable } from "rxjs/Observable"; 

 
@Injectable()
export class RoutingService {
    constructor(
        private http: HttpClient,
        private listSvc: ListService) {

    }

    getRoutings(filters?: RoutingFiltersDTO): Observable<WebResult<Routing>> {
        let url = Endpoints.api.routing.routing;
        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;

        let params = new HttpParams()
        .set('brokerIds', filters.brokerIds || "")
        .set('offerIds', filters.offerIds || "")
        .set('affiliateIds', filters.affiliateIds || "")
        .set('OrderByField', filters.orderByField || '')
        .set('IsDesc', <string><any>filters.isDesc)
        .set('PageSize',  <string><any>filters.pageSize || "")
        .set('PageNumber',  <string><any>filters.pageNumber || "");

        let observable = this.http.get<WebResult<Routing>>(
            url,
            {params}
        ).pipe(share());
        return observable;
    }

    updateRouting(routing) {
        let url = Endpoints.api.routing.update.replace("%id%", routing.id);
        let observable = this.http.put(
            url,
            routing
        ).pipe(share());

        return observable;
    }

    addItemInRouting(item) {
        let url = Endpoints.api.routing.routing;
        let observable = this.http.post(
            url,
            item
        ).pipe(share());

        return observable;
    }

    getRoutingItemForRender(routingItem: Routing) {
        routingItem['affName'] = this.listSvc.getName(routingItem.affId, ListService.affiliates);
        routingItem['brokerName'] = this.listSvc.getName(routingItem.brokerId, ListService.brokers);
        routingItem['offerName'] = this.listSvc.getName(routingItem.offerId, ListService.offers);
        return routingItem;
    }

    removeMultipleRoutingElements(listIds: string) {
        let url = Endpoints.api.routing.routing;

        let params = new HttpParams()
            .set('ids', listIds || "");
        let observable = this.http.delete(
            url,
            { params }
        ).pipe(share());
        return observable;
    }
}
