import { Injectable } from "@angular/core";
import { AffiliateCountryLimit } from "../models/entities/affiliateLimit";
import { Endpoints } from "../enums/endpoints";
import { share } from "rxjs/operators";
import { AffiliateLimitsFiltersDTO } from "../models/filters/affiliateLimitsFiltersDTO";
import { Observable } from "rxjs/Observable";
import { WebResult } from "../models/webResult";
import { HttpParams, HttpClient } from "@angular/common/http";

@Injectable()
export class AffiliateService {
    constructor(
        private http: HttpClient) {
    }
    
    updateAffiliateLimit(affiliateLimit: AffiliateCountryLimit) {
        const url = affiliateLimit.id 
            ? Endpoints.api.affiliateLimits.update.replace("%id%", affiliateLimit.id.toString())
            : Endpoints.api.affiliateLimits.update.replace("%id%", '');

        const updateFunc = affiliateLimit.id 
            ? this.http.put.bind(this.http)
            : this.http.post.bind(this.http);

        const observable = updateFunc(
            url,
            affiliateLimit
        ).pipe(share());

        return observable;
    }

    getAffiliateLimits(filters?: AffiliateLimitsFiltersDTO): Observable<WebResult<AffiliateCountryLimit[]>> {
        const url = Endpoints.api.affiliateLimits.list;
        filters.isFactorGreater = typeof filters.isFactorGreater === 'undefined' ? null : filters.isFactorGreater;
        filters.isMinGreater = typeof filters.isMinGreater === 'undefined' ? null : filters.isMinGreater;
        filters.isMaxGreater = typeof filters.isMaxGreater === 'undefined' ? null : filters.isMaxGreater;
        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;
        
        const affiliateIds = filters.affiliateIds &&  filters.affiliateIds.length > 0
            ? filters.affiliateIds.reduce((prev, curr) => `${prev}, ${curr}`)
            : "";

        let  params = filters 
            ? new HttpParams()
                .set('AffiliateName', filters.affiliateName || "")
                .set('AffiliateIds', affiliateIds)
                .set('Countries', filters.countries || "")
                .set('IsFactorGreater', String(filters.isFactorGreater))
                .set('IsMinGreater', String(filters.isMinGreater))
                .set('IsMaxGreater', String(filters.isMaxGreater))
                .set('Factor', <string><any>filters.factor || "")
                .set('Min', <string><any>filters.min || "")
                .set('Max', <string><any>filters.max || "")
                .set('OrderByField', filters.orderByField || "")
                .set('IsDesc', <string><any>filters.isDesc)
                .set('PageSize', <string><any>filters.pageSize || "")
                .set('PageNumber', <string><any>filters.pageNumber || "")
            : null;

        const observable = this.http.get<WebResult<AffiliateCountryLimit[]>>(
            url,
            { params }
        ).pipe(share());
        return observable;
    }

    deleteAffiliaeLimits(idsToDelete: string): Observable<any> {
        const url = Endpoints.api.affiliateLimits.list + `?ids=${idsToDelete}`;

        const observable = this.http.delete(url).pipe(share());
        return observable;
    }

    addFTD(ids: number[]): Observable<any> {
        const url = Endpoints.api.affiliateLimits.addFTD;

        const observable = this.http.post(url, { ids }).pipe(share());
        return observable;
    }
}
