import { Injectable } from '@angular/core';
import { Charts } from '../models/entities/charts';
declare var Chart;

@Injectable()
export class ChartService {
    private charts: Charts = new Charts;



    deletedCharts() {
        if ( this.charts.lead != null) {
            this.charts.deposit.destroy();
            this.charts.lead.destroy();
        }
    }

        getChart(data: any, chartName: string) {
            let date = this.getArrayProperties(data, 'date');
            date = this.deleteTime(date);
            let amount: number[] = this.getArrayProperties(data, 'amount');
            let minAmount = Math.min.apply(null, amount);
            let maxAmount = Math.max.apply(null, amount);
            let lineChartData = {
                labels: date,
                datasets: [{
                    type: 'line',
                    borderColor: 'red',
                    backgroundColor: 'red',
                    fill: false,
                    data: amount

                }
                ]
            };
            let ctx = document.getElementById(chartName);
            this.charts[chartName] = new Chart(ctx, {
                type: "line",
                data: lineChartData,
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: chartName,
                    },
                    scales: {
                        yAxes: [{
                            type: 'linear',
                            ticks: {
                                suggestedMin: minAmount,
                                suggestedMax: maxAmount
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Amount'
                            },
                        }]
                    }
                }
            });

        }
        getArrayProperties(array, nameProperties) {
            let newArray: any[] = [];
            array.forEach(element => {
                newArray.push(element[nameProperties]);
            });
            return newArray;
        }

        deleteTime(arrayDate: string[]) {
            let newArrayDate: string[] = [];
            arrayDate.forEach( element => {
                newArrayDate.push(element.slice(0, element.length - 9 ));
            });
            return newArrayDate;
        }

    }
