import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { share } from 'rxjs/operators';
import { Endpoints } from "../enums/endpoints";
import { ListService } from "./list.service";
import { BrokerPayment } from '../models/entities/brokerPayment';
import { PaymentFilterDTO } from "../models/filters/PaymentFilterDTO";
import { WebResult } from "../models/webResult";
import { Observable } from "rxjs/Observable"; 

 
@Injectable()
export class BrokerPaymentService {
    constructor(
        private http: HttpClient) {

    }

    getBrokens(filters?: PaymentFilterDTO): Observable<WebResult<BrokerPayment>> {

        let url = Endpoints.api.brokenPayment.apiBrokens;
        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;

        let params = new HttpParams()
        .set('SubjectId', <string><any>filters.subjectId || "" )
        .set('Countries', filters.countries || "")
        .set('PaymentType', <string><any>filters.paymentType || "0")
        .set('IsDesc', <string><any>filters.isDesc)
        .set('OrderByField', <string><any>filters.orderByField || "")
        .set('PageSize',  <string><any>filters.pageSize || "")
        .set('PageNumber',  <string><any>filters.pageNumber || "");

        let observable = this.http.get<WebResult<BrokerPayment>>(
            url,
            {params}
        ).pipe(share());
        return observable;
    }

    updateBroken(brokenPayment) {
        let url = Endpoints.api.brokenPayment.update.replace("%id%", brokenPayment.id);
        let observable = this.http.put(
            url,
            brokenPayment
        ).pipe(share());

        return observable;
    }

    addItemInBroken(item) {
        let url = Endpoints.api.brokenPayment.apiBrokens;
        let observable = this.http.post(
            url,
            item
        ).pipe(share());

        return observable;
    }

 

    removeMultipleBrokenElements(listIds: string) {
        let url = Endpoints.api.brokenPayment.apiBrokens;

        let params = new HttpParams()
            .set('ids', listIds || "");
        let observable = this.http.delete(
            url,
            { params }
        ).pipe(share());
        return observable;
    }
}
