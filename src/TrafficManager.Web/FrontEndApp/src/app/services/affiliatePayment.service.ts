import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { share } from 'rxjs/operators';
import { Endpoints } from "../enums/endpoints";
import { AffiliatePayment } from '../models/entities/affiliatePayment';
import { PaymentFilterDTO } from "../models/filters/paymentFilterDTO";
import { WebResult } from "../models/webResult";
import { Observable } from "rxjs/Observable"; 

 
@Injectable()
export class AffiliatePaymentService {
    constructor(
        private http: HttpClient) {

    }

    getAffiliates(filters?: PaymentFilterDTO): Observable<WebResult<AffiliatePayment>> {

        let url = Endpoints.api.affiliatePayment.apiAffiliates;
        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;

        let params = new HttpParams()
        .set('SubjectId', <string><any>filters.subjectId || "" )
        .set('Countries', filters.countries || "")
        .set('PaymentType', <string><any>filters.paymentType || "0")
        .set('IsDesc', <string><any>filters.isDesc)
        .set('OrderByField', <string><any>filters.orderByField || "")
        .set('PageSize',  <string><any>filters.pageSize || "")
        .set('PageNumber',  <string><any>filters.pageNumber || "");

        let observable = this.http.get<WebResult<AffiliatePayment>>(
            url,
            {params}
        ).pipe(share());
        return observable;
    }

    updateAffiliate(affiliatePayment) {
        let url = Endpoints.api.affiliatePayment.update.replace("%id%", affiliatePayment.id);
        let observable = this.http.put(
            url,
            affiliatePayment
        ).pipe(share());

        return observable;
    }

    addItemInAffiliate(item) {
        let url = Endpoints.api.affiliatePayment.apiAffiliates;
        let observable = this.http.post(
            url,
            item
        ).pipe(share());

        return observable;
    }

 

    removeMultipleAffiliateElements(listIds: string) {
        let url = Endpoints.api.affiliatePayment.apiAffiliates;

        let params = new HttpParams()
            .set('ids', listIds || "");
        let observable = this.http.delete(
            url,
            { params }
        ).pipe(share());
        return observable;
    }
}
