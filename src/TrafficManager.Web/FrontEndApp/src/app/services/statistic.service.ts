import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { share } from 'rxjs/operators';
import { Endpoints } from "../enums/endpoints";
import { StatisticsFilterDTO } from "../models/filters/statisticsFilterDTO";
import { WebResult } from "../models/webResult";
import { Observable } from "rxjs/Observable";
import { AffiliateStatistics } from '../models/entities/affiliateStatistics';
import { BrokerStatistics } from '../models/entities/brokerStatistics';
import { CountryStatistics } from "../models/entities/countryStatistic";

@Injectable()
export class StatisticService {
    constructor(
        private http: HttpClient) {

    }

    getAffiliateStatistics(filters?: StatisticsFilterDTO): Observable<WebResult<AffiliateStatistics[]>> {
        let url = Endpoints.api.paymentStatistics.affiliates;

        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;

        let params = new HttpParams()
            .set('AffiliateIds', filters.affiliateIds || '')
            .set('BrokerIds', filters.brokerIds || '')
            .set('DateFrom', filters.dateFrom || '')
            .set('DateTo', filters.dateTo || '')
            .set('Countries', filters.countries || "")
            .set('OrderByField', filters.orderByField || '')
            .set('IsDesc', <string><any>filters.isDesc)
            .set('PageNumber', <string><any>filters.pageNumber || '')
            .set('PageSize', <string><any>filters.pageSize || '');
        let observable = this.http.get<WebResult<AffiliateStatistics[]>>(
            url,
            { params }
        ).pipe(share());
        return observable;
    }

    getCountryStatistics(filters?: StatisticsFilterDTO): Observable<WebResult<CountryStatistics[]>> {
        let url = Endpoints.api.paymentStatistics.countries;

        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;

        let params = new HttpParams()
            .set('AffiliateIds', filters.affiliateIds || '')
            .set('BrokerIds', filters.brokerIds || '')
            .set('DateFrom', filters.dateFrom || '')
            .set('DateTo', filters.dateTo || '')
            .set('Countries', filters.countries || "")
            .set('OrderByField', filters.orderByField || '')
            .set('IsDesc', <string><any>filters.isDesc)
            .set('PageNumber', <string><any>filters.pageNumber || '')
            .set('PageSize', <string><any>filters.pageSize || '');
        let observable = this.http.get<WebResult<CountryStatistics[]>>(
            url,
            { params }
        ).pipe(share());
        return observable;
    }

    getBrokerStatistics(filters?: StatisticsFilterDTO): Observable<WebResult<BrokerStatistics[]>> {
        let url = Endpoints.api.paymentStatistics.brokens;

        filters.isDesc = typeof filters.isDesc === 'undefined' ? null : filters.isDesc;

        let params = new HttpParams()
            .set('AffiliateIds', filters.affiliateIds || '')
            .set('BrokerIds', filters.brokerIds || '')
            .set('DateFrom', filters.dateFrom || '')
            .set('DateTo', filters.dateTo || '')
            .set('Countries', filters.countries || "")
            .set('OrderByField', filters.orderByField || '')
            .set('IsDesc', <string><any>filters.isDesc)
            .set('PageNumber', <string><any>filters.pageNumber || '')
            .set('PageSize', <string><any>filters.pageSize || '');
        let observable = this.http.get<WebResult<BrokerStatistics[]>>(
            url,
            { params }
        ).pipe(share());
        return observable;
    }

}
