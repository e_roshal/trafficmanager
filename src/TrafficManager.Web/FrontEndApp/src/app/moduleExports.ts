import {
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatSlideToggleModule,
    MatIconModule,
    MatSelectModule,
    MatCardModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDialogModule,
    MatListModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule
  } from '@angular/material';

import { LoginCmpnt } from './components/login.cmpnt';
import { DashboardCmpnt } from './components/dashboard.cmpnt';
import { NavigationCmpnt } from './components/navigation.cmpnt';
import { NotFoundComponent } from './components/notFoundComponent';
import { ProfileMenuCmpnt } from './components/profileMenu.cmpnt';
import { BrokersCmpnt } from './components/brokers.cmpnt';
import { UserMenuCmpnt } from './components/userMenu.cmpnt';
import { RoutingCmpnt } from './components/routing.cmpnt';
import { DeletionConfirmationWindowCmpnt } from './components/deletionConfirmationWindow.cmpnt';
import { ProfileCmpnt } from './components/profile.cmpnt';
import { UsersCmpnt } from './components/users.cmpnt';
import { ContextMenuCmpnt } from './components/contextMenu.Cmpnt';
import { AffiliatePaymentCmpnt } from './components/affiliatePayment.cmpnt';
import { BrokerPaymentCmpnt } from './components/brokerPayment.cmpnt';
import { AffiliateStatisticsCmpnt } from './components/affiliateStatistics.cmpnt';
import { BrokerStatisticsCmpnt } from './components/brokerStatistics.cmpnt';

import { BrokersService } from './services/brokers.service';
import { UserService } from './services/user.service';
import { NotificationService } from './services/notification.service';
import { RoutingService } from './services/routing.service';
import { ListService } from './services/list.service';
import { StatisticService } from './services/statistic.service';
import { UsersService } from './services/users.service';
import { ChartService } from './services/chart.service';
import { FormatterService } from './services/formatter.service';
import { DashboardService } from './services/dashboard.service';
import { AffiliatePaymentService } from './services/affiliatePayment.service';
import { BrokerPaymentService } from './services/brokerPayment.service';
import { CountryStatisticsCmpnt } from './components/countryStatistics.cmpnt';
import { BrokersLimitsCmpnt } from './components/brokersLimits.cmpnt';
import { AffiliatesLimitsCmpnt } from './components/affiliateLimits.cmpnt';
import { AffiliateService } from './services/affiliate.service';
import { AffiliateLimitsContextMenuCmpnt } from './components/affiliateLimitsContextMenu';
import { AddAffiliateDepositConfirmationCmpnt } from './components/addAffiliateDepositConfirmation.cmpnt';


const MATERIAL_DESING_MODULES = [
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatSlideToggleModule,
    MatIconModule,
    MatSelectModule,
    MatCardModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDialogModule,
    MatListModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule
];

const ModuleComponents  = [
    LoginCmpnt,
    DashboardCmpnt,
    NavigationCmpnt,
    NotFoundComponent,
    ProfileMenuCmpnt,
    BrokersCmpnt,
    UserMenuCmpnt,
    RoutingCmpnt,
    DeletionConfirmationWindowCmpnt,
    ProfileCmpnt,
    UsersCmpnt,
    ContextMenuCmpnt,
    BrokerPaymentCmpnt,
    AffiliatePaymentCmpnt,
    AffiliateStatisticsCmpnt,
    BrokerStatisticsCmpnt,
    CountryStatisticsCmpnt,
    BrokersLimitsCmpnt,
    AffiliatesLimitsCmpnt,
    AffiliateLimitsContextMenuCmpnt,
    AddAffiliateDepositConfirmationCmpnt
];

const ModuleServices = [
    UserService,
    NotificationService,
    BrokersService,
    RoutingService,
    ListService,
    ChartService,
    DashboardService,
    FormatterService,
    UsersService,
    BrokerPaymentService,
    AffiliatePaymentService,
    StatisticService,
    AffiliateService
];

const ModuleEntryComponents = [
    RoutingCmpnt,
    DeletionConfirmationWindowCmpnt,
    AddAffiliateDepositConfirmationCmpnt
];


export { MATERIAL_DESING_MODULES,  ModuleComponents, ModuleServices, ModuleEntryComponents };
