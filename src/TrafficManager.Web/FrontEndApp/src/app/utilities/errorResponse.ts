import { HttpErrorResponse } from "@angular/common/http";

export function getError(response: HttpErrorResponse): string {
    let result =  '';
    
    if (response.error && response.error.messages) {
        result = response.error.messages.reduce((prev, cur) => prev + `\n${ cur }`);
    } else {
        result = response.message;
    }

    return result;
}
