import { Component } from '@angular/core';

@Component({
    selector: 'not-found',
    templateUrl: '../templates/notFoundComponent.cmpnt.html'
})
export class NotFoundComponent {
    constructor() {
    }

}
