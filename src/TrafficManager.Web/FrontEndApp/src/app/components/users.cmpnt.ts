import { Component, OnInit, ViewChild, HostListener, AfterViewInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { NotificationService } from '../services/notification.service';
import { ListService } from '../services/list.service';
import { FormatterService } from '../services/formatter.service';
import { UsersFilterDTO } from '../models/filters/userFiltersDTO';
import { WebResult } from '../models/webResult';
import { User, ExtendedUser } from '../models/entities/user';
import { SiteConstants } from "../enums/siteConstants";

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { TabledFilteredComponent } from './tabledFilteredComponent';


@Component({
    selector: 'app-data',
    templateUrl: '../templates/users.cmpnt.html',
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: SiteConstants.dateFormats },
    ],
})
export class UsersCmpnt extends TabledFilteredComponent<ExtendedUser, UsersFilterDTO> implements OnInit, AfterViewInit {
    currentFilters = { pageSize: 50, pageNumber: 0, fromDate: moment() } as UsersFilterDTO;

    displayedColumns = ['select',
        'id',
        'email',
        'phone',
        'firstName',
        'lastName',
        'broker',
        'affiliate',
        'country',
        'leadDate',
        'depositDate',
        'ftd',
        // 'totalDeposit',
        'isReported',
        'callStatus',
        'offerId',
        'hasOfferId'];

    isShowContextMenu = false;
    contextmenuY: number = null;
    contextmenuX: number = null;
    defaultFromDate;

    @HostListener('window:click', ['$event'])
    keyEvent(event: MouseEvent) {
        this.OnClose(null);
    }

    constructor(
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
        private usersSvc: UsersService,
    ) {
        super(
            notifications,
            listSvc,
            formatter,
            null
        );
    }

    getData(
        filters: UsersFilterDTO
    ) {
        let _filters = Object.assign({}, filters);
        _filters.fromDate = filters.fromDate && (filters.fromDate as any).format('YYYY-MM-DD')
            ? (filters.fromDate as any).format('YYYY-MM-DD')
            : filters.fromDate || null;

        _filters.toDate = filters.toDate && (filters.toDate as any).format('YYYY-MM-DD')
            ? (filters.toDate as any).format('YYYY-MM-DD')
            : filters.toDate || null;

        _filters.fromDepositDate = filters.fromDepositDate && (filters.fromDepositDate as any).format('YYYY-MM-DD')
            ? (filters.fromDepositDate as any).format('YYYY-MM-DD')
            : filters.fromDepositDate || null;

        _filters.toDepositDate = filters.toDepositDate && (filters.toDepositDate as any).format('YYYY-MM-DD')
            ? (filters.toDepositDate as any).format('YYYY-MM-DD')
            : filters.toDepositDate || null;
        this.usersSvc.getUsers(_filters)
            .subscribe(
                (resp: WebResult<User[]>) => {
                    this.data = resp.data as ExtendedUser[];
                    this.data.forEach((element, index) => {
                        element.index = this.currentFilters.pageNumber * this.currentFilters.pageSize + index + 1;
                        element.countryName = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.country);
                    });

                    if (this.defaultFromDate) {
                        this.currentFilters.fromDate = this.defaultFromDate;
                        this.defaultFromDate = null;
                    }

                    this.dataSource.data = this.data as ExtendedUser[];
                    this.paginator.length = resp.totalItems;
                },
                err => {
                    this.notifications.showError(err.message);
                }
            );
    }

    resetFilters() {
        this.defaultFromDate = moment();
        this.currentFilters = { pageSize: 50, pageNumber: 0, fromDate: moment() } as UsersFilterDTO;
        this.getData(this.currentFilters);
    }

    OnClickRight(event, isTabel) {
        if (event.button === 2 && isTabel) {
            this.isShowContextMenu = true;
            this.contextmenuY = event.clientY;
            this.contextmenuX = event.clientX;
            document.oncontextmenu = function () { return false; };
            return;
        }
        this.isShowContextMenu = false;
        document.oncontextmenu = function () { return true; };
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.filteredData.forEach(row => this.selection.select(row));
    }

    OnClose($event) {
        this.isShowContextMenu = false;
        document.oncontextmenu = function () { return true; };
    }

    clearSelected(e) {
        if (e.isClearSelected) {
            this.selection.clear();
        }
    }

    sendToBroker(e) {
        this.usersSvc.sendUsersToBroker(e.userIds, e.brokerId)
            .toPromise()
            .then(resp => {
                this.notifications.showInfo('broker for data have been changed');
                this.getData(this.currentFilters);
            },
                err => {
                    this.notifications.showError('Cant set broker for selected data');
                });
    }
}
