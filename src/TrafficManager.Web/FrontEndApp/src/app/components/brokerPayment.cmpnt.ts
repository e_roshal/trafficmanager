import { Component, DoCheck, ApplicationRef } from '@angular/core';
import { BrokerPaymentService } from '../services/brokerPayment.service';
import { NotificationService } from '../services/notification.service';
import { MatDialog } from '@angular/material';
import { ListService } from '../services/list.service';
import { BrokerPayment } from '../models/entities/brokerPayment';
import { DeletionConfirmationWindowCmpnt } from './deletionConfirmationWindow.cmpnt';
import { FormatterService } from '../services/formatter.service';
import { PaymentFilterDTO } from '../models/filters/PaymentFilterDTO';
import { WebResult } from '../models/webResult';
import { TabledFilteredComponent } from './tabledFilteredComponent';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { SiteConstants } from "../enums/siteConstants";

@Component({
    selector: 'broker-payment',
    templateUrl: '../templates/brokerPayment.cmpnt.html',
    // styleUrls: ['./app.component.css']
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: SiteConstants.dateFormats },
    ],
})
export class BrokerPaymentCmpnt extends TabledFilteredComponent<BrokerPayment, PaymentFilterDTO> implements DoCheck {

    brokerPayments: any[] = [];
    currentFiltersOfferName;
    currentFilters = { pageSize: 50, pageNumber: 0 } as PaymentFilterDTO;
    
    editId: number = null;
    isActiveInProgress: boolean = false;
    item: BrokerPayment = null;
    all: string = "**";
    additionInProgress: boolean = false;
    defaultItem: BrokerPayment = {
        id: null,
        brokerId: 0,
        amount: 0,
        validTo: '',
        paymentType: null,
        country: "",
        validFrom: ""
    };
    isCheckBroker: boolean = true;
    displayedColumns = ['select', 'id', 'country', 'validFrom', 'validTo', 'paymentType', 'amount', 'buttons'];
    constructor(
        private brokenSvc: BrokerPaymentService,
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
        dialog: MatDialog,
        ) {
        super(
            notifications,
            listSvc,
            formatter,
            dialog
        );
    }
    ngDoCheck(): void {
        if (this.currentFilters.subjectId) {
            this.isCheckBroker = false;
        }
    }


    getData(filters: PaymentFilterDTO) {
        this.brokenSvc.getBrokens(filters).subscribe(
            (resp: WebResult<BrokerPayment>) => {
                this.brokerPayments = resp.data as any;
                this.brokerPayments.forEach((element, index) => {
                    if (this.currentFilters.pageNumber == undefined) {
                        this.currentFilters.pageNumber = 0;
                    }
                    if (this.editId != null || this.additionInProgress) {
                        this.editId = null;
                        if (this.additionInProgress) {
                            this.item = null;
                            this.additionInProgress = false;
                        }
                    }
                    element.typeName = this.listSvc.getName(element.paymentType, this.getList("type"));
                    let countryName = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.country);
                    element.countryName = this.all == countryName ? "All" : countryName;
                    element.index = this.currentFilters.pageNumber * this.currentFilters.pageSize + index + 1;
                });
                this.dataSource.data = this.brokerPayments;
                this.paginator.length = resp.totalItems;
            },
            err => {
                this.notifications.showError(err.message);
            }
        );
    }

    addItem() {

        if (this.requestInProgress || this.selection.selected.length > 0 || this.additionInProgress) {
            return;
        }
        if (this.editId != null) {
            this.closeEdit();
        }
        this.item = Object.assign({}, this.defaultItem);
        this.brokerPayments.push(this.item);
        this.dataSource.data = this.brokerPayments;
        this.additionInProgress = true;
    }
    validationOfAnOperation() {
        this.item.brokerId = parseInt(this.currentFilters.subjectId.toString());
        let isValid = true;
        
        isValid = isValid && this.item.paymentType != null;
        isValid = isValid && !!this.item.validFrom;
        isValid = isValid && !!this.item.country;

        if (isValid) {

            if (this.requestInProgress) {
                return;
            }
            if (this.additionInProgress) {
                this.add();

            }
            else {
                this.update(this.item);
            }
        }
        else {
            this.notifications.showError("Please fill values");
        }
    }

    add() {
        this.item.id = 0;
        if (typeof (this.item.validTo) == 'object') {
            this.item.validTo = this.formatter.convertDate(this.item.validTo);
        }
        if (typeof (this.item.validFrom) == 'object') {
            this.item.validFrom = this.formatter.convertDate(this.item.validFrom);
        }
        
        this.brokenSvc.addItemInBroken(this.item).subscribe(
            (resp: any) => {

                this.notifications.showSuccess(`Broken payment [${resp.data.id}] added`);
                this.requestInProgress = false;
                this.additionInProgress = false;
                this.getData(this.currentFilters);
            },
            err => {
                this.item.id = null;
                this.notifications.showError(err.error.messages.join('; '));
            }
        );
    }

    update(item) {
        this.requestInProgress = true;
        if (typeof (this.item.validTo) == 'object') {
            this.item.validTo = this.formatter.convertDate(this.item.validTo);
        }
        if (typeof (this.item.validFrom) == 'object') {
            this.item.validFrom = this.formatter.convertDate(this.item.validFrom);
        }
        this.brokenSvc.updateBroken(item)
            .subscribe(
                (result: any) => { // sucess
                    this.getData(this.currentFilters);
                    this.editId = null;
                    this.isActiveInProgress = false;
                    this.notifications.showSuccess(`Broken payment [${result.data.id}] updated`);
                }, err => { // error
                    this.notifications.showError(err.error.messages);
                    this.requestInProgress = false;
                    this.isActiveInProgress = false;
                    this.additionInProgress = false;
                }, () => { // complete
                    this.requestInProgress = false;
                    this.additionInProgress = false;
                });
    }


    closeEdit() {
        this.editId = null;
        if (this.additionInProgress) {
            this.item = null;
            this.brokerPayments = this.brokerPayments.slice(0, this.brokerPayments.length - 1);
            this.dataSource.data = this.brokerPayments;
            this.additionInProgress = false;
        }
    }

    removeMultipleBrokerElements(that) {
        let listIdsForDelete = that.formatter.getCsvStringFromArrayForDeletion(that.selection.selected);
        that.brokenSvc.removeMultipleBrokenElements(listIdsForDelete).subscribe(
            (resp: any) => {
                this.selection.clear();
                this.isDeleteDisabled = true;
                this.getData(this.currentFilters);
            },
            err => {
                that.notifications.showError(err.message);
            }
        );
    }

    openDialog() {
        let that = this;
        let dialogRef = this.dialog.open(DeletionConfirmationWindowCmpnt, {
            width: '400px',
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                that.removeMultipleBrokerElements(that);
            }
        });
    }
    toggleView(row: any) {
        if (!this.isCheckBroker) {
            if (!this.requestInProgress && this.selection.selected.length === 0 && !this.additionInProgress) {
                this.item = Object.assign({}, this.brokerPayments.find(rt => rt.id === row.id));
                if (!this.isActiveInProgress) {
                    this.editId = row.id;
                }
            }
        } else {
            this.notifications.showError("Select broker");
        }
    }

    resetFilters() {
        this.currentFilters = { pageSize: 50, pageNumber: 0 } as PaymentFilterDTO;
        this.currentFiltersOfferName = null;
        this.isCheckBroker = true;
        this.getData(this.currentFilters);
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.filteredData.forEach(row => this.selection.select(row));
        this.isSelected();
    }

    selectionToggle(row) {
        this.selection.toggle(row);
        this.isSelected();
    }

    fastFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        this.selection.clear();
        this.isSelected();
        this.selection.clear();
    }
}
