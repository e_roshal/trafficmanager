import { Component, OnInit, ApplicationRef } from '@angular/core';
import { UserService } from '../services/user.service';
import { ChangeDetectorRef } from '@angular/core';
import { NotificationService } from '../services/notification.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'login',
    templateUrl: '../templates/login.cmpnt.html'
})
export class LoginCmpnt {
    username = ''; // 'admin';
    password = ''; // 'Password@123';

    constructor(
        private userSvc: UserService,
        private notifications: NotificationService
    ) {
        this.userSvc.hideMenu = true;
    }

    login(e) {
        e.preventDefault();

        this.userSvc.logIn(this.username, this.password)
            .subscribe((data) => {
                this.userSvc.hideMenu = false;
            }, (err) => {
                this.notifications.showError(err.message);
            });

        return false;
    }
}
