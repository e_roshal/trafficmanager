import { Component, DoCheck, ApplicationRef } from '@angular/core';
import { NotificationService } from '../services/notification.service';
import { ListService } from '../services/list.service';
import { FormatterService } from '../services/formatter.service';
import { StatisticsFilterDTO } from '../models/filters/statisticsFilterDTO';
import { StatisticService } from '../services/statistic.service';
import { TabledFilteredComponent } from './tabledFilteredComponent';
import { CountryStatistics, ExtendedCountryStatistics } from '../models/entities/countryStatistic';
import { WebResult } from '../models/webResult';

import { SiteConstants } from "../enums/siteConstants";
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { Angular5Csv } from 'angular5-csv/Angular5-csv';

@Component({
    selector: 'country-statistics',
    templateUrl: '../templates/countryStatistics.cmpnt.html',
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: SiteConstants.dateFormats },
    ]
})
export class CountryStatisticsCmpnt extends TabledFilteredComponent<ExtendedCountryStatistics, StatisticsFilterDTO>  {

    currentFilters = { pageSize: 50, pageNumber: 0, dateFrom: moment() } as StatisticsFilterDTO;

    options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: false,
        useBom: true,
        noDownload: false,
        headers: [
        'id',
        'country',
        'leads',
        'ratio',
        'deposites',
        'reportedRatio',
        'reportedDeposites',
        'affiliates',
        'brokers',
        'payouts',
        'payments',
        'profit'
    ]
      };

    displayedColumns = [
        'id',
        'country',
        'leads',
        'ratio',
        'deposites',
        'reportedRatio',
        'reportedDeposites',
        'affiliates',
        'brokers',
        'payouts',
        'payments',
        'profit'];

    currentData: ExtendedCountryStatistics[] = [];

    defaultdateFrom;

    constructor(
        private statisticSvc: StatisticService,
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
    ) {
        super(
            notifications,
            listSvc,
            formatter,
            null
        );
    }

    getData(
        filters: StatisticsFilterDTO
    ) {
        let _filters = Object.assign({}, filters);
        _filters.dateFrom = filters.dateFrom && (filters.dateFrom as any).format('YYYY-MM-DD')
            ? (filters.dateFrom as any).format('YYYY-MM-DD')
            : filters.dateFrom || null;

        _filters.dateTo = filters.dateTo && (filters.dateTo as any).format('YYYY-MM-DD')
            ? (filters.dateTo as any).format('YYYY-MM-DD')
            : filters.dateTo || null;

       
        this.statisticSvc.getCountryStatistics(_filters)
            .subscribe(
                (resp: WebResult<CountryStatistics[]>) => {
                    this.data = resp.data as ExtendedCountryStatistics[];
                    this.data.forEach((element, index) => {
                        if (element.country != null) {
                            element.country = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.country);
                            element.index = this.currentFilters.pageNumber * this.currentFilters.pageSize + index + 1;
                            element['affiliatesName'] = '';
                            element.affiliates.forEach(item => {
                                element['affiliatesName'] += this.listSvc.getName(item, this.getList('affiliates')) + ', ';
                            });
                            element['affiliatesName']  = element['affiliatesName'] .slice(0, element['affiliatesName'].length - 2);
                            element['brokersName'] = '';
                            element.brokers.forEach(item => {

                                element['brokersName'] += this.listSvc.getName(item, this.getList('brokers')) + ', ';
                            });
                            element['brokersName'] = element['brokersName'].slice(0, element['brokersName'].length - 2);
                        }
                        else {
                            element.country = 'Total';
                        }
                    });


                    if (this.defaultdateFrom) {
                        this.currentFilters.dateFrom = this.defaultdateFrom;
                        this.defaultdateFrom = null;
                    }

                    this.dataSource.data = this.data as ExtendedCountryStatistics[];
                    this.paginator.length = resp.totalItems;
                },
                err => {
                    this.notifications.showError(err.message);
                }
            );
    }

    resetFilters() {
        this.defaultdateFrom = moment();
        this.currentFilters = { pageSize: 50, pageNumber: 0, dateFrom: moment() } as StatisticsFilterDTO;
        this.getData(this.currentFilters);
    }

    saveToCsv() {
        let arrayToSave = this.getArrayToSave(this.data);
        for (let i = 0; i < arrayToSave.length - 1; i++) {
            arrayToSave[i].id = i + 1; 
        }
        let result = new Angular5Csv(arrayToSave, "AllRecords", this.options);
    }

    
  getArrayToSave(array: ExtendedCountryStatistics[]) {
    return array.map((x: ExtendedCountryStatistics) => {
      return {
        id: x.id || "",
        country: x.country,
        leads: x.leads,
        ratio: x.ratio,
        deposites: x.deposites,
        reportedRaio: x.reportedRatio,
        reportedDeposites: x.reportedDeposites,
        affiliates: x['affiliatesName'] || '',
        brokers: x['brokersName'] || '',
        income: x.payouts,
        outcome: x.reportedPayments,
        profit: x.payouts - x.reportedPayments
      };
    });
  }

}
