import { Component } from '@angular/core';
import { NotificationService } from '../services/notification.service';
import { ListService } from '../services/list.service';
import { FormatterService } from '../services/formatter.service';
import { StatisticsFilterDTO } from '../models/filters/statisticsFilterDTO';

import { StatisticService } from '../services/statistic.service';
import { TabledFilteredComponent } from './tabledFilteredComponent';
import { BrokerStatistics, ExtendedBrokerStatistics } from '../models/entities/brokerStatistics';
import { WebResult } from '../models/webResult';

import { SiteConstants } from "../enums/siteConstants";
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { Angular5Csv } from 'angular5-csv/Angular5-csv';

@Component({
    selector: 'broker-statistics',
    templateUrl: '../templates/brokerStatistics.cmpnt.html',
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: SiteConstants.dateFormats },
    ]
})
export class BrokerStatisticsCmpnt extends TabledFilteredComponent<ExtendedBrokerStatistics, StatisticsFilterDTO>  {
    currentFilters = { pageSize: 50, pageNumber: 0, dateFrom: moment() } as StatisticsFilterDTO;

    options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: false,
        useBom: true,
        noDownload: false,
        headers: ['id',
            'broker',
            'leads',
            'ratio',
            'deposites',
            'reportedRatio',
            'reportedDeposites',
            'affiliate',
            'income',
            'outcome',
            'profit']
    };

    displayedColumns = [
        'id',
        'broker',
        'leads',
        'ratio',
        'deposites',
        'reportedRatio',
        'reportedDeposites',
        'affiliate',
        'payments',
        'payouts',
        'profit'
    ];

    defaultdateFrom;

    constructor(
        private statisticSvc: StatisticService,
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
    ) {
        super(
            notifications,
            listSvc,
            formatter,
            null
        );
    }

    getData(
        filters: StatisticsFilterDTO
    ) {
        let _filters = Object.assign({}, filters);
        _filters.dateFrom = filters.dateFrom && (filters.dateFrom as any).format('YYYY-MM-DD')
            ? (filters.dateFrom as any).format('YYYY-MM-DD')
            : filters.dateFrom || null;

        _filters.dateTo = filters.dateTo && (filters.dateTo as any).format('YYYY-MM-DD')
            ? (filters.dateTo as any).format('YYYY-MM-DD')
            : filters.dateTo || null;


        this.statisticSvc.getBrokerStatistics(_filters)
            .subscribe(
                (resp: WebResult<BrokerStatistics[]>) => {

                    this.data = resp.data as ExtendedBrokerStatistics[];
                    this.data.forEach((element, index) => {

                        if (element.brokerAffiliates != null) {
                            element.index = this.currentFilters.pageNumber * this.currentFilters.pageSize + index + 1;
                            element.broker = this.listSvc.getName(element.brokerId, this.getList('brokers'));
                            element.affiliate = "";

                            element.brokerAffiliates.forEach(item => {
                                element.affiliate += this.listSvc.getName(item, this.getList('affiliates')) + ', ';
                            });
                            element.affiliate = element.affiliate.slice(0, element.affiliate.length - 2);
                        }
                        else {
                            element.broker = 'Total';

                        }
                    });

                    if (this.defaultdateFrom) {
                        this.currentFilters.dateFrom = this.defaultdateFrom;
                        this.defaultdateFrom = null;
                    }

                    this.dataSource.data = this.data as ExtendedBrokerStatistics[];
                    this.paginator.length = resp.totalItems;
                },
                err => {
                    this.notifications.showError(err.message);
                }
            );
    }

    resetFilters() {
        this.defaultdateFrom = moment();
        this.currentFilters = { pageSize: 50, pageNumber: 0, dateFrom: moment() } as StatisticsFilterDTO;
        this.getData(this.currentFilters);
    }

    saveToCsv() {
        let arrayToSave = this.getArrayToSave(this.data);
        for (let i = 0; i < arrayToSave.length - 1; i++) {
            arrayToSave[i].id = i + 1;
        }
        let result = new Angular5Csv(arrayToSave, "AllRecords", this.options);
    }


    getArrayToSave(array: ExtendedBrokerStatistics[]) {
        return array.map((x: ExtendedBrokerStatistics) => {
            return {
                id: x.id || "",
                broker: x.broker,
                leads: x.leads,
                ratio: x.ratio,
                deposites: x.deposites,
                reportedRaio: x.reportedRatio,
                reportedDeposites: x.reportedDeposites,
                affiliate: x.affiliate || "",
                income: x.payouts,
                outcome: x.reportedPayments,
                profit: x.payouts - x.reportedPayments
            };
        });
    }


}
