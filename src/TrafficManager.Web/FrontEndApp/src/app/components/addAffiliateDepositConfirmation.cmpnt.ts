import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ListService } from '../services/list.service';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'add-affiliate-deposit-сonfirmation',
    templateUrl: '../templates/addAffiliateDepositConfirmation.cmpnt.html',
  })
  export class AddAffiliateDepositConfirmationCmpnt {
  
    constructor(
      private listSvc: ListService,
      public dialogRef: MatDialogRef<AddAffiliateDepositConfirmationCmpnt>,
      @Inject(MAT_DIALOG_DATA) public data: any) { }
  
    getDescription() {
        return this.data.recordsCount > 0 
            ? this.data.recordsCount + ' records'
            : this.listSvc.getName(this.data.record.affiliateId, ListService['affiliates']);
    }

    onNoClick(): void {
      this.dialogRef.close();
    }
  
  }
