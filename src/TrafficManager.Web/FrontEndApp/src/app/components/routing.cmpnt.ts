import { Component, OnInit, ViewChild, AfterViewInit, HostListener, ApplicationRef } from '@angular/core';
import { RoutingService } from '../services/routing.service';
import { NotificationService } from '../services/notification.service';
import { MatDialog } from '@angular/material';
import { ListService } from '../services/list.service';
import { Routing } from '../models/entities/routing';
import { DeletionConfirmationWindowCmpnt } from '../components/deletionConfirmationWindow.cmpnt';
import { FormatterService } from '../services/formatter.service';
import { RoutingFiltersDTO } from '../models/filters/routingFiltresDTO';
import { WebResult } from '../models/webResult';
import { TabledFilteredComponent } from './tabledFilteredComponent';
import { isArray } from 'util';


@Component({
    selector: 'app-routing',
    templateUrl: '../templates/routing.cmpnt.html'
})

export class RoutingCmpnt extends TabledFilteredComponent<Routing, RoutingFiltersDTO> implements OnInit, AfterViewInit {
    routings: any[] = [];
    editId: number = null;
    fromTime: string = '';
    toTime: string = '';
    additionInProgress: boolean = false;
    requestInProgress: boolean = false;
    isActiveInProgress: boolean = false;
    defaultItem: Routing = {
        id: null,
        affId: '',
        brokerId: null,
        priority: 1,
        offerId: '',
        active: false,
        affiliate: '',
        broker: '',
        countries: '',
        from: '',
        to: '',
        offer: '',
        limit: 1000
    };
    displayedColumns = [
        'select',
        'id',
        'affId',
        'offerId',
        'brokerId',
        'priority',
        'countries',
        'from',
        'to',
        'active',
        'limit',
        'buttons'];
    item: Routing = null;

    currentFilters = { pageSize: 50, pageNumber: 0 } as RoutingFiltersDTO;

    currentFiltersOfferName;

    @ViewChild('tagsSelect') tagsSelect;
    @ViewChild('tagsSelectCurrentFilters') tagsSelectCurrentFilters;

    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        if (event.keyCode === 27) {
            if (this.editId != null || this.additionInProgress) {
                this.closeEdit(this.item);
            }
        }
    }

    constructor(
        private routingSvc: RoutingService,
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
        dialog: MatDialog) {
        super(
            notifications,
            listSvc,
            formatter,
            dialog
        );
    }

    getData(filters: RoutingFiltersDTO) {
        this.routingSvc.getRoutings(filters).subscribe(
            (resp: WebResult<Routing>) => {
                this.routings = resp.data as any;
                this.routings.forEach((element, index) => {
                    if (this.currentFilters.pageNumber == undefined) {
                        this.currentFilters.pageNumber = 0;
                    }
                    element['countriesName'] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.countries);
                    if (this.editId != null || this.additionInProgress) {
                        this.editId = null;
                        if (this.additionInProgress) {
                            this.item = null;
                            this.additionInProgress = false;
                        }
                    }
                    element.index = this.currentFilters.pageNumber * this.currentFilters.pageSize + index + 1;
                });
                this.dataSource.data = this.routings;
                this.paginator.length = resp.totalItems;
            },
            err => {
                this.notifications.showError(err.message);
            }
        );
    }

    toggleView(row: any) {
        if (!this.requestInProgress && this.selection.selected.length === 0 && !this.additionInProgress) {
            this.item = Object.assign({}, this.routings.find(rt => rt.id === row.id));
            if (!this.isActiveInProgress) {
                this.editId = row.id;
            }
        }
    }

    add(element) {
        element.id = 0;
        this.routings[this.routings.length - 1] = this.routingSvc.getRoutingItemForRender(element);
        this.routingSvc.addItemInRouting(element).subscribe(
            (resp: any) => {
                this.notifications.showSuccess(`routing [${resp.data.id}] added`);
                this.requestInProgress = false;
                this.additionInProgress = false;
                this.getData(this.currentFilters);
            },
            err => {
                element.id = null;
                if (err.error.message == undefined) {
                if (err.error.length > 1) {
                    let message = '';
                    err.error.forEach( x => {
                        message += x + ', ' ;
                    });
                    message = message.slice(0, message.length - 2);
                    this.notifications.showError(message);
                } else {
                    this.notifications.showError(err.error);
                }
            } else {
                this.notifications.showError(err.error.message);
            }
                
            }
        );
    }

    validationOfAnOperation(item) {
        if (!item.brokerId) {
            this.notifications.showError("Broker should not be empty");
            return;
        }
        if (item.affId != "" || item.offerId != "") {
            if (this.requestInProgress) {
                return;
            }
            if (this.additionInProgress) {
                this.add(item);
            }
            else {
                this.update(item);
            }
        }
        else {
            this.notifications.showError("Fill one of the fields Affiliate or Offer");
        }
    }

    update(item) {
        this.requestInProgress = true;
        this.routingSvc.updateRouting(item)
            .subscribe(
                (result: any) => { // sucess
                    this.getData(this.currentFilters);
                    this.editId = null;
                    this.isActiveInProgress = false;
                    this.notifications.showSuccess(`routing [${result.data.id}] updated`);
                }, err => { // error
                    if (err.error.message == undefined) {
                        if (err.error.length > 1) {
                            let message = '';
                            err.error.forEach( x => {
                                message += x + ', ' ;
                            });
                            message = message.slice(0, message.length - 2);
                            this.notifications.showError(message);
                        } else {
                            this.notifications.showError(err.error);
                        }
                    } else {
                        this.notifications.showError(err.error.message);
                    }
                    this.requestInProgress = false;
                    this.isActiveInProgress = false;
                    this.additionInProgress = false;
                }, () => { // complete
                    this.requestInProgress = false;
                    this.additionInProgress = false;
                });
    }

    addItem() {
        if (this.requestInProgress || this.selection.selected.length > 0 || this.additionInProgress) {
            return;
        }
        if (this.editId != null) {
            this.closeEdit(this.item);
        }
        this.item = Object.assign({}, this.defaultItem);
        this.routings.push(this.item);
        this.dataSource.data = this.routings;
        this.additionInProgress = true;
    }

    deleteElements() {
        const that = this;
        let listIdsForDelete = that.formatter.getCsvStringFromArrayForDeletion(that.selection.selected);
        that.isDeleteDisabled = true;
        that.routingSvc.removeMultipleRoutingElements(listIdsForDelete).subscribe(
            (resp: any) => {
                that.selection.clear();
                this.getData(this.currentFilters);
            },
            err => {
                that.notifications.showError(err.message);
            }
        );
    }


    closeEdit(item: Routing) {
        let index = this.routings.findIndex(x => x.id == item.id);
        this.routings[index] = this.item;
        this.dataSource.data = this.routings;
        this.editId = null;
        if (this.additionInProgress) {
            item = null;
            this.routings = this.routings.slice(0, this.routings.length - 1);
            this.dataSource.data = this.routings;
            this.additionInProgress = false;
        }
    }
    checkedElementActive(element) {
        if (!this.additionInProgress || element.id != null) {
            return element.active;
        }
    }

    onActive(element) {
        element.active = !element.active;
        if (!this.additionInProgress) {
            this.isActiveInProgress = true;
            this.selection.clear();
            this.isSelected();
            this.update(element);
        }
    }


    resetFilters() {
        this.currentFilters = { pageSize: 50, pageNumber: 0 } as RoutingFiltersDTO;
        this.currentFiltersOfferName = null;
        this.getData(this.currentFilters);
    }


    changeOfferId(newValue, element) {
        element.offerId = newValue;
        element.offer = newValue;
        this.tagsSelect.value = null;
    }

    changeCurrentFiltersOfferIds(newValue) {
        if (this.currentFiltersOfferName) {
            let index = this.currentFilters.offerIds.indexOf(this.currentFiltersOfferName);
            this.currentFilters.offerIds[index] = newValue;
        }
        else {
            if (!isArray(this.currentFilters['offerIds'])) {
                this.currentFilters.offerIds = [];
            }
            this.currentFilters.offerIds.push(newValue);
        }
        this.currentFiltersOfferName = newValue;
        this.clearTagsSelectCurrentFiltersValue();
    }

    addValueInput() {
        this.currentFilters.offerIds.push(this.currentFiltersOfferName);
        this.clearTagsSelectCurrentFiltersValue();
    }
    stopEvent() {
        event.stopPropagation();
        this.tagsSelectCurrentFilters.close();
    }

    clearTagsSelectCurrentFiltersValue() {
        if (this.tagsSelectCurrentFilters.value == this.currentFiltersOfferName) {
            this.tagsSelectCurrentFilters.value = null;
        }
    }

    saveTime(element: Routing, value: any, type: string) {
        if (!value) {
            value = "";
            element[type] = null;
        }
        element[type] = value;

    }

    saveCountries(element: Routing, value: any) {
        if (!value) {
            value = "";
            element.countries = null;
        }

        element.countries = this.formatter.getPipedStringFromArray(value);
        element["countriesName"] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.countries);

    }

}
