import { Component, HostListener } from "@angular/core";
import { TabledFilteredComponent } from "./tabledFilteredComponent";
import { BrokersService } from "../services/brokers.service";
import { NotificationService } from "../services/notification.service";
import { ListService } from "../services/list.service";
import { FormatterService } from "../services/formatter.service";
import { BrokerLimitsFiltersDTO } from "../models/filters/brokerLimitsFiltersDTO";
import { BrokerCountryLimit } from "../models/entities/brokerLimit";
import { ActivatedRoute } from "@angular/router";
import { getError } from "../utilities/errorResponse";
import { HttpErrorResponse } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import { AffiliateService } from "../services/affiliate.service";
import { AffiliateLimitsFiltersDTO } from "../models/filters/affiliateLimitsFiltersDTO";
import { AffiliateCountryLimit } from "../models/entities/affiliateLimit";
import { AffiliateLimitType } from "../enums/affiliateLimitType";

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'affiliates-limits',
    templateUrl: '../templates/affiliatesLimits.cmpnt.html'
})
export class AffiliatesLimitsCmpnt extends TabledFilteredComponent<AffiliateCountryLimit, AffiliateLimitsFiltersDTO> {
    
    AffiliateLimitType = Object.values(AffiliateLimitType);
    currentFilters: AffiliateLimitsFiltersDTO = { pageSize: 50, pageNumber: 0 } as AffiliateLimitsFiltersDTO;

    requestInProgress: boolean = false;
    additionInProgress: boolean = false;
    
    editId: number = null;    
    editingItem: AffiliateCountryLimit = null;

    showContextMenu = false;
    contextmenuY = 0;
    contextmenuX = 0;

    selectedElement: AffiliateCountryLimit = null;

    defaultItem = {
        countries: '',
        type: AffiliateLimitType.absolute
    } as AffiliateCountryLimit;

    displayedColumns = [
        'select',
        'id',
        'affiliate',
        'countries',
        'type',
        'min',
        'max',
        'factor',
        'buttons' ];

    @HostListener('window:click', ['$event'])
        keyEvent(event: MouseEvent) {
            this.showContextMenu = false;
            document.oncontextmenu = function () { return true; };
        }
    


    constructor(
        private affiliateSvc: AffiliateService,
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
        dialog: MatDialog
    ) {
        super(
            notifications,
            listSvc,
            formatter,
            dialog
        );
    }

    getData() {
        this.additionInProgress = false;

        this.affiliateSvc.getAffiliateLimits(this.currentFilters).subscribe(x => {
            this.data = x.data;
            this.data.forEach((element, index) => {
                element['affiliate'] = this.listSvc.getName(element.affiliateId, ListService.affiliates);
                element['countriesName'] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.countries);
            });

            this.dataSource.data = this.data;
            this.paginator.length = x.totalItems;

        }, err => {
            this.notifications.showError(err.message);
        });
    }

    toggleView(row: AffiliateCountryLimit) {
        if (!this.requestInProgress && this.selection.selected.length === 0 && !this.additionInProgress) {
            this.editingItem = Object.assign({}, this.data.find(x => x.id === row.id));
            this.editId = row.id;
        }
    }

    closeEdit(item: AffiliateCountryLimit) {
        let index = this.data.findIndex(x => x.id == item.id);
        this.data[index] = this.editingItem;
        this.dataSource.data = this.data;
        this.editId = null;

        if (this.additionInProgress) {
            item = null;
            this.data = this.data.slice(0, this.data.length - 1);
            this.dataSource.data = this.data;
            this.additionInProgress = false;
        }
    }

    addItem() {
        if (this.requestInProgress || this.selection.selected.length > 0 || this.additionInProgress) {
            return;
        }
        if (this.editId != null) {
            this.closeEdit(this.editingItem);
        }
        this.editingItem = Object.assign({}, this.defaultItem);
        this.data.push(this.editingItem);
        this.dataSource.data = this.data;
        this.additionInProgress = true;
    }

    saveCountries(element: AffiliateCountryLimit, value: any) {
        if (!value) {
            value = "";
            element.countries = null;
        }

        element.countries = this.formatter.getPipedStringFromArray(value);
        element["countriesName"] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.countries);
    }

    save(element: AffiliateCountryLimit) {
        let observable = this.affiliateSvc.updateAffiliateLimit(element);
        observable.subscribe(
            (result: any) => { // sucess
                this.notifications.showSuccess(`AffiliateLimit [${ result.data.id }] ${ element.id ? 'updated' : 'added' }`);
                this.requestInProgress = false;
                this.additionInProgress = false;
                this.editId = null;

                this.getData();
            }, (err: HttpErrorResponse) => { // error
                const errorMsg = getError(err);
                this.notifications.showError(errorMsg);
            }, () => { // complete
                this.requestInProgress = false;
            });
        return observable;
    }

    deleteElements() {
        
        let listIdsForDelete = this.formatter.getCsvStringFromArrayForDeletion(this.selection.selected);
        this.isDeleteDisabled = true;
        
        this.affiliateSvc.deleteAffiliaeLimits(listIdsForDelete)
            .subscribe( val => {
                this.notifications.showInfo(`Deleted [${ val }] records`);
                this.selection.clear();
                this.getData();
            },
            err => {
                this.notifications.showError(err.message);
            });
    }

    OnClickRight(event, isTabel, element) {
        if (event.button === 2 && isTabel) {
            this.selectedElement = element;
            this.showContextMenu = true;
            
            this.contextmenuY = event.clientY;
            this.contextmenuX = event.clientX;
            document.oncontextmenu = function () { return false; };
            return;
        }
        this.showContextMenu = false;
        document.oncontextmenu = function () { return true; };
    }
}
