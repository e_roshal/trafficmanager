import { Component, Inject } from '@angular/core';
import { UserService } from '../services/user.service';
import { ChangePasswordDTO } from '../models/changePasswordDTO';
import { NotificationService } from '../services/notification.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'profile',
    templateUrl: '../templates/profile.cmpnt.html',
})


export class ProfileCmpnt {

    changePasswordDTO: ChangePasswordDTO = new ChangePasswordDTO;

    constructor(
        private userSvc: UserService,
        private notifications: NotificationService,
    ) {
    }

    changePassword(oldPassword, newPassword) {
        this.userSvc.getAntiforgeryKey().subscribe(
            (resp) => {
                this.changePasswordDTO.key = resp;
                this.changePasswordDTO.password = oldPassword;
                this.changePasswordDTO.newPassword = newPassword;
                this.userSvc.changePassword(this.changePasswordDTO).subscribe(
                    () => {
                        this.notifications.showSuccess("Password has been changed.");
                    },
                    err => {
                        let msg = err && err.error && err.error[0] && err.error[0].description;
                        this.notifications.showError(msg || "Error occured");
                    }
                );
            },
            err => {
                this.notifications.showError(err.message);
            }
        );

    }

}
