import { Component } from '@angular/core';
import { UserService } from '../services/user.service';
import { SecurityService } from '../services/security.service';
import { Router } from '@angular/router';

@Component({
    selector: 'profile-menu',
    templateUrl: '../templates/profileMenu.cmpnt.html'
})
export class ProfileMenuCmpnt {
    showMenu: boolean = false;

    constructor(
        private userSvc: UserService,
        private router: Router,
        private securitySvc: SecurityService
    ) {
    }

    profile() {
        this.showMenu = false;
        this.router.navigate(['profile']);
    }

    toggleMenu() {
        this.showMenu = !this.showMenu;
    }

    logOut() {
        this.userSvc.logOut();
    }
}
