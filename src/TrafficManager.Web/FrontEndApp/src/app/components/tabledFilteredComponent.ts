import { OnInit, AfterViewInit, ViewChild } from "@angular/core";
import { NotificationService } from "../services/notification.service";
import { ListService } from "../services/list.service";
import { FormatterService } from "../services/formatter.service";
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from "@angular/material";
import { SiteConstants } from "../enums/siteConstants";
import { SelectionModel } from '@angular/cdk/collections';
import { StandardFilters } from "../models/filters/standardFilters";
import { DeletionConfirmationWindowCmpnt } from "./deletionConfirmationWindow.cmpnt";

export class TabledFilteredComponent<T, F extends StandardFilters> implements OnInit, AfterViewInit {
    data: T[];
    currentFilters: F;
    selection = new SelectionModel(true, []);
    requestInProgress: boolean = false;
    displayedColumns: string[];

    itemsPerPage = SiteConstants.itemsPerPage;

    isDeleteDisabled: boolean = true;

    dataSource = new MatTableDataSource(this.data);
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(
        public notifications: NotificationService,
        public listSvc: ListService,
        public formatter: FormatterService,
        public dialog: MatDialog
    ) {
    }

    selectAll(data, filterItem, listName, dataField = 'id') {
        this.currentFilters[filterItem] = !data.deselectAll
            ? this.getList(listName).map(x => x[dataField]).filter(x => x)
            : [];
    }

    ngAfterViewInit() {
        // this.dataSource.sort = this.sort;
    }

    ngOnInit(): void {
        ListService.getIsLoadedPromise().then(() => {
            this.getData(this.currentFilters);
        });
    }

    resetFilters() {
        this.currentFilters = { pageSize: 50, pageNumber: 0 } as F;
        this.getData(this.currentFilters);
    }

    applyFilters(filters: F) {
        filters.pageNumber = 0;
        this.getData(filters);
        this.selection.clear() ;
    }

    getData(filters: F) {
        // empty method which will be overrided in descendants
    }

    deleteElements() {
        // empty method which will be overrided in descendants
    }

    getList(listName) {
        return ListService[listName];
    }

    fastFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        this.selection.clear();
        this.isDeleteDisabled = true;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.filteredData.length;
        return numSelected === numRows;
    }

    isSelected() {
        if (this.selection.selected.length > 0) {
            this.isDeleteDisabled = false;
        }
        else {
            this.isDeleteDisabled = true;
        }
    }

    /* Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() 
            ? this.selection.clear() 
            : this.dataSource.filteredData.forEach(row => this.selection.select(row));
        this.isSelected();
    }

    selectionToggle(row) {
        this.selection.toggle(row);
        this.isSelected();
    }

    pageChanged(event) {
        this.currentFilters.pageNumber = event.pageIndex;
        this.currentFilters.pageSize = event.pageSize;
        this.getData(this.currentFilters);
    }

    sortChanged(event) {
        // active:"lastName"
        // direction:"asc"
        this.currentFilters.orderByField = event.direction ? event.active : '';
        this.currentFilters.isDesc = event.direction === 'desc';
        this.getData(this.currentFilters);
        this.selection.clear() ;
    } 

    openDialog() {
        let that = this;
        let dialogRef = this.dialog.open(DeletionConfirmationWindowCmpnt, {
            width: '400px',
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                that.deleteElements();
            }
        });
    }
}
