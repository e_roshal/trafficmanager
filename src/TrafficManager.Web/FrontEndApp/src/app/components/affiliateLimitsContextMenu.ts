import { Component, Inject, Input, Output, EventEmitter } from '@angular/core';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { ListService } from '../services/list.service';
import { Broker } from '../models/entities/broker';
import { ExtendedUser, User } from '../models/entities/user';
import { UsersService } from '../services/users.service';
import { WebResult } from '../models/webResult';
import { UsersFilterDTO } from '../models/filters/userFiltersDTO';
import { AffiliateCountryLimit } from '../models/entities/affiliateLimit';
import { MatDialog } from '@angular/material';
import { AddAffiliateDepositConfirmationCmpnt } from './addAffiliateDepositConfirmation.cmpnt';
import { AffiliateService } from '../services/affiliate.service';
import { NotificationService } from '../services/notification.service';


@Component({
  selector: 'affiliate-limits-context-menu',
  templateUrl: '../templates/affiliateLimitsContextMenu.cmpnt.html',
})
export class AffiliateLimitsContextMenuCmpnt {
  constructor(
    private listSvc: ListService,
    private affiliatesSvc: AffiliateService,
    private notifications: NotificationService,
    public dialog: MatDialog
  ) { }


  @Input() x = 0;
  @Input() y = 0;
  @Input() selectedRecords: AffiliateCountryLimit[] = [];
  @Input() selectedRecord: AffiliateCountryLimit;

  @Output()  sendToBrokerCallback: EventEmitter<any> = new EventEmitter();
  @Output()  clearSelected: EventEmitter<any> = new EventEmitter();



  getList(listName) {
    return ListService[listName];
  }

  recordDescription() {

    return  this.selectedRecords.length < 1 
        ? `[${this.listSvc.getName(this.selectedRecord.affiliateId, ListService['affiliates'])}] in
           [${this.listSvc.getFullNameCountriesStringFromIsoCountries(this.selectedRecord.countries)}]`
        : ` [${this.selectedRecords.length}] records` ;
  }

  addFTD() {
    let dialogRef = this.dialog.open(AddAffiliateDepositConfirmationCmpnt, {
        width: '400px',
        data: {
            recordsCount: this.selectedRecords.length,
            record: this.selectedRecord
        }
    });
    dialogRef.afterClosed().subscribe(result => {
        if (result) {
            this.addFTDRequest();
        }
    });
  }

  addFTDRequest() {
    const limitsIds = this.selectedRecords.length > 0 
        ? this.selectedRecords.map(x => x.id)
        : [ this.selectedRecord.id ];

    this.affiliatesSvc.addFTD(limitsIds)
    .subscribe((val) => {
        this.notifications.showSuccess('Deposits added.');
    }, err => {
        this.notifications.showError(err.message);
    });
        
  }
}
