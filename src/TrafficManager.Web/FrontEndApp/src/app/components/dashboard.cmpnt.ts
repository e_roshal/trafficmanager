import { Component, OnInit } from '@angular/core';
import { ChartService } from '../services/chart.service';
import { DashboardService } from '../services/dashboard.service';
import { NotificationService } from '../services/notification.service';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { SiteConstants } from '../enums/siteConstants';

@Component({
    selector: 'app-dashboard',
    templateUrl: '../templates/dashboard.cmpnt.html',
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: SiteConstants.dateFormats },
    ],
})

export class DashboardCmpnt implements OnInit {
    leadsArray: any[] = [];
    depostitsArray: any[] = [];
    currentFilters = { fromDate: moment().subtract(3, 'months'), toDate: moment() };

    constructor(
        private chartSvc: ChartService,
        private notifications: NotificationService,
        private dashboardSvc: DashboardService
    ) { }

    ngOnInit(): void {
        this.getData(this.currentFilters);
    }

    getData(filters) {
        let _filters = Object.assign({}, filters);
        _filters.fromDate = filters.fromDate && (filters.fromDate as any).format('YYYY-MM-DD')
            ? (filters.fromDate as any).format('YYYY-MM-DD')
            : filters.fromDate || null;

        _filters.toDate = filters.toDate && (filters.toDate as any).format('YYYY-MM-DD')
            ? (filters.toDate as any).format('YYYY-MM-DD')
            : filters.toDate || null;

        this.dashboardSvc.getData(_filters).subscribe((resp: any) => {
            this.depostitsArray = resp.deposits;
            this.leadsArray = resp.leads;
            this.chartSvc.deletedCharts();
            this.chartSvc.getChart(this.leadsArray, "leads");
            this.chartSvc.getChart(this.depostitsArray, "deposits");
        },
            err => {
                this.notifications.showError(err.message);
                this.chartSvc.getChart(this.leadsArray, "leads");
                this.chartSvc.getChart(this.depostitsArray, "deposits");
            });
    }
}
