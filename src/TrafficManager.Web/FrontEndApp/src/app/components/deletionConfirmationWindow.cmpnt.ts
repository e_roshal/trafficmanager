import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'deletion-сonfirmation',
    templateUrl: '../templates/deletionConfirmationWindow.cmpnt.html',
  })
  export class DeletionConfirmationWindowCmpnt {
  
    constructor(
      public dialogRef: MatDialogRef<DeletionConfirmationWindowCmpnt>,
      @Inject(MAT_DIALOG_DATA) public data: any) { }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
  }
