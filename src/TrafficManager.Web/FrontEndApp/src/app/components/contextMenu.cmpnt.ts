import { Component, Inject, Input, Output, EventEmitter } from '@angular/core';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { ListService } from '../services/list.service';
import { Broker } from '../models/entities/broker';
import { ExtendedUser, User } from '../models/entities/user';
import { UsersService } from '../services/users.service';
import { WebResult } from '../models/webResult';
import { UsersFilterDTO } from '../models/filters/userFiltersDTO';


@Component({
  selector: 'context-menu',
  templateUrl: '../templates/ContextMenu.cmpnt.html',
})
export class ContextMenuCmpnt {
  constructor(
    private listSvc: ListService,
    private usersSvc: UsersService
  ) { }

  options = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: false,
    useBom: true,
    noDownload: false,
    headers: ['id',
      'email',
      'phone',
      'firstName',
      'lastName',
      'broker',
      'affiliate',
      'country',
      'leadDate',
      'depositDate',
      'ftd',
      // 'totalDeposit',
      'isReported',
      'callStatus',
      'offerId',
      'hasOfferId']
  };

  @Input() x = 0;
  @Input() y = 0;
  @Input() selectedRecords = [];
  @Input() filters: UsersFilterDTO;


  @Output()
  sendToBrokerCallback: EventEmitter<any> = new EventEmitter();
  @Output()
  clearSelected: EventEmitter<any> = new EventEmitter();

  isActiveMenuBrokers = false;

  sendTo($event) {
    this.isActiveMenuBrokers = true;
  }

  closeMenuBroker() {
    this.isActiveMenuBrokers = false;
  }

  allToCSV() {
    let _filters = Object.assign({}, this.filters);
    _filters.fromDate = this.filters.fromDate && (this.filters.fromDate as any).format('YYYY-MM-DD')
      ? (this.filters.fromDate as any).format('YYYY-MM-DD')
      : this.filters.fromDate || null;

    _filters.toDate = this.filters.toDate && (this.filters.toDate as any).format('YYYY-MM-DD')
      ? (this.filters.toDate as any).format('YYYY-MM-DD')
      : this.filters.toDate || null;

    _filters.fromDepositDate = this.filters.fromDepositDate && (this.filters.fromDepositDate as any).format('YYYY-MM-DD')
      ? (this.filters.fromDepositDate as any).format('YYYY-MM-DD')
      : this.filters.fromDepositDate || null;

    _filters.toDepositDate = this.filters.toDepositDate && (this.filters.toDepositDate as any).format('YYYY-MM-DD')
      ? (this.filters.toDepositDate as any).format('YYYY-MM-DD')
      : this.filters.toDepositDate || null;
    this.usersSvc.getUsers(Object.assign({}, _filters, { pageSize: 0, pageNumber: 0 }))
      .subscribe(
        (resp: WebResult<User[]>) => {
          let arrayToSave = resp.data as ExtendedUser[];
          arrayToSave.forEach((element, index) => {
            element.index = index + 1;
            element.countryName = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.country);
          });
          let result = new Angular5Csv(this.getArrayToSave(arrayToSave), "AllRecords", this.options);
        }
      );

  }

  selectedToCSV() {
    let arrayToSave = this.getArrayToSave(this.selectedRecords);
    let result = new Angular5Csv(arrayToSave, "SelectedRecords", this.options);
  }

  sendToBroker(broker: Broker) {
    let userIds = '';
    this.selectedRecords.forEach(x => { userIds = userIds + x.userId + ', '; });
    this.sendToBrokerCallback.emit({
      userIds,
      brokerId: broker.id
    });
    this.clearSelected.emit({
      isClearSelected: true
    });
  }


  getArrayToSave(array: ExtendedUser[]) {
    return array.map((x: ExtendedUser) => {
      return {
        id: x.id,
        email: x.email,
        phone: x.phone,
        firstName: x.firstName,
        lastName: x.lastName,
        broker: x.broker,
        affiliate: x.affiliate,
        country: x.country,
        leadDate: x.leadDate,
        depositDate: x.depositDate || '',
        ftd: x.ftd || '',
        isReported: x.isReported || '',
        // totalDeposit: x.totalDeposit || '',
        callStatus: x.callStatus || '',
        offerId: x.offerId,
        hasOfferId: x.hasOfferId,

      };
    });
  }

  deleteFieldIndexAndValueNull(array: any[]) {
    array.forEach(element => {
      delete element['index'];
      if (element['depositDate'] == null) {
        element['depositDate'] = "";
      }
    });
    return array;
  }

  getList(listName) {
    return ListService[listName];
  }

}
