import { Component } from "@angular/core";
import { TabledFilteredComponent } from "./tabledFilteredComponent";
import { Broker } from "../models/entities/broker";
import { BrokersService } from "../services/brokers.service";
import { NotificationService } from "../services/notification.service";
import { ListService } from "../services/list.service";
import { FormatterService } from "../services/formatter.service";
import { BrokerLimitsFiltersDTO } from "../models/filters/brokerLimitsFiltersDTO";
import { BrokerCountryLimit } from "../models/entities/brokerLimit";
import { ActivatedRoute } from "@angular/router";
import { getError } from "../utilities/errorResponse";
import { HttpErrorResponse } from "@angular/common/http";
import { MatDialog } from "@angular/material";

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'brokers-limits',
    templateUrl: '../templates/brokersLimits.cmpnt.html'
})
export class BrokersLimitsCmpnt extends TabledFilteredComponent<BrokerCountryLimit, BrokerLimitsFiltersDTO> {
    
    initialBroker: string;
    currentFilters: BrokerLimitsFiltersDTO = { pageSize: 50, pageNumber: 0, isGreater: null } as BrokerLimitsFiltersDTO;

    requestInProgress: boolean = false;
    additionInProgress: boolean = false;
    
    editId: number = null;    
    editingItem: BrokerCountryLimit = null;

    defaultItem = {
        broker: '',
        countries: '',
    } as BrokerCountryLimit;

    displayedColumns = [
        'select',
        'id',
        'broker',
        'countries',
        'limit',
        'buttons' ];

    constructor(
        private brokersSvc: BrokersService,
        private activatedRoute: ActivatedRoute,
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
        dialog: MatDialog
    ) {
        super(
            notifications,
            listSvc,
            formatter,
            dialog
        );

        this.activatedRoute.queryParams.subscribe( x => {
            if (x.brokerId) {
                this.currentFilters.brokerIds = [ x.brokerId ];
            }
        });
    }

    getData(filters: BrokerLimitsFiltersDTO) {
        this.additionInProgress = false;

        this.brokersSvc.getBrokersLimits(this.currentFilters).subscribe(x => {
            this.data = x.data;
            this.data.forEach((element, index) => {
                element['broker'] = this.listSvc.getName(element.brokerId, ListService.brokers);
                element['countriesName'] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.countries);
            });

            this.dataSource.data = this.data;
            this.paginator.length = x.totalItems;

        }, err => {
            this.notifications.showError(err.message);
        });
    }

    toggleView(row: BrokerCountryLimit) {
        if (!this.requestInProgress && this.selection.selected.length === 0 && !this.additionInProgress) {
            this.editingItem = Object.assign({}, this.data.find(x => x.id === row.id));
            this.editId = row.id;
        }
    }

    closeEdit(item: BrokerCountryLimit) {
        let index = this.data.findIndex(x => x.id == item.id);
        this.data[index] = this.editingItem;
        this.dataSource.data = this.data;
        this.editId = null;

        if (this.additionInProgress) {
            item = null;
            this.data = this.data.slice(0, this.data.length - 1);
            this.dataSource.data = this.data;
            this.additionInProgress = false;
        }
    }

    addItem() {
        if (this.requestInProgress || this.selection.selected.length > 0 || this.additionInProgress) {
            return;
        }
        if (this.editId != null) {
            this.closeEdit(this.editingItem);
        }
        this.editingItem = Object.assign({}, this.defaultItem);
        this.data.push(this.editingItem);
        this.dataSource.data = this.data;
        this.additionInProgress = true;
    }

    saveCountries(element: BrokerCountryLimit, value: any) {
        if (!value) {
            value = "";
            element.countries = null;
        }

        element.countries = this.formatter.getPipedStringFromArray(value);
        element["countriesName"] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.countries);
    }

    save(element: BrokerCountryLimit) {
        let observable = this.brokersSvc.updateBrokerLimit(element);
        observable.subscribe(
            (result: any) => { // sucess
                this.notifications.showSuccess(`BrokerLimit [${ result.data.id }] ${ element.id ? 'updated' : 'added' }`);
                this.requestInProgress = false;
                this.additionInProgress = false;
                this.editId = null;

                this.getData(this.currentFilters);
            }, (err: HttpErrorResponse) => { // error
                const errorMsg = getError(err);
                this.notifications.showError(errorMsg);
            }, () => { // complete
                this.requestInProgress = false;
            });
        return observable;
    }

    deleteElements() {
        
        let listIdsForDelete = this.formatter.getCsvStringFromArrayForDeletion(this.selection.selected);
        this.isDeleteDisabled = true;
        
        this.brokersSvc.deleteBrokerLimits(listIdsForDelete)
            .subscribe( val => {
                this.notifications.showInfo(`Deleted [${ val }] records`);
                this.selection.clear();
                this.getData(this.currentFilters);
            },
            err => {
                this.notifications.showError(err.message);
            });
    }
}
