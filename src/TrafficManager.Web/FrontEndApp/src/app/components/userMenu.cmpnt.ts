import { Component, OnInit, ApplicationRef } from '@angular/core';
import { UserService } from '../services/user.service';
import { ChangeDetectorRef } from '@angular/core';
import { NotificationService } from '../services/notification.service';

@Component({
    selector: 'user-menu',
    templateUrl: '../templates/userMenu.cmpnt.html'
})
export class UserMenuCmpnt implements OnInit {
    constructor(
        private userSvc: UserService,
        private notifications: NotificationService
    ) {
        this.userSvc.hideMenu = true;
    }

    ngOnInit(): void {
    }

}
