import { Component, OnInit, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import { BrokersService } from '../services/brokers.service';
import { NotificationService } from '../services/notification.service';
import { ListService } from '../services/list.service';
import { BrokerFiltersDTO } from '../models/filters/brokerFiltersDTO';
import { FormatterService } from '../services/formatter.service';
import { Broker } from '../models/entities/broker';
import { WebResult } from '../models/webResult';
import { TabledFilteredComponent } from './tabledFilteredComponent';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'brokers',
    templateUrl: '../templates/brokers.cmpnt.html'
})
export class BrokersCmpnt extends TabledFilteredComponent<Broker, BrokerFiltersDTO> {
    editId: number = null;
    previousBrokerVersion: Broker;
    currentFilters: BrokerFiltersDTO = { pageSize: 50, pageNumber: 0, isActive: true, isGreater: null } as BrokerFiltersDTO;

    requestInProgress: boolean = false;

    displayedColumns = [
        'id',
        'name',
        'active',
        'countries',
        'blockedCountries',
        'priority',
        'limit'];


    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        if (event.keyCode === 27) {
            this.cancel();
        }
    }


    constructor(
        private brokersSvc: BrokersService,
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
    ) {
        super(
            notifications,
            listSvc,
            formatter,
            null
        );
    }

    getData(filters: BrokerFiltersDTO) {
        if (filters.isGreater != null && filters.dailyLimit == null) {
            this.notifications.showError("Enter daily limit");
            return;
        }
        this.brokersSvc.getBrokers(filters)
            .subscribe(
                (resp: WebResult<Broker[]>) => {
                    this.data = resp.data as Broker[];
                    this.data.forEach((element, i) => {
                        element['countriesName'] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.countries);
                        element['blockedCountriesName'] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.blockedCountries);
                        element['index'] = ++i;
                    });
                    this.dataSource.data = this.data;
                    this.paginator.length = resp.totalItems;
                },
                err => {
                    this.notifications.showError(err.message);
                }
            );
        return false;
    }

    save(broker) {
        let observable = this.brokersSvc.updateBroker(broker);
        observable.subscribe(
            (result: any) => { // sucess
                let item = this.data.find(o => o.id === result.data.id);
                item = Object.assign(item, result.data);
                item['countriesName'] = this.listSvc.getFullNameCountriesStringFromIsoCountries(item.countries);                
                item['blockedCountriesName'] = this.listSvc.getFullNameCountriesStringFromIsoCountries(item.blockedCountries);
                this.previousBrokerVersion = null;

                this.notifications.showSuccess(`Broker [${result.data.name}] updated`);
                this.cancel();
            }, err => { // error
                this.notifications.showError(err.message);
            }, () => { // complete
                this.requestInProgress = false;
            });
        return observable;
    }

    toggleActive(broker) {
        if (this.requestInProgress) {
            return;
        }
        this.requestInProgress = true;

        broker.active = !broker.active;

        this.save(broker).subscribe(
            () => { }, // success
            () => { // failure
                broker.active = !broker.active;
            });
    }

    toggleView(broker: Broker) {
        if (!this.editId) {
            this.displayedColumns.push('buttons');
        }

        this.previousBrokerVersion = Object.assign({}, broker);
        this.editId = broker.id;
    }

    cancel() {
        if (this.editId) {
            this.editId = null;
            this.displayedColumns.pop();

            if (this.previousBrokerVersion) {
                let brokerIndex = this.data.findIndex(x => x.id === this.previousBrokerVersion.id);
                this.data[brokerIndex] = Object.assign({}, this.previousBrokerVersion);
                this.dataSource.data = this.data;
                this.previousBrokerVersion = null;
            }
        }
    }


    saveCountries(element: Broker, value: any) {
        if (!value) {
            value = "";
            element.countries = null;
        }

        element.countries = this.formatter.getPipedStringFromArray(value);
        element["countriesName"] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.countries);

    }

    saveBlockedCountries(element: Broker, value: any) {
        if (!value) {
            value = "";
            element.blockedCountries = null;
        }

        element.blockedCountries = this.formatter.getPipedStringFromArray(value);
        element["blockedCountriesName"] = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.blockedCountries);

    }
}
