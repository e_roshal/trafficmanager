import { Component, DoCheck, ApplicationRef } from '@angular/core';
import { AffiliatePaymentService } from '../services/affiliatePayment.service';
import { NotificationService } from '../services/notification.service';
import { MatDialog } from '@angular/material';
import { ListService } from '../services/list.service';
import { AffiliatePayment } from '../models/entities/affiliatePayment';
import { DeletionConfirmationWindowCmpnt } from './deletionConfirmationWindow.cmpnt';
import { FormatterService } from '../services/formatter.service';
import { PaymentFilterDTO } from '../models/filters/PaymentFilterDTO';
import { WebResult } from '../models/webResult';
import { TabledFilteredComponent } from './tabledFilteredComponent';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { SiteConstants } from "../enums/siteConstants";

@Component({
    selector: 'affiliate-payment',
    templateUrl: '../templates/affiliatePayment.cmpnt.html',
    // styleUrls: ['./app.component.css']
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: SiteConstants.dateFormats },
    ],
})
export class AffiliatePaymentCmpnt extends TabledFilteredComponent<AffiliatePayment, PaymentFilterDTO> implements DoCheck {

    brokerPayments: any[] = [];
    currentFiltersOfferName;
    all: string = "**";
    currentFilters = { pageSize: 50, pageNumber: 0 } as PaymentFilterDTO;
    
    editId: number = null;
    isActiveInProgress: boolean = false;
    item: AffiliatePayment = null;
    additionInProgress: boolean = false;
    defaultItem: AffiliatePayment = {
        id: null,
        AffiliateId: 0,
        amount: 0,
        validTo: '',
        paymentType: null,
        country: "",
        validFrom: ""
    };
    isCheckAffiliate: boolean = true;
    displayedColumns = ['select', 'id', 'country', 'validFrom', 'validTo', 'paymentType', 'amount', 'buttons'];
    
    constructor(
        private affiliateSvc: AffiliatePaymentService,
        notifications: NotificationService,
        listSvc: ListService,
        formatter: FormatterService,
        dialog: MatDialog,
        ) {
        super(
            notifications,
            listSvc,
            formatter,
            dialog
        );
    }

    ngDoCheck(): void {
        if (this.currentFilters.subjectId) {
            this.isCheckAffiliate = false;
        }
    }


    getData(filters: PaymentFilterDTO) {
        this.affiliateSvc.getAffiliates(filters).subscribe(
            (resp: WebResult<AffiliatePayment>) => {
                this.brokerPayments = resp.data as any;
                this.brokerPayments.forEach((element, index) => {
                    // tslint:disable-next-line:triple-equals
                    if (this.currentFilters.pageNumber == undefined) {
                        this.currentFilters.pageNumber = 0;
                    }
                    if (this.editId != null || this.additionInProgress) {
                        this.editId = null;
                        if (this.additionInProgress) {
                            this.item = null;
                            this.additionInProgress = false;
                        }
                    }
                    element.typeName = this.listSvc.getName(element.paymentType, this.getList("type"));
                    let countryName = this.listSvc.getFullNameCountriesStringFromIsoCountries(element.country);
                    element.countryName = this.all == countryName ? "All" : countryName;
                    element.index = this.currentFilters.pageNumber * this.currentFilters.pageSize + index + 1;
                });
                this.dataSource.data = this.brokerPayments;
                this.paginator.length = resp.totalItems;
            },
            err => {
                this.notifications.showError(err.message.join('; '));
            }
        );
    }

    addItem() {

        if (this.requestInProgress || this.selection.selected.length > 0 || this.additionInProgress) {
            return;
        }
        if (this.editId != null) {
            this.closeEdit();
        }
        this.item = Object.assign({}, this.defaultItem);
        this.brokerPayments.push(this.item);
        this.dataSource.data = this.brokerPayments;
        this.additionInProgress = true;
    }

    validationOfAnOperation() {
        this.item.AffiliateId = parseInt(this.currentFilters.subjectId.toString());
        if (this.item.paymentType != null) {

            if (this.requestInProgress) {
                return;
            }
            if (this.additionInProgress) {
                this.add();

            }
            else {
                this.update(this.item);
            }
        }
        else {
            this.notifications.showError("Select type");
        }
    }

    add() {
        this.item.id = 0;
        if (typeof (this.item.validTo) == 'object') {
            this.item.validTo = this.formatter.convertDate(this.item.validTo);
        }
        if (typeof (this.item.validFrom) == 'object') {
            this.item.validFrom = this.formatter.convertDate(this.item.validFrom);
        }
        // this.brokerPayments[this.brokerPayments.length - 1] = this.affiliateSvc.getBrokenItemForRender(this.item);
        this.affiliateSvc.addItemInAffiliate(this.item).subscribe(
            (resp: any) => {
                this.notifications.showSuccess(`Affiliate payment [${resp.data.id}] added`);
                this.requestInProgress = false;
                this.additionInProgress = false;
                this.getData(this.currentFilters);
            },
            err => {
                this.item.id = null;
                this.notifications.showError(err.error.messages);
            }
        );
    }

    update(item) {
        this.requestInProgress = true;
        if (typeof (this.item.validTo) == 'object') {
            this.item.validTo = this.formatter.convertDate(this.item.validTo);
        }
        if (typeof (this.item.validFrom) == 'object') {
            this.item.validFrom = this.formatter.convertDate(this.item.validFrom);
        }
        this.affiliateSvc.updateAffiliate(item)
            .subscribe(
                (result: any) => { // sucess
                    this.getData(this.currentFilters);
                    this.editId = null;
                    this.isActiveInProgress = false;
                    this.notifications.showSuccess(`Affiliate payment [${result.data.id}] updated`);
                }, err => { // error
                    this.notifications.showError(err.error.messages);
                    this.requestInProgress = false;
                    this.isActiveInProgress = false;
                    this.additionInProgress = false;
                }, () => { // complete
                    this.requestInProgress = false;
                    this.additionInProgress = false;
                });
    }

    closeEdit() {
        this.editId = null;
        if (this.additionInProgress) {
            this.item = null;
            this.brokerPayments = this.brokerPayments.slice(0, this.brokerPayments.length - 1);
            this.dataSource.data = this.brokerPayments;
            this.additionInProgress = false;
        }
    }

    removeMultipleAffiliateElements(that) {
        let listIdsForDelete = that.formatter.getCsvStringFromArrayForDeletion(that.selection.selected);
        that.affiliateSvc.removeMultipleAffiliateElements(listIdsForDelete).subscribe(
            (resp: any) => {
                that.selection.clear();
                that.isDeleteDisabled = true;
                this.getData(this.currentFilters);
            },
            err => {
                that.notifications.showError(err.message);
            }
        );
    }

    openDialog() {
        let that = this;
        let dialogRef = this.dialog.open(DeletionConfirmationWindowCmpnt, {
            width: '400px',
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                that.removeMultipleAffiliateElements(that);
            }
        });
    }
    toggleView(row: any) {
        if (!this.isCheckAffiliate) {
            if (!this.requestInProgress && this.selection.selected.length === 0 && !this.additionInProgress) {
                this.item = Object.assign({}, this.brokerPayments.find(rt => rt.id === row.id));
                if (!this.isActiveInProgress) {
                    this.editId = row.id;
                }
            }
        }
        else {
            this.notifications.showError("Select affiliate");
        }
    }

    resetFilters() {
        this.currentFilters = { pageSize: 50, pageNumber: 0 } as PaymentFilterDTO;
        this.currentFiltersOfferName = null;
        this.isCheckAffiliate = true;
        this.getData(this.currentFilters);
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.filteredData.forEach(row => this.selection.select(row));
        this.isSelected();
    }
}
