import { Component, OnInit, ApplicationRef, Input } from '@angular/core';
import { UserService } from '../services/user.service';
import { SecurityService } from '../services/security.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'navigation',
    templateUrl: '../templates/navigation.cmpnt.html'
})
export class NavigationCmpnt implements OnInit {
    constructor(
        private userSvc: UserService,
        public securitySvc: SecurityService
    ) {
    }

    ngOnInit(): void {
    }
}
