export class SiteConstants {
    static itemsPerPage = [5, 10, 50, 100, 500, 1000, 10000];    

    /// Formats for angular-moment provider
    static dateFormats = {
        parse: {
            dateInput: 'YYYY-MM-DD',
        },
        display: {
            dateInput: 'YYYY-MM-DD',
            monthYearLabel: 'MMM YYYY',
            dateA11yLabel: 'YYYY-MM-DD',
            monthYearA11yLabel: 'MMMM YYYY',
        },
    };
}


