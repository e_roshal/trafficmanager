import { environment } from "../../environments/environment";

class Endpoints {

    static baseUrl = environment.baseUrl;

    static forntend = {

    };

    static api = {
        user: {
            login: Endpoints.baseUrl + 'connect/token',
            logout: Endpoints.baseUrl + 'connect/revocation'
        },
        brokers: {
            list: Endpoints.baseUrl + 'api/broker',
            update: Endpoints.baseUrl + 'api/broker/%id%',
        },
        brokerLimits: {
            list: Endpoints.baseUrl + 'api/broker-limit',
            update: Endpoints.baseUrl + 'api/broker-limit/%id%',
        },
        affiliateLimits: {
            list: Endpoints.baseUrl + 'api/affiliate-limit',
            update: Endpoints.baseUrl + 'api/affiliate-limit/%id%',
            addFTD: Endpoints.baseUrl + 'api/affiliate-limit/add-deposit',
        },
        list: {
            countries: Endpoints.baseUrl + 'api/list/countries',
            offers: Endpoints.baseUrl + 'api/list/offers',
            affiliates: Endpoints.baseUrl + 'api/list/affiliates',  
            brokers: Endpoints.baseUrl + 'api/list/brokers',
        },
        routing: {
            routing: Endpoints.baseUrl + 'api/routing',
            update: Endpoints.baseUrl + 'api/routing/%id%',
            delete: Endpoints.baseUrl + 'api/routing/%id%'
        },
        identity: {
            changePassword: Endpoints.baseUrl + 'api/Identity/password',
            antiforgery: Endpoints.baseUrl + 'api/Identity/antiforgery'
        },
        users: {
            users: Endpoints.baseUrl + 'api/users',
            toBroker: Endpoints.baseUrl + 'api/users'
        },
        dashboard: {
            apiDynamics: Endpoints.baseUrl + 'api/dynamics'
        },
        affiliatePayment: {
            apiAffiliates: Endpoints.baseUrl + 'api/Payments/affiliate',
            update: Endpoints.baseUrl + 'api/Payments/affiliate/%id%',
            delete: Endpoints.baseUrl + 'api/Payments/affiliate/%id%'
        },
        brokenPayment: {
            apiBrokens: Endpoints.baseUrl + 'api/Payments/broker',
            update: Endpoints.baseUrl + 'api/Payments/broker/%id%',
            delete: Endpoints.baseUrl + 'api/Payments/broker/%id%'
        },
        paymentStatistics: {
            affiliates: Endpoints.baseUrl + 'api/payment_statistics/affiliate',
            brokens: Endpoints.baseUrl + 'api/payment_statistics/broker',
            countries: Endpoints.baseUrl + 'api/payment_statistics/country'
            
        },        
    };

}

export  { Endpoints };
