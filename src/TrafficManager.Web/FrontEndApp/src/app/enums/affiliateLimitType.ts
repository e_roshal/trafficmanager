export enum AffiliateLimitType {
    absolute = 'Absolute',
    relative = 'Relative'
}
