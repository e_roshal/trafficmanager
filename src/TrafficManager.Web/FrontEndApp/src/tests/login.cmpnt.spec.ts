import { LoginCmpnt } from '../app/components/login.cmpnt';
import { ComponentFixture, TestBed } from '@angular/core/testing';

let component: LoginCmpnt;
let fixture: ComponentFixture<LoginCmpnt>;
let h1: HTMLElement;

describe('Login component specs', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginCmpnt],
    });
    fixture = TestBed.createComponent(LoginCmpnt);
    component = fixture.componentInstance; // BannerComponent test instance
    h1 = fixture.nativeElement.querySelector('h1');
  });
});

