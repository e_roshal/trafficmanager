﻿USE [OVO_ser]
GO
/****** Object:  StoredProcedure [dbo].[GetBrokerUsersWithPayments]    Script Date: 28.09.2019 13:59:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetBrokerUsersWithPayments] (
		@date_from DATETIME = '1900-01-01', 
		@date_to DATETIME = NULL
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Set @date_to = Case When @date_to is NULL Then Getdate() Else @date_to End;

       SELECT 
		bu.user_id, 
		bu.lead_date, 
		bu.deposit_date, 
		bu.aff_id, 
		bu.broker_id, 
		bu.api_report,
		bu.is_reported,
		bu.is_dep_added,
		u.country, 
		ap.amount as affPayment, 
		bp.amount as brokerPayment 
	FROM (SELECT * from brokers_users as bsrs WHERE (
			((bsrs.deposit_date >= @date_from AND bsrs.deposit_date <= @date_to) 
		OR ( bsrs.lead_date >= @date_from AND bsrs.lead_date <= @date_to)))
		) as bu 
	LEFT JOIN users as u ON bu.user_id = u.id
	LEFT JOIN aff_payment as ap ON 
	bu.aff_id = ap.aff_id AND
				(   
					(
						(
							ap.payment_type = 1 --CPA 
							AND NOT(bu.deposit_date is null or bu.deposit_date = '')
							AND (bu.deposit_date >= ap.valid_from)
							AND (bu.deposit_date <= ap.valid_to OR (ap.valid_to is null or ap.valid_to = ''))
						) AND (
						ap.country = u.country
							OR  (
								(u.country is null OR ap.country like '**' )
								AND u.country not in (select distinct afp.country from aff_payment as afp
								WHERE afp.aff_id = ap.aff_id AND bu.deposit_date >= afp.valid_from AND (bu.deposit_date <= afp.valid_to OR afp.valid_to is null))
							)
						)
					)
						OR

					(
						( 
							ap.payment_type = 2 --CPL
							AND NOT(bu.lead_date is null or bu.lead_date= '')
							AND (bu.lead_date >= ap.valid_from)
							AND (bu.lead_date <= ap.valid_to OR (ap.valid_to is null or ap.valid_to = ''))
						) AND (
						ap.country = u.country
							OR (
								(ap.country is null OR ap.country like '**' )
								AND u.country not in (select distinct afp.country from aff_payment as afp
								WHERE afp.aff_id = ap.aff_id AND bu.lead_date >= afp.valid_from AND (bu.lead_date <= afp.valid_to OR afp.valid_to is null))
							)	
						) 
					)
				)
	LEFT JOIN brokers_payment as bp ON 
	bu.broker_id = bp.broker_id AND
				(   
					(
						(
							bp.payment_type = 1 --CPA 
							AND NOT(bu.deposit_date is null or bu.deposit_date = '')
							AND (bu.deposit_date >= bp.valid_from)
							AND (bu.deposit_date <= bp.valid_to OR (bp.valid_to is null or bp.valid_to = ''))
						) AND (
							bp.country = u.country
							OR (
								(bp.country is null OR bp.country like '**' )
								AND u.country not in (select distinct brp.country from brokers_payment as brp
								WHERE brp.broker_id = bp.broker_id AND  bu.deposit_date >= brp.valid_from AND (bu.deposit_date <= brp.valid_to OR brp.valid_to is null))
							)
						)
					)
						OR

					(
						( 
							bp.payment_type = 2 --CPL
							AND NOT(bu.lead_date is null or bu.lead_date= '')
							AND (bu.lead_date >= bp.valid_from)
							AND (bu.lead_date <= bp.valid_to OR (bp.valid_to is null or bp.valid_to = ''))
						) AND (
						bp.country = u.country
							OR (
								(bp.country is null OR bp.country like '**' )
								AND u.country not in (select distinct brp.country from brokers_payment as brp
								WHERE brp.broker_id = bp.broker_id AND bu.lead_date >= brp.valid_from AND (bu.lead_date <= brp.valid_to OR brp.valid_to is null))
							)	
						) 
					)
				)

END